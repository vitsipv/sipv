import Vue from 'vue'
import VueRouter from 'vue-router'
import pasos from './middleware';
Vue.use(VueRouter);

export default new VueRouter({
	routes: [
		{ path: '/', component: require('./views/Home.vue').default},
		{ path: '/home', component: require('./views/Home.vue').default},
		{ path: '/perfil', component: require('./views/profile/Perfil.vue').default,
		beforeEnter:(to,from,next)=>{
			window.ruta = to.path;
			
			pasos(window.user,to.path).then((resultado) => {
				
				if(resultado === 1){
					
					return next();	
				} 
				else return next('/unathorize');
			});
		}  },
		{ path: '/clientes', 
		component: require('./views/customers/Clientes.vue').default,
		beforeEnter:(to,from,next)=>{
			window.ruta = to.path;
			
			pasos(window.user,to.path).then((resultado) => {
				if(resultado === 1) return next();
				else return next('/unathorize');
			});
		} 
        },
		{ path: '/casos', component: require('./views/casos/Casos.vue').default,
		beforeEnter:(to,from,next)=>{
			window.ruta = to.path;
			
			pasos(window.user,to.path).then((resultado) => {
				if(resultado === 1) return next();
				else return next('/unathorize');
			});
		}  
		},
		{ path: '/gestionar_caso', component: require('./views/casos/GestionarCaso.vue').default,
		beforeEnter:(to,from,next)=>{
			window.ruta = to.path;
			pasos(window.user,to.path).then((resultado) => {
				if(resultado === 1) return next();
				else return next('/unathorize');
			});
		}
		},
		{ path: '/inventario_partes', component: require('./views/inventories/InventarioPP.vue').default,
		beforeEnter:(to,from,next)=>{
			//window.user,to.path
			// if(pasos(window.user,to.path)) next();
			// else $route.push('*');
			pasos(window.user,to.path).then((resultado) => {
				if(resultado === 1) return next();
				else return next('/unathorize');
			});
		}
		 },
		{ path: '/inventario_equipos', component: require('./views/inventories/InventarioE.vue').default,
		  beforeEnter:(to,from,next)=>{
			window.ruta = to.path;
			// if(pasos(window.user,to.path)) next();
			// else $route.push('*');
			pasos(window.user,to.path).then((resultado) => {
				
				if(resultado === 1){
					
					return next();
				}			 
				else 
				 return next('/unathorize');
			});
		}
		},
		{ path: '/inventario_movimientos', component: require('./views/inventories/InventarioMovimientos.vue').default,
			beforeEnter:(to,from,next)=>{
			//window.user,to.path
			// if(pasos(window.user,to.path)) next();
			// else $route.push('*');
			window.ruta = to.path;
			pasos(window.user,to.path).then((resultado) => {
				if(resultado === 1) return next();
				else return next('/unathorize');
			});
			}
		 },
		{ path: '/desarrollo', component: require('./views/develop/Desarrollo.vue').default,
		beforeEnter:(to,from,next)=>{
			window.ruta = to.path;
			//window.user,to.path
			// if(pasos(window.user,to.path)) next();
			// else $route.push('*');
			pasos(window.user,to.path).then((resultado) => {
				if(resultado === 1) return next();
				else return next('/unathorize');
			});
		}
		},
		{ path: '/adm_usuarios', component: require('./views/admin/AdmUsuarios.vue').default },
		{ path: '/adm_canales_de_compra', component: require('./views/admin/AdmCanales.vue').default },
		{ path: '/adm_modos_de_atencion', component: require('./views/admin/AdmModosAtencion.vue').default},
		{ path: '/adm_llamada', component: require('./views/admin/GestionLlamada.vue').default},
		{ path: '/adm_auditoria', component: require('./views/admin/AdmAuditoria.vue').default },
		{ path: '/invoice', component: require('./views/Invoice.vue').default,
		beforeEnter:(to,from,next)=>{
			//window.user,to.path
			window.ruta = to.path;
			// if(pasos(window.user,to.path)) next();
			// else $route.push('*');
			pasos(window.user,to.path).then((resultado) => {
				if(resultado === 1) return next();
				else return next('/unathorize');
			});
		} },
		{ path: '/tecnico', component: require('./views/tecnicos/Tecnico.vue').default,beforeEnter:(to,from,next)=>{
			//window.user,to.path
			window.ruta = to.path;
			// if(pasos(window.user,to.path)) next();
			// else $route.push('*');
			pasos(window.user,to.path).then((resultado) => {
				if(resultado === 1) return next();
				else return next('/unathorize');
			});
		} },
		{ path: '/recepcion', component: require('./views/reception/Recep_asig.vue').default,
		beforeEnter:(to,from,next)=>{
			window.ruta = to.path;
			//window.user,to.path
			// if(pasos(window.user,to.path)) next();
			// else $route.push('*');
			pasos(window.user,to.path).then((resultado) => {
				if(resultado === 1) return next();
				else return next('/unathorize');
			});
		} },
		{ path: '/llamadas', component: require('./views/casos/Llamadas.vue').default,
		beforeEnter:(to,from,next)=>{
			//window.user,to.path
			// if(pasos(window.user,to.path)) next();
			// else $route.push('*');
			window.ruta = to.path;
			pasos(window.user,to.path).then((resultado) => {
				if(resultado === 1) return next();
				else return next('/unathorize');
			});
		} },
		{ path: '/recepcion', component: require('./views/reception/Recep_asig.vue').default,
		beforeEnter:(to,from,next)=>{
			//window.user,to.path
			window.ruta = to.path;
			// if(pasos(window.user,to.path)) next();
			// else $route.push('*');
			pasos(window.user,to.path).then((resultado) => {
				if(resultado === 1) return next();
				else return next('/unathorize');
			});
		} },
		{ path: '/presupuesto', component: require('./views/presupuesto/Presupuesto.vue').default,
		beforeEnter:(to,from,next)=>{
			//window.user,to.path
			window.ruta = to.path;
			// if(pasos(window.user,to.path)) next();
			// else $route.push('*');
			pasos(window.user,to.path).then((resultado) => {
				if(resultado === 1) return next();
				else return next('/unathorize');
			});
		} },
		{ path: '/presupuesto/pdf', component: require('./views/presupuesto/Presupuesto.vue').default,
		beforeEnter:(to,from,next)=>{
			//window.user,to.path
			window.ruta = to.path;
			// if(pasos(window.user,to.path)) next();
			// else $route.push('*');
			pasos(window.user,to.path).then((resultado) => {
				if(resultado === 1) return next();
				else return next('/unathorize');
			});
		} },
		{ path: '/llamadas', component: require('./views/casos/Llamadas.vue').default,
		beforeEnter:(to,from,next)=>{
			//window.user,to.path
			window.ruta = to.path;
			// if(pasos(window.user,to.path)) next();
			// else $route.push('*');
			pasos(window.user,to.path).then((resultado) => {
				if(resultado === 1) return next();
				else return next('/unathorize');
			});
		} },
		{ path: '/comentarios_caso', component: require('./views/casos/Comentario.vue').default,
		beforeEnter:(to,from,next)=>{
			//window.user,to.path
			// if(pasos(window.user,to.path)) next();
			// else $route.push('*');
			window.ruta = to.path;
			pasos(window.user,to.path).then((resultado) => {
				if(resultado === 1) return next();
				else return next('/unathorize');
			});
		} },
		{ path: '/pdf', component: require('./views/pdf.vue').default},
		{ path: '/permisos', component: require('./views/permisos/listauser.vue').default},
		{ path: '/unathorize',component: require('./views/errors/unathorize.vue').default},
		{ path: '*', component: require('./views/errors/404.vue').default }
		
	],
     mode: 'history',
})