/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import Vue from 'vue'

//Fuentes para los pdf
require('./fonts/Nunito-Regular-normal')
require('./fonts/Nunito-Bold-bold')

require('./bootstrap');

window.Vue = require('vue');

window.EventBus = new Vue();

// Bootstrap-vue
import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue)
// import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// V-FORM para el Backend Error con Vuejs
import {
    Form,
    HasError,
    AlertError
} from 'vform';
window.Form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

// Paquete para la Paginación de las tablas
Vue.component('pagination', require('laravel-vue-pagination'));

// Puertas de acceso para las rutas
import Gate from './gate';
Vue.prototype.$gate = new Gate(window.user);
// FILTROS PARA TRANSFORMAR, VALIDAR, MANIPULAR Y MOSTRAR DATOS Y FECHAS EN JS
import moment from 'moment';
Vue.filter('upText', function (text) {
    /*ESTA FUNCION SE ENCARGA DE PONER LA PRIMERA LETRA EN MAYUS AL LLAMAR DATOS A TABLAS CON VUE*/
    return text.charAt(0).toUpperCase() + text.slice(1)
});

Vue.filter('myDate', function (created) {
    /*ESTA FUNCION SE ENCARGA DE MEJORAR LA VISTA DE LA FECHA Y HORA*/
    return moment(created).format('DD/MM/YYYY') + ' a las ' + moment(created).format('h:mm:ss a');
});


// Barra de progreso VUE-PROGRESSBAR
import VueProgressBar from 'vue-progressbar'
Vue.use(VueProgressBar, {
    color: 'rgb(143, 255, 199)',
    failedColor: 'red',
    height: '5px'
})

// SweetAlert 2
import swal from 'sweetalert2'
window.swal = swal;
const toast = swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 5000
});
window.toast = toast;

// Recargar datos en la pagina sin refrescar
window.Fire = new Vue();

// Mascaras o parametros para los inputs
import VueTheMask from 'vue-the-mask'
Vue.use(VueTheMask)

// VUEX
import Vuex from 'vuex'
Vue.use(Vuex)

// MULTISELECT
import Multiselect from 'vue-multiselect'
Vue.component('multiselect', Multiselect);
import 'vue-multiselect/dist/vue-multiselect.min.css'
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

/* Vue.component('example-component', require('./components/ExampleComponent.vue')); Usando Laravel-Mix V5 en vez de V2 como se ha estado usando hasta ahora
    la manera de presentar los componentes de vue cambia. El "require() ya está obsoleto*/

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('seguridad', require('./components/Seguridad.vue').default);
Vue.component('tecnico', require('./components/usuario_tecnico/TecnicoComponent.vue').default);
Vue.component('presupuesto', require('./components/presupuesto/PresupuestoComponent.vue').default);
Vue.component('adm-clients', require('./components/clientes/AdmClientsComponent.vue').default); // Componente encargado para realisar la busqueda, creación y edit de los clientes
Vue.component('crear-caso', require('./components/clientes/casos/CrearCasoComponent.vue').default); // Componente encargado para realisar la busqueda, creación y edit de los clientes
Vue.component('gestionar-caso', require('./components/clientes/casos/GestionarCasosComponent.vue').default);
Vue.component('llamada-caso', require('./components/clientes/casos/LlamadaCasoComponent.vue').default);
// COMPONENTES DE RECEPCION
Vue.component('Panelreception', require('./components/reception/Panelreception.vue').default);
Vue.component('Comentario', require('./components/clientes/casos/ComentarioComponent.vue').default);
// COMPONENTES DESARROLLO
Vue.component('passport-clients', require('./components/passport/Clients.vue').default);
Vue.component('passport-authorized-clients', require('./components/passport/AuthorizedClients.vue').default);
Vue.component('passport-personal-access-tokens', require('./components/passport/PersonalAccessTokens.vue').default );

import _, { forEach, result } from 'lodash' // Necesario para el _.debounce
import router from './routes' // Rutas Vue
import permisos from './permisos';

const store = new Vuex.Store({
    state: {
      paso:[],
      crear:"",
      borrar:"",
      editar:""
    },
    mutations: {
       increment (state) {
        permisos().then((result) => {
          state.paso = result;
          if(state.paso.length > 0){
            for(var i =0 ; i<state.paso.length;i++){
              if(state.paso[i] === 'crear') state.crear = 'crear'; 
              if(state.paso[i] === 'borrar') state.borrar = 'borrar';
              if(state.paso[i] === 'editar') state.editar = 'editar';
            }
          }
          
        });


      },
      
    }
    
  });

const app = new Vue({ // Inicio de app para el id en master.blade.php
    el: '#app',
    router,
    store:store,
    created(){
      store.commit('increment')
      
    }
});