export default class Gate{
	constructor(user){
		
		this.user = user;
		
	}

	esAdmin(){
		
		return this.user.user_type_id === 1;
	}
	
  esGerente(){
      	return this.user.user_type_id === 2;
  }

  esCoordinador(){
        return this.user.user_type_id === 3;
    }

  esAdminOesGerente(){
      	if ((this.user.user_type_id === 1) || (this.user.user_type_id === 2)) {}
      	return true;
  }
}