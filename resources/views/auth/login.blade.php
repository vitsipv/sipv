@extends('layouts.app')

@section('content')
<section class="login-block">
    <div class="container efecto">
        <div class="row">
            <div class="col-md-4 login-sec">
                 <div class="logo-login">
                    <img src="/images/logo-vit.png" alt="Logo VIT" height="80px">
                 </div>

            <div class="container">
                  <div class="form-group">
                      <label for="email">{{ __('Usuario') }}</label>
                          <div>
                            <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} loguse" name="email" value="{{ old('email') }}" placeholder="Usuario" required autofocus>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        </div>
                    </div>
                        <div class="form-group">
                            <label for="password">{{ __('Contraseña') }}</label>
                            <div>
                                <div class="input-group mb-3">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} pass" name="password" placeholder="Contraseña" required>
                                <div class="input-group-append">
                                    <i class="fas fa-eye input-group-text toggle-password" style="display: inline-flex;"></i>
                                </div>
                                </div>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <button style="height: 40px;"  class="btn btn-primary ingresar">
                                    {{ __('Ingresar') }}
                                    </button>
                                </div>
                                <div class="col-md-8">
                                    <div class="custom-control custom-checkbox">
                                      <input type="checkbox" name="remember" class="custom-control-input" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                      <label class="custom-control-label" for="remember">{{ __('Recordarme') }}</label>
                                    </div>
                                    <a href="{{ route('password.request') }}">
                                        {{ __('¿Olvido su contraseña?') }}
                                    </a>
                                </div> 
                            </div> 
                        </div>
                </div>
            </div>        
        <div class="col-md-8 banner-sec">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                 <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                  </ol>
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                  <img class="d-block img-fluid" src="/images/login/vit1.jpg" alt="login1">
                </div>
                <div class="carousel-item">
                  <img class="d-block img-fluid" src="/images/login/vit2.jpg" alt="login2">
                </div>
                <div class="carousel-item">
                  <img class="d-block img-fluid" src="/images/login/vit3.jpg" alt="login3">
                </div>
            </div>
    </div>
  </div>
</div>
</section>
<script src="{{ asset('./jquery/jquery.min.js')}}"></script>
<script>
    $(document).ready(function(){
        
        $('.ingresar').click(function(){
            var loguse = $('.loguse').val();
            var password = $('.pass').val();
            
            $.ajax({
                type:'POST',
                url:"{{route('login')}}",
                data:{
                 "_token": "{{ csrf_token() }}",
                 loguse: loguse,
                 pass: password

                },success:function(request){
                    //
                    
                    if(request === 'Sus Credenciales no estan registrada en nuestra Bases de datos'){
                        toastr.error(request);
                    }else{
                        window.location.reload();//cargar cuando el usuario se haya autenticado
                    }
                    console.log(request);
                }   
            });


        })
    });
</script>
@endsection
