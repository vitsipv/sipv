@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-sm-11">
      <!-- Main content -->
      <div class="invoice p-1 mb-3">
        <!-- Encabezado Enmarcado-->
        <div class="border border-dark">
          <div class="col-11 d-flex justify-content-center" style="margin-bottom: -5px;">
            <h4>VENEZOLANA DE INDUSTRIA TECNOLÓGICA, C.A.</h4>
          </div>
          <div class="col-11 d-flex justify-content-center" style="margin-bottom: -5px;">
            <h6>RIF: G-20009381-1</h6>
          </div>
          <div class="col-11 d-flex justify-content-center" style="margin-bottom: -5px;">
            <h6>PRESUPUESTO</h6>
          </div>
        </div>
        <!-- /. Fin del Encabezado Enmarcado -->

        <!-- Encabezado-->
        <div class="col-11 d-flex justify-content-center">
            El Computador Bolivariano
        </div>

        <div class="col-11 d-flex justify-content-end">
            <strong>Número de Presupuesto: {{ $request->numero_presupuesto }}</strong>
        </div>
        <!-- Fin del Encabezado-->

        <div class="row invoice-info">
          <div class="col-sm-6 invoice-col border-left">
            <div class="col-12">
              PARA: {{ $request->datosCliente }}
            </div>
            <div class="col-12">
              CEDULA / RIF: {{ $request->documento }}
            </div>
            <div class="col-12">
              DIRECCIÓN: {{ $request->direccion }}
            </div>
          </div>
          <!-- /.col -->
          <div class="col-sm-6 invoice-col border-left">
            <div class="col-12">
              PERSONA CONTACTO:
            </div>
            <div class="col-12">
              TELEFONOS: {{ $request->telefonos }}
            </h8>
          </div>
          <div class="col-12">
            CORREO ELECTRONICO: {{ $request->correo }}
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table Primary -->
      <div class="row">
        <div class="col-12 table-responsive" style="margin-bottom:-10px;">
          <table class="table table-sm">
            <thead>
              <tr>
                <th>Fecha y Hora del presupuesto</th>
                <th>Elaborado por</th>
                <th>Gerencia</th>
              </tr>
              <tr>
                <td>{{ $request->date }}</td>
                <td>{{ $request->analista_presupuesto }}</td>
                <td>POST-VENTA</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>

      <!-- Table Secundary -->
      <div class="row" style="margin-top: -10px;">
        <div class="col-12 table-responsive">
          <table class="table table-sm">
            <thead>
              <tr>
                <th>Numero de Caso</th>
                <th>Descripción</th>
                <th>Uni</th>
                <th>Monto Uni Bs.</th>
                <th>Total Bs.</th>
              </tr>
              @foreach ($presupuesto as $presupuestos)
              <tr>
                <td>{{ $request->numero_caso }}</td> 
                <td>{{ $presupuestos['piezas'] }}</td>
                <td>{{ $presupuestos['cant_piezas'] }}</td>
                <td>{{ $presupuestos['precio_piezas'] }}</td>
                <td>{{ $presupuestos['total'] }}</td>
              </tr>
              @endforeach
            </thead>
          </table>
        </div>
      </div>


      <div class="col-5" style="margin-left: 60%; margin-top: -20px;">
        <div class="table-responsive " >
          <table class="table table-sm " >
            
            <tbody>
              <tr>
                <th>Sub-Total Bs.</th>
                <td><strong>{{ $subtotal }}</strong></td>
              </tr>
              <tr>
                <th>IVA ({{ $iva }}%) Bs.</th>
                <td><strong>{{ $ivaTotal }}</strong></td>
              </tr>
              <tr>
                <th>Total Bs. S</th>
                <td><strong>{{ $montoFinal }}</strong></td>
              </tr>
            </tbody>
            
          </table>
          </div>
        </div>
        @if($request->condiciones == 'PC')
        <div class="row" style="margin-top: -20px; margin-bottom: -5px;">
          <!-- accepted payments column -->
          <div class="col-12" style="page-break-before: all; ">
            PRESUPUESTO APLICA EXCLUSIVAMENTE PARA MODELO: {{ $request->modelo }}<br>
            TÉRMINOS Y CONDICIONES:<br>
            1.- Las garantías son únicamente por defecto de fabrica durante el siguiente
            periodo: 6 meses para monitores, 3 meses para partes y piezas. A excepción de
            los ajustes de componente y las soldaduras de pin de carga.<br>
            2.- Los equipos se reparan luego de recibir el pago por las piezas
            presupuestadas en un tiempo no mayor un (3) días hábiles.<br>
            3.- Al culminar la reparación, se le contactará para informarle que debe efectuar
            el retiro formal de su equipo.<br>
            4.- FORMAS DE PAGO:
            NO ACEPTAMOS CHEQUES NI MANEJAMOS EFECTIVO en los Centro de
            Soporte Técnico.<br>
            5.1- Depósito.<br>
            5.2- Transferencia.<br>
            5.3- Punto de Venta<br>
            5.4- Deposito o transferencia a nombre de Venezolana de Industria Tecnológica
            C.A, en cualquiera las siguientes cuentas corriente:<br>
            Banco del Tesoro: 0163-0202-3020-2200-0125<br>
            Banco Banesco: 0134-1070-4800-0300-1776<br>
            NOTA: Para que la verificación del pago sea mas rápida, se recomienda hacer el
            depósito en efectivo o transferir desde el mismo banco.<br>
            5.- Luego de realizar el pago, debe llevar al Centro de Soporte Técnico una
            copia firmada de este documento junto con la planilla de depósito bancario
            original, o enviar dichos documentos de forma digital a los siguientes correos:<br>
            Clientes Caracas:presupuestos@vit.gob.ve<br>
            Clientes Falcon:presupuestosfalcon@vit.gob.ve<br>
            Clientes Valencia :presupuestosvalencia@vit.gob.ve<br>
            Para Ente o Empresa... Consignar con los soportes de pago, copia de RIF<br>
            6.- Estimado cliente, si desea se le entregue la pieza defectuosa, realizar
            confirmación mediante correo electrónico indicado o informar al personal que
            le atienda en el Centro de soporte.<br>
            7.- Ante cualquier duda, reclamo o sugerencia, debe comunicarse al Centro de
            Atención Telefónica marcando al 0800-INFOVIT (08004636848).<br>
            8.- El ultimo día hábil de cada mes no se recibirán pagos ni se emitirán
            facturas en el centro de soporte, ya que se realizará el cierre fiscal.<br>
            9.- Este presupuesto tiene una validéz de Cinco (5) días hábiles. Si usted no
            realiza el pago en ese periodo de tiempo, su caso será cerrado y deberá retirar
            su equipo sin ser reparado.<br>
            10.- No nos hacemos responsables de los equipos que tengan más de 03 meses
            en nuestro Centro de Soportes, deben retirar el equipo en un lapso no mayor a
            15 días hábil.
          </div>
          <!-- /.col -->
        </div>
        @endif

        @if ($request->condiciones == 'SERVIDOR')
          <div class="row">
          <div class="col-12" style="page-break-before: all; margin-top:-20px;">
           <strong> PRESUPUESTO APLICA EXCLUSIVAMENTE PARA MODELO: {{ $request->modelo }}</strong><br>
            <strong>TÉRMINOS Y CONDICIONES:</strong><br>
            1.- Marca: Equipos VIT.<br>
            Lugar de entrega: A Convenir.<br>
            Validez de la Oferta: 5 días.<br>
            Modalidad de pago: Depósito o Transferencia a las Cuentas Bancarias:<br>
            Banesco 0134-1070-48-0003001776  / Tesoro 0163-0202-30-2022000125<br>
            Garantía:  La  garantía  de  las  partes  y  piezas  esta  sujeta  al  tiempo  de  garantía
            que   tenga   el   contrato   inicial   de   la   adquisición   del   equipo   al   cuál   serán
            instaladas.<br>
            2.- Anexo 1: Términos y Condiciones de la Garantía.<br>
            La garantía cubre únicamente aquellos defectos que surgiesen como resultado
            del  uso  normal  del  producto  y  no  será  válida  la  Garantía  en  los  siguientes
            casos:<br>
            1°. Si ha sido violentada la etiqueta de sello de garantía.<br>
            2°.  Que  el  producto  no  haya  sido  operado  conforme  al  instructivo  de  uso  y
            operación  que  se  acompaña  y  no  se  hayan  observado  las  recomendaciones  y
            advertencias que se indican.<br>
            3°.  Que  el  producto  haya  sido  modificado  o  desarmado  parcial  o  totalmente;  o
            haya  sido  manipulado  negligentemente  y  como  consecuencia  haya  sufrido
            daños atribuibles al cliente, persona o talleres no autorizados por VIT, así como
            por  daños  causados  por  el  uso  o  conservación  fuera  de  los  parámetros
            normales   del   producto   y   por   la   modificación   e   incorporación   de   otros
            productos.<br>
            4°.  Que  los  datos  del  certificado  y  del  producto  no  coincidan  o  hayan  sido
            alterados o removidos de su lugar.<br>
            5°.   Que   el   sello   de   garantía   del   equipo   sea   removido   por   personas   no
            autorizadas por VIT.<br>
            3.- Anexo 2: Servicio y Soporte Técnico.<br>
            Servicio de Soporte Técnico al Cliente por Helpdesk:<br>
            El  objetivo  fundamental  es  atender  los  requerimientos  solicitados  por  los
            clientes brindando soporte técnico preliminar, a través de la línea 0800-INFOVIT
            (0800-4636848),  chat  o  correo  electrónico,  a  través  de  nuestra  página  web,
            www.vit.gob.ve, brindando atención personalizada por parte de especialistas en
            aras  de  solucionar  las  fallas  o  problemas  presentados  en  un  primer  nivel,  las
            cuales de no ser resueltas por esta vía, se realizará el servicio en sitio.
            Escalamiento de la Llamada (niveles).<br>
            1)  Operador  -  1°  Nivel:  Se  deberá  llamar  al  0800-INFOVIT  (0800-4636848),  para
            solicitar información del servicio en cuestión.<br>
            2)Analista  de  Soporte  Técnico  (A.S.T.)  -  2°  Nivel:  Analista  de  Soporte  Técnico
            (coordinador    de    los    servicios)    a    través    del    teléfono    0800-INFOVIT
            (0800-4636848), e-mail: soporte@vit.gob.ve<br>
            3) Analista de Post-venta - 3° Nivel: Analista de Post-venta a través del teléfono
            0800-INFOVIT     (0800-4636848)     opción     2,     luego     marcar     8888,     e-mail:
            soporteservidores@vit.gob.ve<br>
            4)  Gerente  de  Post-venta  -  4°  Nivel:  Gerente  de  Post-venta  a  través  de  los
            teléfonos 0212-3732853, 0414-6827627, e-mail: lrada@vit.gob.ve<br>
          </div>
        </div>
        @endif
      </div>
      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-12">
          <button type="button" target="_blank"  onclick="javascript:window.print()" class="btn btn-primary float-right" style="margin-right: 5px;">
            <i class="fa fa-download"></i> Descargar PDF
          </button>
          <button type="button"  onclick="window.location='/presupuesto'" class="btn btn-danger float-right" style="margin-right: 5px;">
            Atras
          </button>
        </div>
      </div>
      

   </div>
   <!-- /.invoice -->
 </div>
</div>


@endsection