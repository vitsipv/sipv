<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} | Sistema Integral VIT</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/fonts/Nunito-Regular-normal.js') }}" type="module"></script>
    <script src="{{ asset('js/fonts/Nunito-Bold-bold.js') }}" type="module"></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('toastr/toastr.min.css')}}">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <!-- <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
            </div>
        </nav>
         -->
        <main>
            @yield('content')
        </main>
    </div>
</body>
{{-- <footer class="text-center margen-footer no-print">
    <strong>Copyright &copy; {{ date('Y') }} <a href="#">Venezolana de Industria Tecnológica C.A</a> RIF: G-20009381-1 - Todos los derechos reservados.</strong>
</footer> --}}
<script src="{{asset('jquery/jquery.min.js')}}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('toastr/toastr.min.js')}}"></script>
<script>   
      $(".toggle-password").click(function() {
          $(this).toggleClass("far fa-eye-slash");
          var x = document.getElementById("password");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        });
</script>


</html>
