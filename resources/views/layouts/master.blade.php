<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title>{{ config('app.name', 'Laravel') }} | Sistema Integral VIT</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
  </head>
<body class="hold-transition sidebar-mini sidebar-collapse">
<div class="wrapper" id="app">
  
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-dark navbar-gray-dark">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="/" class="nav-link">Inicio</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="/" class="nav-link">Soporte</a>
      </li>
    </ul>

    @can('esAdmin')
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="false" href="#"><i
            class="fa fa-th-large"></i></a>
      </li>
    </ul>
    @endcan
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
      <img src="/images/logo-vit.png" alt="VIT Logo" class="brand-image-logo" style="height: 65px; padding-left: 10%;">
      <span class="brand-text font-weight-light">Sistema Integral NH</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="/images/perfil/{{Auth::user()->photo}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <span class="nombre-color" style="font-size: 20px">{{ ucwords(Auth::user()->name) }}</span>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
            <li class="nav-item">
              <router-link to="/" class="nav-link">
                <i class="nav-icon fa fa-th"></i>
                <p>Home<span class="right badge badge-danger">Nuevo</span></p>
              </router-link>
            </li>

            @can('index',App\Permisos::class)
            <li  class="nav-item">
              <router-link to="/clientes" class="nav-link">
                  <i class="fas fa-users nav-icon"></i>
                  <p>Clientes</p>
                </router-link>
              </li>  
            @endcan
            <li class="nav-item">
              <a href="" class="nav-link">
                <i class="fa fa-address-card nav-icon"></i>
                <p>Casos<i class="fa fa-angle-left right"></i></p>
              </a>
            
              <ul class="nav nav-treeview">
                @can('Apertura_caso', App\Permisos::class)
                <li class="nav-item">
                  <router-link to="/casos" class="nav-link">
                    <i class="fa fa-plus nav-icon"></i>
                    <p>Aperturar Caso</p>
                  </router-link>
                </li>  
                @endcan
                @can('Gestionar_caso', App\Permisos::class)
                <li class="nav-item">
                  <router-link to="/gestionar_caso" class="nav-link">
                    <i class="fa fa-edit nav-icon"></i>
                    <p>Gestionar Caso</p>
                  </router-link>
                </li>  
                @endcan
                
              </ul>
            </li>
            @can('Recepcion', App\Permisos::class)
            <li class="nav-item">
              <router-link to="/recepcion" class="nav-link">
                <i class="fas fa-bell nav-icon"></i>
                <p>Recepcion</p>
              </router-link>
            </li>  
            @endcan
            
            @can('Tecnico', App\Permisos::class)
            <li class="nav-item">
              <router-link to="/tecnico" class="nav-link">
                <i class="nav-icon fas fa-screwdriver"></i>
                <p>Tecnico</p>
              </router-link>
            </li>  
            @endcan

            @can('Presupuesto', App\Permisos::class)
            <li class="nav-item">
              <router-link to="/presupuesto" class="nav-link">
                <i class="nav-icon fas fa-file-invoice"></i>
                <p>Presupuesto</p>
              </router-link>
            </li>  
            @endcan
            @can('LLamada', App\Permisos::class)
            <li class="nav-item">
              <router-link to="/llamadas" class="nav-link">
                <i class="fas fa-tty nav-icon"></i>
                <p>LLamada</p>
              </router-link>
            </li>
            @endcan
            
              <li class="nav-item">
                <router-link to="/perfil" class="nav-link">
                  <i class="nav-icon fas fa-user-edit"></i>
                  <p>Perfil</p>
                </router-link>
              </li>  
            
            <li class="nav-item">

            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="fas fa-warehouse nav-icon"></i>
                <p>Inventario<i class="fa fa-angle-left right"></i></p>
              </a>
              <ul class="nav nav-treeview">
                @can('PartesPiezas', App\Permisos::class)
                <li class="nav-item">
                  <router-link to="/inventario_partes" class="nav-link">
                    <i class="fas fa-microchip nav-icon"></i>
                    <p>Partes y Piezas</p>
                  </router-link>
                </li>  
                @endcan
                @can('Equipos', App\Permisos::class)
                <li class="nav-item">
                  <router-link to="/inventario_equipos" class="nav-link">
                    <i class="fas fa-server nav-icon"></i>
                    <p>Equipos</p>
                  </router-link>
                </li>                  
                @endcan
                @can('Movimientos', App\Permisos::class)
                <li class="nav-item">
                  <router-link to="/inventario_movimientos" class="nav-link">
                    <i class="fas fa-truck nav-icon"></i>
                    <p>Movimientos</p>
                  </router-link>
                </li>  
                @endcan
                
              </ul>
            </li>
            @can('esAdmin')
            <li class="nav-item">
              <router-link to="/desarrollo" class="nav-link">
                <i class="nav-icon fa fa-cogs"></i>
                <p>Desarrollo</p>
              </router-link>
            </li>
            @endcan
            <!-- Boton de Salir -->
            <li class="nav-item">
              <a href="{{ route('logout') }}" onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();" class="nav-link">
                <i class="nav-icon fas fa-sign-out-alt red"></i>
                <p>Salir</p>
              </a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
              </form>
            </li>
        </ul>
      </nav>
    </div>
  </aside>
  <!-- Content Wrapper tiene el contenido de la página -->
  <div class="content-wrapper">
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <vue-progress-bar></vue-progress-bar>
          <router-view></router-view>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @can('esAdmin')
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark sidebar-dark-primary">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Administrador</h5>
      <p>Opciones de Administración</p>
    </div>
      <nav class="mt-2">
        <ul class="nav nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item has-treeview">
            <router-link to="/adm_usuarios" class="nav-link">
              <h6 class="asidefix"><i class="nav-icon fas fa-users-cog"></i>Usuarios</h6>
            </router-link>
          </li>
        </ul>
      </nav>
      <nav class="mt-2">
        <ul class="nav nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item has-treeview">
            <router-link to="/adm_canales_de_compra" class="nav-link">
              <h6 class="asidefix"><i class="nav-icon fas fa-cart-plus"></i>Canales de Compra</h6>
            </router-link>
          </li>
        </ul>
      </nav>
      <nav class="mt-2">
        <ul class="nav nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item has-treeview">
            <router-link to="/adm_modos_de_atencion" class="nav-link">
              <h6 class="asidefix"><i class="nav-icon fas fa-headset"></i>Modos de Atención</h6>
            </router-link>
          </li>
        </ul>
      </nav>
      <nav class="mt-2">
        <ul class="nav nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item has-treeview">
            <router-link to="/adm_llamada" class="nav-link">
              <h6 class="asidefix"><i class="nav-icon fas fa-phone-square"></i>Gestión de llamadas</h6>
            </router-link>
          </li>
        </ul>
      </nav>
       <nav class="mt-2">
        <ul class="nav nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item has-treeview">
            <router-link to="/adm_auditoria" class="nav-link">
              <h6 class="asidefix"><i class="nav-icon fas fa-search"></i>Auditoria</h6>
            </router-link>
          </li>
        </ul>
      </nav>
      <nav class="mt-2">
        <ul class="nav nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item has-treeview">
            <router-link to="/permisos" class="nav-link">
              <h6 class="asidefix"><i class="nav-icon fas fa-lock"></i>Permisos</h6>
            </router-link>
          </li>
        </ul>
      </nav>
  </aside>
  @endcan
</div>
<!-- Main Footer -->
<footer class="main-footer no-print">
  <!-- To the right -->
  <div class="float-right d-none d-sm-inline">
    Version 0.0.1
  </div>
  <!-- Default to the left -->
  <strong>Copyright &copy; 2021 <a href="/">Venezolana de Industria Tecnológica C.A</a></strong> RIF: G-20009381-1 - Todos los derechos reservados.
</footer>
<script>
  @auth
    window.user = @json(auth()->user())
  @endauth
</script>
<script src="/js/app.js"></script>
</body>
</html>
