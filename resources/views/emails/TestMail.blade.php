<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title></title>
</head>
<body>
    <h3> SOLICITUD DE PIEZAS ALMACEN:{{ $details['almacen'] }} </h3>
    <p>El tecnico {{ $details['tecnico'] }} solicita las siguientes piezas para el diagnostico: {{ $details['numero_caso'] }}</p>
    <ol>
        @foreach($details['piezas'] as $pieza)
            <li>{{ $pieza }}</li>
        @endforeach
    </ol>
</body>
</html>