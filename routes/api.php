<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResources(['user' => 'API\UserController']);
Route::put('perfil', 'API\UserController@updatePerfil');
Route::get('perfil', 'API\UserController@perfil');
Route::get('operador', 'API\UserController@operador');
Route::get('findUser', 'API\UserController@search');
Route::post('cneUser', 'API\UserController@cne');

Route::get('auditoria', 'API\AuditoriaController@index');

Route::apiResources(['pieces' => 'API\ppController']);
Route::apiResources(['call' => 'API\CallController']);
Route::apiResources(['equipos' => 'API\EquipoController']);

/* Movimiento de inventario */
Route::apiResources(['movimiento_inventario' => 'API\MovimientoInventarioController']);
Route::apiResources(['despacho_movimiento_inventario' => 'API\DespachoMovimientoInventarioController']);
Route::post('procesar_movimiento', 'API\MovimientoInventarioController@procesarMovimiento');
Route::post('despachar_movimiento', 'API\MovimientoInventarioController@despacharMovimiento');
Route::post('crear_reporte_inventario', 'API\MovimientoInventarioController@crearReporte');
Route::get('cargar_seriales/{numero_parte_id}', 'API\MovimientoInventarioController@indexSeriales');

Route::get('search_equipo','API\EquipoController@search');
Route::get('ptxgalpon','API\EquipoController@searchg');
Route::get('ptgalpones', 'API\EquipoController@ptGalpones');
Route::get('ptcategorias', 'API\EquipoController@ptCategorias');
Route::get('ptxcategoria','API\EquipoController@searchc');

Route::get('filtro', 'API\ppController@search');
Route::get('filtrog', 'API\ppController@searchg');
Route::post('busqueda', 'API\ppController@busquedaA');
Route::apiResources(['llamada' => 'API\RegistroLlamadaController']);
Route::get('getllamada/{id}/{tipo}','API\RegistroLlamadaController@getllamada');
Route::get('ppgalpones', 'API\ppController@ppGalpones');

Route::get('ppcategories', 'API\ppCategoriesController@ppCategories');
Route::get('ppdescriptions/{ppcategory}', 'API\ppCategoriesController@ppDescription');
Route::get('pporigins', 'API\ppCategoriesController@ppOrigins');

Route::apiResources(['client' => 'API\ClientController']);
Route::post('cneClient', 'API\ClientController@cne'); // quede en actualizar la info de los clientes que son empresa falta crear la ruta para clientes empresa
Route::post('searchClientP', 'API\ClientController@searchClientP');
Route::post('searchClientE', 'API\ClientController@searchClientE');
Route::post('storeCompany', 'API\ClientController@storeCompany');
Route::put('company/{id}', 'API\ClientController@updateCompany')->name('company.update');

Route::get('estados', 'API\VenezuelaUController@estados');
Route::get('municipios/{estado}', 'API\VenezuelaUController@municipios');
Route::get('parroquias/{municipio}', 'API\VenezuelaUController@parroquias');

Route::get('user_types', 'UserTypeController@index');
Route::get('support_channels', 'SupportChannelController@index');


// Modulo de casos
Route::ApiResources(['caso' => 'API\CasoController']);
Route::ApiResources(['gestionar'=>'API\GestionarCasoController']);
Route::get('vCaso/{nacionalidad}/{cedula}','API\GestionarCasoController@vCaso');
Route::get('jCaso/{tipo_emp}/{rif_emp}','API\GestionarCasoController@jCaso');
Route::get('nCaso/{numero_caso}','API\GestionarCasoController@nCaso');
Route::get('sCaso/{serial_equipo}','API\GestionarCasoController@sCaso');
Route::get('indexTecnico/{id}','API\CasoController@indexTecnico');
Route::put('confirmarRecepcion/{id}', 'API\CasoController@confirmarRecepcion');
Route::get('numeroCaso', 'API\CasoController@numeroCaso');

//Modulo de tecnicos
Route::ApiResources(['diagnostico'=>'API\DiagnosticoController']);
Route::get('casoTecnicos','API\DiagnosticoController@casoTecnicos');
Route::get('aceptarCasoTecnico/{id}','API\DiagnosticoController@aceptarCasoTecnico');
Route::post('piezasEliminar/{piezasEliminar}','API\DiagnosticoController@piezasEliminar');
Route::post('cerrarCaso','API\CasoController@cerrarCaso');
Route::get('fallas','API\CasoController@fallas');

//Modulo de presupuesto
Route::ApiResources(['presupuesto'=>'API\PresupuestoController']);
Route::get('diagnosticosTecnicos','API\PresupuestoController@diagnosticoTecnico');
Route::get('presupuestos','API\PresupuestoController@presupuestos');
Route::post('procesarPresupuesto','API\PresupuestoController@procesarPresupuesto');
Route::post('exportpdf', 'API\PresupuestoController@exportpdf');
Route::post('eliminarPiezas/{piezaEliminar}', 'API\PresupuestoController@eliminarPiezas');

//Recepcion
Route::get('buscar_caso/{filter}','API\GeneralReceptionController@buscar_caso');
Route::get('crearRecibo/{id}','API\GeneralReceptionController@crearRecibo');
//-->RECARGA DE DATOS
Route::get('searchTec/{support_channel}','API\GeneralReceptionController@searchTec');
Route::get('loadAuto_Case','API\GeneralReceptionController@loadAuto_Case');
//-->UPDATES DEL CONTROLADOR MODULOD DE RECEPCION
Route::put('asigTec/{id}/{check}','API\GeneralReceptionController@asigTec');
//-->FILTER
Route::get('filterAutoCase/{e}','API\GeneralReceptionController@filterAutoCase');
//-->CONTADORES DE INFOBOX
Route::get('autoload_waiting','API\GeneralReceptionController@autoload_waiting');
Route::get('autoload_asig','API\GeneralReceptionController@autoload_asig');
Route::get('autoload_reasig','API\GeneralReceptionController@autoload_reasig');
Route::get('autoload_closed','API\GeneralReceptionController@autoload_closed');
//-->COMENTARIOS PARA CASO
//Route::post('RecepComent/{c}','API\GeneralReceptionController@Recep_Coment');
Route::post('RecepComent','API\GeneralReceptionController@Recep_Coment');
Route::post('add_serial','API\GeneralReceptionController@Add_serial');
Route::post('add_comment','API\ComentarioController@store');



// Conexiones con el SGP
Route::post('sgp/comprobar/serial_equipo', 'API\sgpController@comprobarSE')->name('sgp/comprobar/serial_equipo');
Route::get('sgp/productos_terminados', 'API\sgpController@indexSerialesPT')->name('sgp/productos_terminados');
Route::get('sgp/orden_produccion', 'API\sgpController@indexOrdenP')->name('sgp/orden_produccion');
Route::get('sgp/modelos', 'API\sgpController@indexModelos')->name('sgp/modelos');
Route::get('sgp/modelos_versiones', 'API\sgpController@indexModelosV')->name('sgp/modelosV');
Route::get('sgp/piezas_modelos', 'API\sgpController@indexPiezasM')->name('sgp/piezas_modelos');
Route::get('sgp/numeros_partes', 'API\sgpController@indexNumerosP')->name('sgp/numeros_partes');
Route::get('sgp/caracteristicas_piezas', 'API\sgpController@indexCP')->name('sgp/caracteristicas_piezas');
Route::get('sgp/almacen', 'API\sgpController@indexAlmacenes')->name('sgp/almacen');
Route::get('sgp/precios', 'API\sgpController@Precios')->name('sgp/precios');
Route::get('sgp/piezasAlmacen/{almacen}','API\sgpController@piezaAlmacen');
Route::get('sgp/inventario/{slash}/{almacen}','API\sgpController@inventario');
Route::get('sgp/cantidad_pieza/{cantidadPieza}/{slash}/{almacen}','API\sgpController@cantidadPieza');
Route::get('sgp/menos_cantidad_pieza/{cantidadPieza}','API\sgpController@menosCantidadPieza');
Route::get('sgp/existenciaEtiquetadas','API\sgpController@existenciaEtiquetadas');
Route::get('sgp/existencias_produccion','API\sgpController@indexExistenciaProduccion');
Route::get('sgp/piezas','API\sgpController@indexPiezas');
Route::get('sgp/piezasEtiquetadas','API\sgpController@piezasEtiquetadas');
Route::get('indexPiezasEtiquetadas/{pieza}','API\sgpController@indexPiezasEtiquetadas');
Route::get('sgp/existencias_pp_inventario/{posicion}/{filtro}','API\sgpController@indexExistenciaPPInventario');
Route::get('sgp/indexEmpleados','API\sgpController@indexEmpleados');
Route::get('sgp/paletas','API\sgpController@indexPaleta');
Route::get('sgp/vehiculos','API\sgpController@indexVehiculos');
Route::get('sgp/conductores','API\sgpController@indexConductores');
Route::get('sgp/lote/{fecha}','API\sgpController@indexLote');
Route::get('sgp/usuarios_almacenistas', 'API\sgpController@indexUsuariosAlmacenistas');


Route::post('sgp/piezas_sugeridas', 'API\sgpController@buscarPiezasS')->name('sgp/piezas_sugeridas');

// Canales de compra
Route::ApiResources(['canal_compra' => 'API\PurchaseChannelController']);

// Modos de atención
Route::ApiResources(['modo_atencion' => 'API\AttentionModeController']);

// Estatus de los casos
Route::ApiResources(['case_status' => 'API\CaseStatusController']);

// Comentario de los casos
Route::ApiResources(['comentario' => 'API\ComentarioController']);
Route::post('comentarios_caso','API\ComentarioController@ComentariosCasos');
Route::post('casos/{client_id}','API\ComentarioController@casos');
Route::post('casosPagination','API\ComentarioController@casosPagination');

// Tipo de Comentario
Route::ApiResources(['tipo_comentario' => 'API\TipoComentarioController']);


//permisos
Route::get('permisos','API\PermisosController@index');
Route::get('search_P','API\PermisosController@filters');    
Route::get('modulos', 'API\PermisosController@listaModulos');
Route::post('registros', 'API\PermisosController@registro_P');
Route::post('delete_registros', 'API\PermisosController@destroy');
Route::get('list_delete_registros', 'API\PermisosController@show');
Route::get('modulosSearch', 'API\PermisosController@modulosSearch');
Route::get('middlewares/', 'API\PermisosController@middlewares');
Route::get('aprobarPermisos', 'API\PermisosController@aprobarPermisos');
Route::get('agregarPermisosAdmin', 'API\PermisosController@agregarPermisosAdmin');
