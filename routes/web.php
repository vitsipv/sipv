<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Mail\TestMail;
use App\Http\Controllers\MailsController;

Route::get('invoice',function(){
return view('invoice');
});

/*Route::get('htmlpdf','API\PresupuestoController@htmlpdf');
Route::get('generate','API\PresupuestoController@generate');*/

//Auth::routes();
Auth::routes(['register' => false]);
Route::post('exportpdf/{variable}', 'API\PresupuestoController@exportpdf')->name('invoice');
Route::get('/home', 'HomeController@index')->name('home');

//Route::get('{path}', 'HomeController@index')->where('path', '([A-z\d-\/_.]+)?'); // Ruta para el History Mode de Vue.
Route::get('{path}',"HomeController@index")->where( 'path', '([A-z\d_.]+)?' );


//Route::get('/send-mail', [MailsController::class,'Mensaje']);


/*Route::get('/send-mail', function() {
    $datails=[
        'title'=> 'Mail from Surfside Media',
        'body'=> 'This is aform testing email using smtp'
    ];
    \Mail::to('thistoprovethat@gmail.com')->send(new \App\Mail\TestMail($datails));

    echo "email has been sent";
});*/

//fqblwpkwuiyxvxou