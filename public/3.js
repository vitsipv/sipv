(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[3],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/presupuesto/PresupuestoComponent.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/presupuesto/PresupuestoComponent.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      text: null,
      casos: {},
      diagnosticos: {},
      presupuestos: {},
      almacenes: {},
      piezas: {},
      tipos: {},
      comentarios: {},
      inventarios: {},
      numero_caso: null,
      caseNumber: 0,
      cantidadPieza: 1,
      cantidadPiezaM: 1,
      PiezasEliminar: [],
      stockPiece: {},
      arregloStock: [],
      piece: false,
      piece2: false,
      manage: false,
      piece_id: 0,
      diagnostico: '',
      precios: null,
      pdf: null,
      Disabled: true,
      printMode: false,
      checked: null,
      update: false,
      procesar: true,
      actualizar: true,
      disable: false,
      datosCliente: null,
      documento: null,
      telefonos: null,
      correo: null,
      direccion: null,
      analista_presupuesto: null,
      date: null,
      dia: null,
      mes: null,
      año: null,
      variable: null,
      vista: null,
      dom: true,
      iva_id: null,
      form: new Form({
        id: null,
        tecnico_id: null,
        numero_caso: null,
        arregloStock: [],
        PiezasEliminar: [],
        pieza: {},
        presupuesto: null,
        condiciones: null
      })
    };
  },
  created: function created() {
    var _this = this;

    Fire.$on('AfterCreate', function () {
      _this.loadPresupuestos();

      _this.loadCasoComentarios();
    });
    axios.get("api/perfil").then(function (_ref) {
      var data = _ref.data;
      return _this.analista_presupuesto = data.name;
    });
  },
  mounted: function mounted() {
    this.loadCasosDiagnosticos();
    this.loadAlmacenes();
    this.loadPreciosProductos();
    this.loadPresupuestos();
    this.loadTiposComentarios();
  },
  methods: {
    getResults: function getResults() {
      var _this2 = this;

      var page = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
      axios.get('/api/presupuestos?page=' + page).then(function (response) {
        _this2.presupuestos = response.data;
      });
    },
    getResults2: function getResults2() {
      var _this3 = this;

      var page = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
      axios.get('/api/diagnosticosTecnicos?page=' + page).then(function (response) {
        _this3.diagnosticos = response.data;
      });
    },
    crearPresupuesto: function crearPresupuesto() {
      var _this4 = this;

      this.$Progress.start();
      this.$Progress.finish();
      this.form.post('api/presupuesto').then(function () {
        Fire.$emit('AfterCreate');
        swal('Presupuesto!', 'Creado con éxito.', 'success').then(function () {
          _this4.AtrasMD();
        });
      })["catch"](function () {
        _this4.$Progress.fail();

        toast({
          type: 'warning',
          title: 'Verifique los datos ingresados.'
        });
      });
    },
    crearComentario: function crearComentario() {
      var _this5 = this;

      this.$Progress.start();
      this.form.post('api/comentario').then(function (response) {
        // Validacion exitosa
        _this5.$refs['addComentario'].hide();

        swal('¡Creación exitosa!', 'Comentario creado con éxito.', 'success');
        _this5.form.tipos = null;
        _this5.form.comentario = null;

        _this5.$Progress.finish();

        Fire.$emit('AfterCreate'); // Funcion para recargar nuevos datos en la pagina sin refrescar
      })["catch"](function () {
        //Validacion con error
        _this5.$Progress.fail();

        toast({
          type: 'warning',
          title: 'Verifique los datos ingresados.'
        });
      });
    },
    updatePresupuesto: function updatePresupuesto() {
      var _this6 = this;

      this.$Progress.start();
      this.form.put('api/presupuesto/' + this.form.id).then(function () {
        // Validacion exitosa
        _this6.$Progress.finish(); // Funcion para recargar nuevos datos en la pagina sin refrescar


        Fire.$emit('AfterCreate');
        swal('¡Editado!', 'La informacion ha sido editada.', 'success').then(function () {
          _this6.Disabled = true;
          _this6.manage = false;
          _this6.update = false;

          _this6.quitarPieza();
        });
      })["catch"](function () {
        // Validacion con error
        _this6.$Progress.fail();

        toast({
          type: 'error',
          title: 'Verifique los datos ingresados.'
        });
      });
      console.log(this.form.PiezasEliminar);

      if (this.form.PiezasEliminar.length > 0) {
        console.log('CANTIDAD DE PIEZAS MAYOR A CERO');
        console.log(this.form.PiezasEliminar);
        this.form.post('api/eliminarPiezas/' + this.form.PiezasEliminar).then(function () {
          console.log('THEN');
          console.log('this.form.PiezasEliminar');
          _this6.PiezasEliminar = [];
          _this6.form.PiezasEliminar = [];
        })["catch"](function () {
          // Validacion con error
          _this6.form.PiezasEliminar = [];
          _this6.PiezasEliminar = [];
        });
      }
    },
    cerrar: function cerrar() {
      this.$refs['pdf'].hide();
    },
    printActivar: function printActivar(pdf) {
      $('#diagnostico').modal('show');
      this.fechaActual();
    },
    print: function print() {
      window.print();
    },
    exportpdf: function exportpdf(presupuesto) {
      var _this7 = this;

      this.form.fill(presupuesto);

      if (presupuesto.empresa == null) {
        this.form.numero_presupuesto = presupuesto.numero_presupuesto;
        this.form.datosCliente = presupuesto.cliente.nombres + ' ' + presupuesto.cliente.apellidos;
        this.form.documento = presupuesto.cliente.cedula;
        this.form.telefonos = presupuesto.cliente.tlf1 + ' / ' + presupuesto.cliente.tlf2;
        this.form.correo = presupuesto.cliente.email1;
        this.form.direccion = presupuesto.cliente.direccion;
        this.form.date = presupuesto.updated_at;
        this.form.iva_id = presupuesto.iva_id;
        this.arregloStock = presupuesto.detalles_presupuesto;
        this.form.analista_presupuesto = this.analista_presupuesto;
        this.form.arregloStock = this.arregloStock;
        this.form.modelo = presupuesto.caso.modelo_equipo;
      }

      if (presupuesto.cliente == null) {
        this.form.datosCliente = presupuesto.empresa.nombre_emp;
        this.form.numero_presupuesto = presupuesto.numero_presupuesto;
        this.form.documento = presupuesto.empresa.rif_emp;
        this.form.telefonos = presupuesto.empresa.tlf1_emp + ' / ' + presupuesto.empresa.tlf2_emp;
        this.form.correo = presupuesto.empresa.email_emp;
        this.form.direccion = presupuesto.empresa.direccion_emp;
        this.form.date = presupuesto.updated_at;
        this.form.iva_id = presupuesto.iva_id;
        this.arregloStock = presupuesto.detalles_presupuesto;
        this.form.analista_presupuesto = this.analista_presupuesto;
        this.form.arregloStock = this.arregloStock;
        this.form.modelo = presupuesto.caso.modelo_equipo;
      }

      this.form.post('/api/exportpdf').then(function (response) {
        _this7.vista = response.data;
      });
      this.dom = false;
    },
    fechaActual: function fechaActual() {
      var f = new Date();
      this.dia = f.getDate();
      this.mes = f.getMonth();
      this.año = f.getFullYear();
    },
    procesarPresupuesto: function procesarPresupuesto() {
      var _this8 = this;

      this.$Progress.start();
      this.form.post('api/procesarPresupuesto').then(function () {
        // Validacion exitosa
        _this8.$Progress.finish(); // Funcion para recargar nuevos datos en la pagina sin refrescar


        Fire.$emit('AfterCreate');
        swal('Procesado!', 'La presupuesto ha sido procesado.', 'success').then(function () {
          _this8.AtrasMP();
        });
      })["catch"](function () {
        // Validacion con error
        _this8.$Progress.fail();

        toast({
          type: 'error',
          title: 'Verifique los datos ingresados.'
        });
      });
    },
    NuevoComentarioModal: function NuevoComentarioModal() {
      $('#addComentario').modal('show');
    },
    gestionarPiezas: function gestionarPiezas() {
      this.manage = true;
    },
    gestionar: function gestionar() {
      this.manage = true;
      this.Disabled = false;
      this.update = true;
      this.disable = false;
    },
    diagnosticoModal: function diagnosticoModal(diagnostico) {
      this.form.numero_caso = diagnostico.numero_caso;
      this.form.fill(diagnostico);

      if (diagnostico.empresa == null) {
        this.form.datosCliente = diagnostico.cliente.nombres + ' ' + diagnostico.cliente.apellidos;
      }

      if (diagnostico.cliente == null) {
        this.form.datosCliente = diagnostico.empresa.nombre_emp;
      }

      this.form.modelo_equipo = diagnostico.modelo_equipo;
      this.form.serial_equipo = diagnostico.serial_equipo;
      this.arregloStock = diagnostico.detalles_diagnostico;
      this.form.arregloStock = this.arregloStock;

      for (var i = 0; i < this.arregloStock.length; i += 1) {
        this.arregloStock[i].id = i;
      }

      this.piece2 = true;
      $('#diagnostico').modal('show');
    },
    presupuestoModal: function presupuestoModal(presupuesto) {
      if (presupuesto.status_id == 3) {
        this.procesar = true;
        this.actualizar = true;
        this.disable = true;
      }

      this.Disabled = true;
      this.form.numero_presupuesto = presupuesto.numero_presupuesto;
      this.form.fill(presupuesto);

      if (presupuesto.empresa == null) {
        this.form.datosCliente = presupuesto.cliente.nombres + ' ' + presupuesto.cliente.apellidos;
      }

      if (presupuesto.cliente == null) {
        this.form.datosCliente = presupuesto.empresa.nombre_emp;
      }

      this.form.modelo_equipo = presupuesto.caso.modelo_equipo;
      this.form.PiezasEliminar = this.PiezasEliminar;
      this.form.serial_equipo = presupuesto.caso.serial_equipo;
      this.form.condiciones = false;
      this.arregloStock = presupuesto.detalles_presupuesto;
      this.form.arregloStock = this.arregloStock;

      for (var i = 0; i < this.arregloStock.length; i += 1) {
        this.arregloStock[i].idObj = i;
      }

      console.log(this.arregloStock);

      if (this.arregloStock.length == 0) {
        console.log('array vacio');
        this.piece2 = false;
      } else {
        this.piece2 = true;
      }

      if (presupuesto.condiciones == 'PC') {
        console.log('Condiciones PC');
        this.form.condiciones = false; //this.form.condiciones = 'PC';

        console.log(this.form.condiciones);
      } else {
        console.log('Condiciones SERVIDOR');
        this.form.condiciones = true; //this.form.condiciones = 'SERVIDOR';

        console.log(this.form.condiciones);
      }

      console.log(this.form.condiciones);
      $('#presupuesto').modal('show');
    },
    ComentarioModalDiagnostico: function ComentarioModalDiagnostico(diagnostico) {
      this.numero_caso = diagnostico.numero_caso;
      this.form.numero_caso = diagnostico.numero_caso;
      $('#Comentario').modal('show');
      this.loadCasoComentarios();
    },
    ComentarioModalPresupuesto: function ComentarioModalPresupuesto(presupuesto) {
      this.numero_caso = presupuesto.numero_caso;
      this.form.numero_caso = presupuesto.numero_caso;
      $('#Comentario').modal('show');
      this.loadCasoComentarios();
    },
    AtrasMD: function AtrasMD() {
      this.$refs['diagnostico'].hide();
      this.arregloStock = [];
      this.form.arregloStock = [];
      this.inventarios = {};
      this.stockPiece = {};
      this.piece2 = false;
      this.piece = false;
      this.manage = false;
      this.procesar = true;
      this.update = false;
      this.loadCasosDiagnosticos();
    },
    AtrasMP: function AtrasMP() {
      this.$refs['presupuesto'].hide();
      this.arregloStock = [];
      this.form.arregloStock = [];
      this.inventarios = {};
      this.stockPiece = {};
      this.piece2 = false;
      this.piece = false;
      this.manage = false;
      this.update = false;
      this.procesar = true;
      this.disable = false;
      this.loadPresupuestos();
    },
    AtrasMC: function AtrasMC() {
      this.$refs['Comentario'].hide();
    },
    salir: function salir() {
      this.$refs['addComentario'].hide();
      this.form.tipos = null;
      this.form.comentario = null;
    },
    cantidadPiecePlus: function cantidadPiecePlus() {
      var _this9 = this;

      var slash2 = "";
      slash2 = this.form.pieza;
      var slash = slash2.replace("/", "|");
      axios.get('/api/sgp/cantidad_pieza/' + this.cantidadPieza + '/' + slash + '/' + this.form.almacen).then(function (_ref2) {
        var data = _ref2.data;
        return _this9.cantidadPieza = data;
      });
    },
    cantidadPieceMinus: function cantidadPieceMinus() {
      var _this10 = this;

      axios.get('/api/sgp/menos_cantidad_pieza/' + this.cantidadPieza).then(function (_ref3) {
        var data = _ref3.data;
        return _this10.cantidadPieza = data;
      });
    },
    cantidadPiecePlusM: function cantidadPiecePlusM(arreglo) {
      var _this11 = this;

      var idObj = arreglo.idObj;
      console.log('arreglo.idObj');
      console.log(arreglo.idObj);
      console.log('arreglo.cantidad_piezas');
      console.log(arreglo.cant_piezas);
      this.cantidadPiezaM = arreglo.cant_piezas;
      console.log('this.cantidadPiezaM');
      console.log(this.cantidadPiezaM);
      this.pieza = arreglo.piezas + ' - ' + arreglo.np_piezas;
      this.almacen = arreglo.almacenes;
      var slash2 = "";
      slash2 = this.pieza;
      var slash = slash2.replace("/", "|");
      axios.get('/api/sgp/cantidad_pieza/' + this.cantidadPiezaM + '/' + slash + '/' + this.almacen).then(function (_ref4) {
        var data = _ref4.data;
        return _this11.arregloStock[idObj].cant_piezas = data;
      });
      this.loadAlmacenes();
    },
    cantidadPieceMinusM: function cantidadPieceMinusM(arreglo) {
      var idObj = arreglo.idObj;

      if (arreglo.cant_piezas > 1) {
        this.arregloStock[idObj].cant_piezas = this.arregloStock[idObj].cant_piezas - 1;
        this.loadAlmacenes();
      }
    },
    agregarPieza: function agregarPieza() {
      console.log("MEMES")
      this.stockPiece = {};
      this.stockPiece.piezas = this.inventarios[0].pieza.nombre;
      this.stockPiece.np_piezas = this.inventarios[0].numero_parte.pn;
      this.stockPiece.cant_piezas = this.cantidadPieza;
      this.stockPiece.almacenes = this.form.almacen;
      this.stockPiece.precio_piezas = this.inventarios[0].precio.precio;
      this.stockPiece.numero_presupuesto = this.form.numero_presupuesto;
      this.stockPiece.idObj = this.piece_id;
      this.arregloStock.push(this.stockPiece);
      this.form.arregloStock = this.arregloStock;
      this.piece_id = this.piece_id + 1;

      for (var i = 0; i < this.arregloStock.length; i += 1) {
        this.arregloStock[i].idObj = i;
      }

      this.inventarios = {};
      this.stockPiece = {};
      this.cantidadPieza = 1;
      this.piece2 = true;
      this.piece = false;
    },
    quitarPieza: function quitarPieza() {
      this.inventarios = {};
      this.cantidadPieza = 1;
      this.piece = false;
    },
    quitarPiezaSelected: function quitarPiezaSelected(arreglo) {
      console.log("Quitar pieza");
      var idObj = arreglo.idObj;

      if (arreglo.id) {
        console.log("Quitar pieza Eliminar");
        this.PiezasEliminar.push(arreglo.id);
        this.form.PiezasEliminar = this.PiezasEliminar;
      }

      this.arregloStock.splice(idObj, 1); //función que elimina un elemento de un array de objetos.

      for (var i = 0; i < this.arregloStock.length; i += 1) {
        this.arregloStock[i].idObj = i;
      }

      this.form.arregloStock = this.arregloStock;

      if (Object.keys(this.arregloStock).length === 0) {
        this.piece2 = false;
        this.piece_id = 0;
        return this.arregloStock = [];
      }

      return this.arregloStock;
    },
    loadCasosDiagnosticos: function loadCasosDiagnosticos() {
      var _this12 = this;

      axios.get('/api/diagnosticosTecnicos/').then(function (_ref5) {
        var data = _ref5.data;
        return _this12.diagnosticos = data;
      });
    },
    loadPresupuestos: function loadPresupuestos() {
      var _this13 = this;

      axios.get('/api/presupuestos/').then(function (_ref6) {
        var data = _ref6.data;
        return _this13.presupuestos = data;
      });
    },
    loadPreciosProductos: function loadPreciosProductos() {
      var _this14 = this;

      axios.get('/api/sgp/precios/').then(function (_ref7) {
        var data = _ref7.data;
        return _this14.precios = data;
      });
    },
    loadAlmacenes: function loadAlmacenes() {
      var _this15 = this;

      axios.get('api/sgp/almacen').then(function (_ref8) {
        var data = _ref8.data;
        return _this15.almacenes = data;
      });
    },
    loadCasoComentarios: function loadCasoComentarios() {
      var _this16 = this;

      this.form.post("api/comentarios_caso").then(function (_ref9) {
        var data = _ref9.data;
        return _this16.comentarios = data;
      });
    },
    loadTiposComentarios: function loadTiposComentarios() {
      var _this17 = this;

      axios.get("api/tipo_comentario").then(function (_ref10) {
        var data = _ref10.data;
        return _this17.tipos = data;
      });
    },
    getResultsComentarios: function getResultsComentarios() {
      var _this18 = this;

      var page = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
      this.form.post('api/comentarios_caso?page=' + page).then(function (response) {
        _this18.comentarios = response.data;
      });
    },
    onChange: function onChange() {
      var _this19 = this;

      axios.get('api/sgp/piezasAlmacen/' + this.form.almacen).then(function (_ref11) {
        var data = _ref11.data;
        return _this19.piezas = data;
      });
      $('#pieza').attr('disabled', false);
    },
    onChange2: function onChange2() {
      var _this20 = this;

      var slash2 = "";
      slash2 = this.form.pieza;
      var slash = slash2.replace("/", "|");
      console.log(slash);
      axios.get('api/sgp/inventario/' + slash + '/' + this.form.almacen).then(function (_ref12) {
        var data = _ref12.data;
        return _this20.inventarios = data;
      });
      this.piece = true;
      this.cantidadPieza = 1;
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/presupuesto/PresupuestoComponent.vue?vue&type=template&id=70c030d1&":
/*!***********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/presupuesto/PresupuestoComponent.vue?vue&type=template&id=70c030d1& ***!
  \***********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "div",
      {
        directives: [
          { name: "show", rawName: "v-show", value: _vm.dom, expression: "dom" }
        ],
        staticClass: "row"
      },
      [
        _c(
          "div",
          {
            directives: [
              {
                name: "b-tooltip",
                rawName: "v-b-tooltip.html",
                modifiers: { html: true }
              }
            ],
            staticClass: "col-12 col-sm-6 col-md-2",
            attrs: {
              title:
                "<strong>Nota:</strong> Casos asignados a un operador técnico para su atención."
            }
          },
          [
            _c(
              "div",
              { staticClass: "info-box" },
              [
                _vm._m(0),
                _vm._v(" "),
                _c("b-col", [
                  _c("div", { staticClass: "info-box-content" }, [
                    _c("span", { staticClass: "info-box-text" }, [
                      _vm._v("Asignados")
                    ]),
                    _vm._v(" "),
                    _c("span", { staticClass: "info-box-number" }, [
                      _vm._v(_vm._s(0))
                    ])
                  ])
                ])
              ],
              1
            )
          ]
        ),
        _vm._v(" "),
        _c(
          "div",
          {
            directives: [
              {
                name: "b-tooltip",
                rawName: "v-b-tooltip.html",
                modifiers: { html: true }
              }
            ],
            staticClass: "col-12 col-sm-6 col-md-2",
            attrs: {
              title:
                "<strong>Nota:</strong> Es el caso en el cual un equipo cuenta con una garantía valida."
            }
          },
          [
            _c(
              "div",
              { staticClass: "info-box mb-4" },
              [
                _vm._m(1),
                _vm._v(" "),
                _c("b-col", [
                  _c("div", { staticClass: "info-box-content" }, [
                    _c("span", { staticClass: "info-box-text" }, [
                      _vm._v("Con garantía")
                    ]),
                    _vm._v(" "),
                    _c("span", { staticClass: "info-box-number" }, [
                      _vm._v("0")
                    ])
                  ])
                ])
              ],
              1
            )
          ]
        ),
        _vm._v(" "),
        _c(
          "div",
          {
            directives: [
              {
                name: "b-tooltip",
                rawName: "v-b-tooltip.html",
                modifiers: { html: true }
              }
            ],
            staticClass: "col-12 col-sm-6 col-md-2",
            attrs: {
              title:
                "<strong>Nota:</strong> Es el caso en el cual un equipo pasa el tiempo definido en su garantía."
            }
          },
          [
            _c(
              "div",
              { staticClass: "info-box mb-3" },
              [
                _vm._m(2),
                _vm._v(" "),
                _c("b-col", [
                  _c("div", { staticClass: "info-box-content" }, [
                    _c("span", { staticClass: "info-box-text" }, [
                      _vm._v("Sin garantía")
                    ]),
                    _vm._v(" "),
                    _c("span", { staticClass: "info-box-number" }, [
                      _vm._v("0")
                    ])
                  ])
                ])
              ],
              1
            )
          ]
        ),
        _vm._v(" "),
        _c("div", { staticClass: "clearfix hidden-md-up" }),
        _vm._v(" "),
        _c(
          "div",
          {
            directives: [
              {
                name: "b-tooltip",
                rawName: "v-b-tooltip.html",
                modifiers: { html: true }
              }
            ],
            staticClass: "col-12 col-sm-6 col-md-2",
            attrs: {
              title:
                "<strong>Nota:</strong> Casos en los que el presupuesto ha sido pagado."
            }
          },
          [
            _c(
              "div",
              { staticClass: "info-box mb-3" },
              [
                _vm._m(3),
                _vm._v(" "),
                _c("b-col", [
                  _c("div", { staticClass: "info-box-content" }, [
                    _c("span", { staticClass: "info-box-text" }, [
                      _vm._v("Pagado")
                    ]),
                    _vm._v(" "),
                    _c("span", { staticClass: "info-box-number" }, [
                      _vm._v("0")
                    ])
                  ])
                ])
              ],
              1
            )
          ]
        ),
        _vm._v(" "),
        _c(
          "div",
          {
            directives: [
              {
                name: "b-tooltip",
                rawName: "v-b-tooltip.html",
                modifiers: { html: true }
              }
            ],
            staticClass: "col-12 col-sm-6 col-md-2",
            attrs: {
              title:
                "<strong>Nota:</strong> Casos en los que el tiempo estipulado en el presupuesto se ha expirado."
            }
          },
          [
            _c(
              "div",
              { staticClass: "info-box mb-3" },
              [
                _vm._m(4),
                _vm._v(" "),
                _c("b-col", [
                  _c("div", { staticClass: "info-box-content" }, [
                    _c("span", { staticClass: "info-box-text" }, [
                      _vm._v("Expirado")
                    ]),
                    _vm._v(" "),
                    _c("span", { staticClass: "info-box-number" }, [
                      _vm._v("0")
                    ])
                  ])
                ])
              ],
              1
            )
          ]
        ),
        _vm._v(" "),
        _c(
          "div",
          {
            directives: [
              {
                name: "b-tooltip",
                rawName: "v-b-tooltip.html",
                modifiers: { html: true }
              }
            ],
            staticClass: "col-12 col-sm-6 col-md-2",
            attrs: {
              title:
                "<strong>Nota:</strong> Casos en los que el tiempo de atención ha sobrepasado el estipulado."
            }
          },
          [
            _c(
              "div",
              { staticClass: "info-box mb-3" },
              [
                _vm._m(5),
                _vm._v(" "),
                _c("b-col", [
                  _c("div", { staticClass: "info-box-content" }, [
                    _c("span", { staticClass: "info-box-text" }, [
                      _vm._v("Sin atención")
                    ]),
                    _vm._v(" "),
                    _c("span", { staticClass: "info-box-number" }, [
                      _vm._v("0")
                    ])
                  ])
                ])
              ],
              1
            )
          ]
        )
      ]
    ),
    _vm._v(" "),
    _c(
      "form",
      {
        directives: [
          { name: "show", rawName: "v-show", value: _vm.dom, expression: "dom" }
        ],
        staticClass: "form-horizontal"
      },
      [
        _c("div", { staticClass: "card" }, [
          _vm._m(6),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "card-body p-0", staticStyle: { display: "block" } },
            [
              _c("div", { staticClass: "table-responsive" }, [
                _c("table", { staticClass: "table m-0" }, [
                  _vm._m(7),
                  _vm._v(" "),
                  _c(
                    "tbody",
                    _vm._l(_vm.diagnosticos.data, function(diagnostico) {
                      return _c(
                        "tr",
                        { key: diagnostico.id },
                        [
                          diagnostico.diagnostico_caso.length
                            ? [
                                _c("td", [
                                  _vm._v(_vm._s(diagnostico.numero_caso))
                                ]),
                                _vm._v(" "),
                                _c("td", [
                                  _vm._v(_vm._s(diagnostico.serial_equipo))
                                ]),
                                _vm._v(" "),
                                _c("td", [
                                  _vm._v(_vm._s(diagnostico.modelo_equipo))
                                ]),
                                _vm._v(" "),
                                _c("td", [
                                  _vm._v(
                                    _vm._s(
                                      diagnostico.diagnostico_caso[0]
                                        .diagnostico_caso
                                    )
                                  )
                                ]),
                                _vm._v(" "),
                                diagnostico.estado_caso_id == 2
                                  ? _c(
                                      "div",
                                      {
                                        staticClass:
                                          "d-flex justify-content-center"
                                      },
                                      [_vm._m(8, true)]
                                    )
                                  : _vm._e(),
                                _vm._v(" "),
                                diagnostico.estado_caso_id == 1
                                  ? _c(
                                      "div",
                                      {
                                        staticClass:
                                          "d-flex justify-content-center"
                                      },
                                      [_vm._m(9, true)]
                                    )
                                  : _vm._e(),
                                _vm._v(" "),
                                _c("td", [
                                  _vm._v(_vm._s(diagnostico.updated_at))
                                ]),
                                _vm._v(" "),
                                _c(
                                  "button",
                                  {
                                    directives: [
                                      {
                                        name: "b-modal",
                                        rawName: "v-b-modal.diagnostico",
                                        modifiers: { diagnostico: true }
                                      }
                                    ],
                                    staticClass: "btn btn-primary",
                                    attrs: { type: "button" },
                                    on: {
                                      click: function($event) {
                                        return _vm.diagnosticoModal(diagnostico)
                                      }
                                    }
                                  },
                                  [
                                    _c("i", {
                                      staticClass: "fas fa-file-invoice-dollar"
                                    })
                                  ]
                                ),
                                _vm._v(" "),
                                _c("td", [
                                  _vm._v(
                                    "\n<<<<<<< HEAD\n                      "
                                  ),
                                  _c(
                                    "a",
                                    {
                                      directives: [
                                        {
                                          name: "b-modal",
                                          rawName: "v-b-modal.Comentario",
                                          modifiers: { Comentario: true }
                                        }
                                      ],
                                      staticClass: "btn btn-primary",
                                      attrs: { href: "#" },
                                      on: {
                                        click: function($event) {
                                          return _vm.ComentarioModalDiagnostico(
                                            diagnostico
                                          )
                                        }
                                      }
                                    },
                                    [_c("i", { staticClass: "fas fa-comment" })]
                                  ),
                                  _vm._v("\n=======\n                      "),
                                  _c(
                                    "a",
                                    {
                                      directives: [
                                        {
                                          name: "b-modal",
                                          rawName: "v-b-modal.Comentario",
                                          modifiers: { Comentario: true }
                                        }
                                      ],
                                      attrs: { href: "#" },
                                      on: {
                                        click: function($event) {
                                          return _vm.ComentarioModalDiagnostico(
                                            diagnostico
                                          )
                                        }
                                      }
                                    },
                                    [
                                      _c("i", {
                                        staticClass:
                                          "btn btn-primary fas fa-comment"
                                      })
                                    ]
                                  ),
                                  _vm._v(
                                    "\n>>>>>>> 83de8bb5afda2ac1df443182a00ec13dc5109ea1\n                    "
                                  )
                                ])
                              ]
                            : _vm._e()
                        ],
                        2
                      )
                    }),
                    0
                  )
                ])
              ])
            ]
          ),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass: "card-footer clearfix",
              staticStyle: { display: "block" }
            },
            [
              _c(
                "pagination",
                {
                  attrs: { data: _vm.diagnosticos },
                  on: { "pagination-change-page": _vm.getResults2 }
                },
                [
                  _c(
                    "span",
                    { attrs: { slot: "prev-nav" }, slot: "prev-nav" },
                    [_c("i", { staticClass: "fas fa-arrow-left" })]
                  ),
                  _vm._v(" "),
                  _c(
                    "span",
                    { attrs: { slot: "next-nav" }, slot: "next-nav" },
                    [_c("i", { staticClass: "fas fa-arrow-right" })]
                  )
                ]
              )
            ],
            1
          )
        ]),
        _vm._v(" "),
        _c(
          "b-modal",
          {
            ref: "diagnostico",
            attrs: {
              id: "diagnostico",
              centered: "",
              size: "xl",
              "hide-footer": "",
              "no-close-on-backdrop": ""
            },
            scopedSlots: _vm._u([
              {
                key: "modal-header",
                fn: function(ref) {
                  var close = ref.close
                  return [
                    _c(
                      "h5",
                      {
                        staticClass: "modal-title",
                        attrs: { id: "BuscarCasoLabel" }
                      },
                      [
                        _vm._v("Numero de Caso: "),
                        _c("b", [_vm._v(_vm._s(_vm.form.numero_caso))]),
                        _vm._v(" - "),
                        _c("b", [_vm._v(_vm._s(_vm.form.datosCliente))])
                      ]
                    )
                  ]
                }
              }
            ])
          },
          [
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c(
                "div",
                { staticClass: "col-sm-8" },
                [
                  _c("div", { staticClass: "form-group" }, [
                    _c("label", [_vm._v("Serial del equipo")]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.form.serial_equipo,
                          expression: "form.serial_equipo"
                        }
                      ],
                      staticClass: "form-control",
                      class: {
                        "is-invalid": _vm.form.errors.has("serial_equipo")
                      },
                      attrs: {
                        disabled: "",
                        type: "text",
                        readonly: "readonly",
                        placeholder: "Serial del Equipo"
                      },
                      domProps: { value: _vm.form.serial_equipo },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            _vm.form,
                            "serial_equipo",
                            $event.target.value
                          )
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      directives: [
                        {
                          name: "show",
                          rawName: "v-show",
                          value: _vm.manage,
                          expression: "manage"
                        }
                      ]
                    },
                    [
                      _c("div", { staticClass: "form-group" }, [
                        _c("label", [_vm._v("Seleccione un Almacen")]),
                        _vm._v(" "),
                        _c(
                          "select",
                          {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.almacen,
                                expression: "form.almacen"
                              }
                            ],
                            staticClass: "custom-select",
                            on: {
                              change: [
                                function($event) {
                                  var $$selectedVal = Array.prototype.filter
                                    .call($event.target.options, function(o) {
                                      return o.selected
                                    })
                                    .map(function(o) {
                                      var val =
                                        "_value" in o ? o._value : o.value
                                      return val
                                    })
                                  _vm.$set(
                                    _vm.form,
                                    "almacen",
                                    $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  )
                                },
                                function($event) {
                                  return _vm.onChange()
                                }
                              ]
                            }
                          },
                          [
                            _c(
                              "option",
                              {
                                attrs: { disabled: "true" },
                                domProps: { value: null }
                              },
                              [_vm._v("Seleccione un almacén")]
                            ),
                            _vm._v(" "),
                            _vm._l(_vm.almacenes, function(almacen) {
                              return _c("option", { key: almacen.id }, [
                                _vm._v(_vm._s(almacen.nombre))
                              ])
                            })
                          ],
                          2
                        )
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "form-group" },
                        [
                          _c("label", [_vm._v("Seleccione una Pieza")]),
                          _vm._v(" "),
                          _c(
                            "select",
                            {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.form.pieza,
                                  expression: "form.pieza"
                                }
                              ],
                              staticClass: "form-control custom-select",
                              class: {
                                "is-invalid": _vm.form.errors.has("pieza")
                              },
                              attrs: { id: "pieza", disabled: "true" },
                              on: {
                                change: [
                                  function($event) {
                                    var $$selectedVal = Array.prototype.filter
                                      .call($event.target.options, function(o) {
                                        return o.selected
                                      })
                                      .map(function(o) {
                                        var val =
                                          "_value" in o ? o._value : o.value
                                        return val
                                      })
                                    _vm.$set(
                                      _vm.form,
                                      "pieza",
                                      $event.target.multiple
                                        ? $$selectedVal
                                        : $$selectedVal[0]
                                    )
                                  },
                                  function($event) {
                                    return _vm.onChange2()
                                  }
                                ]
                              }
                            },
                            _vm._l(_vm.piezas, function(pieza) {
                              return pieza.pieza !== null
                                ? _c("option", { key: pieza.id }, [
                                    _vm._v(
                                      "\n                " +
                                        _vm._s(
                                          pieza.pieza.nombre +
                                            " - " +
                                            pieza.numero_parte.pn
                                        )
                                    )
                                  ])
                                : _vm._e()
                            }),
                            0
                          ),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "pieza" }
                          })
                        ],
                        1
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "table-responsive" }, [
                    _c(
                      "table",
                      { staticClass: "table table-striped table-light" },
                      [
                        _c(
                          "thead",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.piece,
                                expression: "piece"
                              }
                            ]
                          },
                          [
                            _c("tr", [
                              _c("th", { attrs: { scope: "col" } }, [
                                _vm._v("Pieza")
                              ]),
                              _vm._v(" "),
                              _c("th", { attrs: { scope: "col" } }, [
                                _vm._v("Numero de Parte")
                              ]),
                              _vm._v(" "),
                              _c("th", { attrs: { scope: "col" } }, [
                                _vm._v("Cantidad")
                              ]),
                              _vm._v(" "),
                              _c("th", { attrs: { scope: "col" } }, [
                                _vm._v("Opciones")
                              ])
                            ])
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "tbody",
                          _vm._l(_vm.inventarios, function(inventario) {
                            return _c("tr", { key: inventario.id }, [
                              _c("td", [
                                _vm._v(_vm._s(inventario.pieza.nombre))
                              ]),
                              _vm._v(" "),
                              _c("td", [
                                _vm._v(_vm._s(inventario.numero_parte.pn))
                              ]),
                              _vm._v(" "),
                              _c("td", [_vm._v(_vm._s(_vm.cantidadPieza))]),
                              _vm._v(" "),
                              _c(
                                "button",
                                {
                                  staticClass: "btn btn-primary",
                                  attrs: { type: "button" },
                                  on: {
                                    click: function($event) {
                                      return _vm.cantidadPiecePlus()
                                    }
                                  }
                                },
                                [_c("i", { staticClass: "fas fa-plus" })]
                              ),
                              _vm._v(" "),
                              _c(
                                "button",
                                {
                                  staticClass: "btn btn-secondary",
                                  attrs: { type: "button" },
                                  on: { click: _vm.cantidadPieceMinus }
                                },
                                [_c("i", { staticClass: "fa fa-minus" })]
                              ),
                              _vm._v(" "),
                              _c(
                                "button",
                                {
                                  staticClass: "btn btn-success",
                                  attrs: { type: "button" },
                                  on: { click: _vm.agregarPieza }
                                },
                                [_c("i", { staticClass: "fa fa-check" })]
                              ),
                              _vm._v(" "),
                              _c(
                                "button",
                                {
                                  staticClass: "btn btn-danger",
                                  attrs: { type: "button" },
                                  on: { click: _vm.quitarPieza }
                                },
                                [_c("i", { staticClass: "fa fa-times" })]
                              )
                            ])
                          }),
                          0
                        )
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c("alert-error", {
                    attrs: {
                      form: _vm.form,
                      message: "Se han encontrado algunos errores."
                    }
                  }),
                  _vm._v(" "),
                  _c("div", { staticClass: "table-responsive" }, [
                    _c(
                      "table",
                      { staticClass: "table table-striped table-light" },
                      [
                        _c(
                          "thead",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.piece2,
                                expression: "piece2"
                              }
                            ]
                          },
                          [
                            _c("tr", [
                              _c("th", { attrs: { scope: "col" } }, [
                                _vm._v("Pieza")
                              ]),
                              _vm._v(" "),
                              _c("th", { attrs: { scope: "col" } }, [
                                _vm._v("Numero de Parte")
                              ]),
                              _vm._v(" "),
                              _c("th", { attrs: { scope: "col" } }, [
                                _vm._v("Cantidad")
                              ]),
                              _vm._v(" "),
                              _c("th", { attrs: { scope: "col" } }, [
                                _vm._v("Precio Unit")
                              ]),
                              _vm._v(" "),
                              _c(
                                "th",
                                {
                                  directives: [
                                    {
                                      name: "show",
                                      rawName: "v-show",
                                      value: _vm.manage,
                                      expression: "manage"
                                    }
                                  ],
                                  attrs: { scope: "col" }
                                },
                                [_vm._v("Opciones")]
                              )
                            ])
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "tbody",
                          _vm._l(_vm.arregloStock, function(arreglo) {
                            return _c("tr", { key: arreglo.id }, [
                              _c("td", [
                                _c("span", [_vm._v(_vm._s(arreglo.piezas))])
                              ]),
                              _vm._v(" "),
                              _c("td", [
                                _c(
                                  "span",
                                  { staticClass: "badge badge-light" },
                                  [_vm._v(_vm._s(arreglo.np_piezas))]
                                )
                              ]),
                              _vm._v(" "),
                              _c("td", [
                                _c("span", [
                                  _vm._v(_vm._s(arreglo.cant_piezas))
                                ])
                              ]),
                              _vm._v(" "),
                              _c("td", [
                                _c("span", [
                                  _vm._v(_vm._s(arreglo.precio_piezas))
                                ])
                              ]),
                              _vm._v(" "),
                              _c(
                                "button",
                                {
                                  directives: [
                                    {
                                      name: "show",
                                      rawName: "v-show",
                                      value: _vm.manage,
                                      expression: "manage"
                                    }
                                  ],
                                  staticClass: "btn btn-danger",
                                  attrs: { type: "button" },
                                  on: {
                                    click: function($event) {
                                      return _vm.quitarPiezaSelected(arreglo)
                                    }
                                  }
                                },
                                [_c("i", { staticClass: "fa fa-times" })]
                              )
                            ])
                          }),
                          0
                        )
                      ]
                    )
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c("div", { staticClass: "col-sm-4" }, [
                _c("div", { staticClass: "form-group" }, [
                  _c("label", [_vm._v("Modelo del equipo")]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form.modelo_equipo,
                        expression: "form.modelo_equipo"
                      }
                    ],
                    staticClass: "form-control",
                    class: { "is-invalid": _vm.form.errors.has("cedula") },
                    attrs: {
                      disabled: "",
                      type: "text",
                      readonly: "readonly",
                      placeholder: "Modelo del equipo"
                    },
                    domProps: { value: _vm.form.modelo_equipo },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.form, "modelo_equipo", $event.target.value)
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _c("label", [_vm._v("Observaciones del presupuesto")]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "form-group" },
                  [
                    _c("b-form-textarea", {
                      class: {
                        "is-invalid": _vm.form.errors.has("presupuesto")
                      },
                      attrs: {
                        id: "textarea",
                        size: "1",
                        maxlength: "140",
                        placeholder: "Observación general del presupuesto...",
                        rows: "6",
                        "max-rows": "1"
                      },
                      model: {
                        value: _vm.form.presupuesto,
                        callback: function($$v) {
                          _vm.$set(_vm.form, "presupuesto", $$v)
                        },
                        expression: "form.presupuesto"
                      }
                    }),
                    _c("has-error", {
                      attrs: { form: _vm.form, field: "presupuesto" }
                    })
                  ],
                  1
                )
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "card-footer clearfix" }, [
              _c("div", { staticClass: "row" }, [
                _c("div", { staticClass: "col-sm-6" }, [
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-success",
                      attrs: { type: "submit" },
                      on: {
                        click: function($event) {
                          return _vm.crearPresupuesto()
                        }
                      }
                    },
                    [_vm._v("Crear Presupuesto")]
                  )
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "col-sm-6 d-flex justify-content-end" },
                  [
                    _c(
                      "button",
                      {
                        staticClass: "btn btn-outline-danger",
                        attrs: { type: "button", "data-dismiss": "modal" },
                        on: {
                          click: function($event) {
                            return _vm.AtrasMD()
                          }
                        }
                      },
                      [_vm._v("Salir")]
                    )
                  ]
                )
              ])
            ])
          ]
        )
      ],
      1
    ),
    _vm._v(" "),
    _c("div", {
      directives: [
        { name: "show", rawName: "v-show", value: !_vm.dom, expression: "!dom" }
      ],
      domProps: { innerHTML: _vm._s(this.vista) }
    }),
    _vm._v(" "),
    _c(
      "form",
      {
        directives: [
          { name: "show", rawName: "v-show", value: _vm.dom, expression: "dom" }
        ],
        staticClass: "form-horizontal"
      },
      [
        _c("div", { staticClass: "card" }, [
          _vm._m(10),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "card-body p-0", staticStyle: { display: "block" } },
            [
              _c("div", { staticClass: "table-responsive" }, [
                _c("table", { staticClass: "table m-0" }, [
                  _vm._m(11),
                  _vm._v(" "),
                  _c(
                    "tbody",
                    _vm._l(_vm.presupuestos.data, function(presupuesto) {
                      return _c("tr", { key: presupuesto.id }, [
                        _c("td", [
                          _vm._v(_vm._s(presupuesto.numero_presupuesto))
                        ]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(presupuesto.presupuesto))]),
                        _vm._v(" "),
                        presupuesto.status_id == 1
                          ? _c(
                              "div",
                              { staticClass: "d-flex justify-content-center" },
                              [_vm._m(12, true)]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        presupuesto.status_id == 3
                          ? _c(
                              "div",
                              { staticClass: "d-flex justify-content-center" },
                              [_vm._m(13, true)]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(presupuesto.updated_at))]),
                        _vm._v(" "),
                        presupuesto.status_id == 1
                          ? _c(
                              "button",
                              {
                                directives: [
                                  {
                                    name: "b-modal",
                                    rawName: "v-b-modal.presupuesto",
                                    modifiers: { presupuesto: true }
                                  }
                                ],
                                staticClass: "btn btn-primary",
                                attrs: { type: "button" },
                                on: {
                                  click: function($event) {
                                    return _vm.presupuestoModal(presupuesto)
                                  }
                                }
                              },
                              [
                                _c("i", {
                                  staticClass: "fas fa-file-invoice-dollar"
                                })
                              ]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        presupuesto.status_id == 3
                          ? _c(
                              "button",
                              {
                                directives: [
                                  {
                                    name: "b-modal",
                                    rawName: "v-b-modal.presupuesto",
                                    modifiers: { presupuesto: true }
                                  }
                                ],
                                staticClass: "btn btn-primary",
                                attrs: { type: "button" },
                                on: {
                                  click: function($event) {
                                    return _vm.presupuestoModal(presupuesto)
                                  }
                                }
                              },
                              [_c("i", { staticClass: "fas fa-search" })]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        presupuesto.status_id == 3
                          ? _c(
                              "button",
                              {
                                staticClass: "btn btn-danger",
                                attrs: { target: "_blank", type: "button" },
                                on: {
                                  click: function($event) {
                                    return _vm.exportpdf(presupuesto)
                                  }
                                }
                              },
                              [_c("i", { staticClass: "fas fa-file-pdf" })]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _c("td", [
                          _c(
                            "a",
                            {
                              directives: [
                                {
                                  name: "b-modal",
                                  rawName: "v-b-modal.Comentario",
                                  modifiers: { Comentario: true }
                                }
                              ],
                              staticClass: "btn btn-primary",
                              attrs: { type: "button", href: "#" },
                              on: {
                                click: function($event) {
                                  return _vm.ComentarioModalPresupuesto(
                                    presupuesto
                                  )
                                }
                              }
                            },
                            [_c("i", { staticClass: "fas fa-comment" })]
                          )
                        ])
                      ])
                    }),
                    0
                  )
                ])
              ])
            ]
          ),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass: "card-footer clearfix",
              staticStyle: { display: "block" }
            },
            [
              _c(
                "pagination",
                {
                  attrs: { data: _vm.presupuestos },
                  on: { "pagination-change-page": _vm.getResults }
                },
                [
                  _c(
                    "span",
                    { attrs: { slot: "prev-nav" }, slot: "prev-nav" },
                    [_c("i", { staticClass: "fas fa-arrow-left" })]
                  ),
                  _vm._v(" "),
                  _c(
                    "span",
                    { attrs: { slot: "next-nav" }, slot: "next-nav" },
                    [_c("i", { staticClass: "fas fa-arrow-right" })]
                  )
                ]
              )
            ],
            1
          )
        ]),
        _vm._v(" "),
        _c(
          "b-modal",
          {
            ref: "presupuesto",
            attrs: {
              id: "presupuesto",
              centered: "",
              size: "xl",
              "hide-footer": "",
              "no-close-on-backdrop": ""
            },
            scopedSlots: _vm._u([
              {
                key: "modal-header",
                fn: function(ref) {
                  var close = ref.close
                  return [
                    _c(
                      "h5",
                      {
                        staticClass: "modal-title",
                        attrs: { id: "BuscarCasoLabel" }
                      },
                      [
                        _vm._v("Numero de Presupuesto: "),
                        _c("b", [_vm._v(_vm._s(_vm.form.numero_presupuesto))]),
                        _vm._v(" - "),
                        _c("b", [_vm._v(_vm._s(_vm.form.datosCliente))])
                      ]
                    )
                  ]
                }
              }
            ])
          },
          [
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c(
                "div",
                { staticClass: "col-sm-8" },
                [
                  _c("div", { staticClass: "form-group" }, [
                    _c("label", [_vm._v("Serial del equipo")]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.form.serial_equipo,
                          expression: "form.serial_equipo"
                        }
                      ],
                      staticClass: "form-control",
                      class: {
                        "is-invalid": _vm.form.errors.has("serial_equipo")
                      },
                      attrs: {
                        disabled: "",
                        type: "text",
                        readonly: "readonly",
                        placeholder: "Serial del Equipo"
                      },
                      domProps: { value: _vm.form.serial_equipo },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            _vm.form,
                            "serial_equipo",
                            $event.target.value
                          )
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      directives: [
                        {
                          name: "show",
                          rawName: "v-show",
                          value: _vm.manage,
                          expression: "manage"
                        }
                      ]
                    },
                    [
                      _c("div", { staticClass: "form-group" }, [
                        _c("label", [_vm._v("Seleccione un Almacen")]),
                        _vm._v(" "),
                        _c(
                          "select",
                          {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.almacen,
                                expression: "form.almacen"
                              }
                            ],
                            staticClass: "custom-select",
                            on: {
                              change: [
                                function($event) {
                                  var $$selectedVal = Array.prototype.filter
                                    .call($event.target.options, function(o) {
                                      return o.selected
                                    })
                                    .map(function(o) {
                                      var val =
                                        "_value" in o ? o._value : o.value
                                      return val
                                    })
                                  _vm.$set(
                                    _vm.form,
                                    "almacen",
                                    $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  )
                                },
                                function($event) {
                                  return _vm.onChange()
                                }
                              ]
                            }
                          },
                          [
                            _c(
                              "option",
                              {
                                attrs: { disabled: "true" },
                                domProps: { value: null }
                              },
                              [_vm._v("Seleccione un almacén")]
                            ),
                            _vm._v(" "),
                            _vm._l(_vm.almacenes, function(almacen) {
                              return _c("option", { key: almacen.id }, [
                                _vm._v(_vm._s(almacen.nombre))
                              ])
                            })
                          ],
                          2
                        )
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "form-group" },
                        [
                          _c("label", [_vm._v("Seleccione una Pieza")]),
                          _vm._v(" "),
                          _c(
                            "select",
                            {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.form.pieza,
                                  expression: "form.pieza"
                                }
                              ],
                              staticClass: "form-control custom-select",
                              class: {
                                "is-invalid": _vm.form.errors.has("pieza")
                              },
                              attrs: { id: "pieza", disabled: "true" },
                              on: {
                                change: [
                                  function($event) {
                                    var $$selectedVal = Array.prototype.filter
                                      .call($event.target.options, function(o) {
                                        return o.selected
                                      })
                                      .map(function(o) {
                                        var val =
                                          "_value" in o ? o._value : o.value
                                        return val
                                      })
                                    _vm.$set(
                                      _vm.form,
                                      "pieza",
                                      $event.target.multiple
                                        ? $$selectedVal
                                        : $$selectedVal[0]
                                    )
                                  },
                                  function($event) {
                                    return _vm.onChange2()
                                  }
                                ]
                              }
                            },
                            _vm._l(_vm.piezas, function(pieza) {
                              return pieza.pieza !== null
                                ? _c("option", { key: pieza.id }, [
                                    _vm._v(
                                      "\n                " +
                                        _vm._s(
                                          pieza.pieza.nombre +
                                            " - " +
                                            pieza.numero_parte.pn
                                        )
                                    )
                                  ])
                                : _vm._e()
                            }),
                            0
                          ),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "pieza" }
                          })
                        ],
                        1
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "tablet-responsive" }, [
                    _c(
                      "table",
                      { staticClass: "table table-striped table-light" },
                      [
                        _c(
                          "thead",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.piece,
                                expression: "piece"
                              }
                            ]
                          },
                          [
                            _c("tr", [
                              _c("th", { attrs: { scope: "col" } }, [
                                _vm._v("Pieza")
                              ]),
                              _vm._v(" "),
                              _c("th", { attrs: { scope: "col" } }, [
                                _vm._v("Numero de Parte")
                              ]),
                              _vm._v(" "),
                              _c("th", { attrs: { scope: "col" } }, [
                                _vm._v("Cantidad")
                              ]),
                              _vm._v(" "),
                              _c("th", { attrs: { scope: "col" } }, [
                                _vm._v("Opciones")
                              ])
                            ])
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "tbody",
                          _vm._l(_vm.inventarios, function(inventario) {
                            return _c("tr", { key: inventario.id }, [
                              _c("td", [
                                _vm._v(_vm._s(inventario.pieza.nombre))
                              ]),
                              _vm._v(" "),
                              _c("td", [
                                _vm._v(_vm._s(inventario.numero_parte.pn))
                              ]),
                              _vm._v(" "),
                              _c("td", [_vm._v(_vm._s(_vm.cantidadPieza))]),
                              _vm._v(" "),
                              _c(
                                "button",
                                {
                                  staticClass: "btn btn-primary",
                                  attrs: { type: "button" },
                                  on: {
                                    click: function($event) {
                                      return _vm.cantidadPiecePlus()
                                    }
                                  }
                                },
                                [_c("i", { staticClass: "fas fa-plus" })]
                              ),
                              _vm._v(" "),
                              _c(
                                "button",
                                {
                                  staticClass: "btn btn-secondary",
                                  attrs: { type: "button" },
                                  on: { click: _vm.cantidadPieceMinus }
                                },
                                [_c("i", { staticClass: "fa fa-minus" })]
                              ),
                              _vm._v(" "),
                              _c(
                                "button",
                                {
                                  staticClass: "btn btn-success",
                                  attrs: { type: "button" },
                                  on: { click: _vm.agregarPieza }
                                },
                                [_c("i", { staticClass: "fa fa-check" })]
                              ),
                              _vm._v(" "),
                              _c(
                                "button",
                                {
                                  staticClass: "btn btn-danger",
                                  attrs: { type: "button" },
                                  on: { click: _vm.quitarPieza }
                                },
                                [_c("i", { staticClass: "fa fa-times" })]
                              )
                            ])
                          }),
                          0
                        )
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c("alert-error", {
                    attrs: {
                      form: _vm.form,
                      message: "Se han encontrado algunos errores."
                    }
                  }),
                  _vm._v(" "),
                  _c("div", { staticClass: "table-responsive" }, [
                    _c(
                      "table",
                      { staticClass: "table table-striped table-light" },
                      [
                        _c(
                          "thead",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.piece2,
                                expression: "piece2"
                              }
                            ]
                          },
                          [
                            _c("tr", [
                              _c("th", { attrs: { scope: "col" } }, [
                                _vm._v("Pieza")
                              ]),
                              _vm._v(" "),
                              _c("th", { attrs: { scope: "col" } }, [
                                _vm._v("Numero de Parte")
                              ]),
                              _vm._v(" "),
                              _c("th", { attrs: { scope: "col" } }, [
                                _vm._v("Precio Unit")
                              ]),
                              _vm._v(" "),
                              _c("th", { attrs: { scope: "col" } }, [
                                _vm._v("Cantidad")
                              ]),
                              _vm._v(" "),
                              _c(
                                "th",
                                {
                                  directives: [
                                    {
                                      name: "show",
                                      rawName: "v-show",
                                      value: _vm.manage,
                                      expression: "manage"
                                    }
                                  ],
                                  attrs: { scope: "col" }
                                },
                                [_vm._v("Opciones")]
                              )
                            ])
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "tbody",
                          _vm._l(_vm.arregloStock, function(arreglo) {
                            return _c("tr", { key: arreglo.idObj }, [
                              _c("td", [
                                _c("span", [_vm._v(_vm._s(arreglo.piezas))])
                              ]),
                              _vm._v(" "),
                              _c("td", [
                                _c(
                                  "span",
                                  { staticClass: "badge badge-light" },
                                  [_vm._v(_vm._s(arreglo.np_piezas))]
                                )
                              ]),
                              _vm._v(" "),
                              _c("td", [
                                _c("span", [
                                  _vm._v(_vm._s(arreglo.precio_piezas))
                                ])
                              ]),
                              _vm._v(" "),
                              _c("td", [
                                _c("span", [
                                  _vm._v(_vm._s(arreglo.cant_piezas))
                                ])
                              ]),
                              _vm._v(" "),
                              _c(
                                "button",
                                {
                                  directives: [
                                    {
                                      name: "show",
                                      rawName: "v-show",
                                      value: _vm.manage,
                                      expression: "manage"
                                    }
                                  ],
                                  staticClass: "btn btn-primary",
                                  attrs: { type: "button" },
                                  on: {
                                    click: function($event) {
                                      return _vm.cantidadPiecePlusM(arreglo)
                                    }
                                  }
                                },
                                [_c("i", { staticClass: "fas fa-plus" })]
                              ),
                              _vm._v(" "),
                              _c(
                                "button",
                                {
                                  directives: [
                                    {
                                      name: "show",
                                      rawName: "v-show",
                                      value: _vm.manage,
                                      expression: "manage"
                                    }
                                  ],
                                  staticClass: "btn btn-secondary",
                                  attrs: { type: "button" },
                                  on: {
                                    click: function($event) {
                                      return _vm.cantidadPieceMinusM(arreglo)
                                    }
                                  }
                                },
                                [_c("i", { staticClass: "fa fa-minus" })]
                              ),
                              _vm._v(" "),
                              _c(
                                "button",
                                {
                                  directives: [
                                    {
                                      name: "show",
                                      rawName: "v-show",
                                      value: _vm.manage,
                                      expression: "manage"
                                    }
                                  ],
                                  staticClass: "btn btn-danger",
                                  attrs: { type: "button" },
                                  on: {
                                    click: function($event) {
                                      return _vm.quitarPiezaSelected(arreglo)
                                    }
                                  }
                                },
                                [_c("i", { staticClass: "fa fa-times" })]
                              )
                            ])
                          }),
                          0
                        )
                      ]
                    )
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c("div", { staticClass: "col-sm-4" }, [
                _c("div", { staticClass: "form-group" }, [
                  _c("label", [_vm._v("Modelo del equipo")]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form.modelo_equipo,
                        expression: "form.modelo_equipo"
                      }
                    ],
                    staticClass: "form-control",
                    class: { "is-invalid": _vm.form.errors.has("cedula") },
                    attrs: {
                      disabled: "",
                      type: "text",
                      readonly: "readonly",
                      placeholder: "Modelo del equipo"
                    },
                    domProps: { value: _vm.form.modelo_equipo },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.form, "modelo_equipo", $event.target.value)
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _c("label", [_vm._v("Observaciones del presupuesto")]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "form-group" },
                  [
                    _c("b-form-textarea", {
                      class: {
                        "is-invalid": _vm.form.errors.has("presupuesto")
                      },
                      attrs: {
                        id: "textarea",
                        size: "1",
                        maxlength: "140",
                        placeholder: "Observación general del presupuesto...",
                        rows: "6",
                        "max-rows": "1",
                        readonly: _vm.Disabled
                      },
                      model: {
                        value: _vm.form.presupuesto,
                        callback: function($$v) {
                          _vm.$set(_vm.form, "presupuesto", $$v)
                        },
                        expression: "form.presupuesto"
                      }
                    }),
                    _c("has-error", {
                      attrs: { form: _vm.form, field: "presupuesto" }
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "form-group" },
                  [
                    _c(
                      "label",
                      {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: _vm.procesar,
                            expression: "procesar"
                          }
                        ]
                      },
                      [_vm._v("Asignar condiciones")]
                    ),
                    _vm._v(" "),
                    _c(
                      "label",
                      {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: !_vm.procesar,
                            expression: "!procesar"
                          }
                        ]
                      },
                      [_vm._v("Condiciones")]
                    ),
                    _vm._v(" "),
                    [
                      _c(
                        "div",
                        [
                          _c("label", { staticClass: "float-left" }, [
                            _vm._v("PC")
                          ]),
                          _c(
                            "b-form-checkbox",
                            {
                              staticClass: "float-left ml-2",
                              attrs: {
                                disabled: _vm.disable,
                                name: "check-button",
                                switch: ""
                              },
                              model: {
                                value: _vm.form.condiciones,
                                callback: function($$v) {
                                  _vm.$set(_vm.form, "condiciones", $$v)
                                },
                                expression: "form.condiciones"
                              }
                            },
                            [_c("label", [_vm._v("Servidor")])]
                          )
                        ],
                        1
                      )
                    ]
                  ],
                  2
                )
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "card-footer clearfix" }, [
              _c("div", { staticClass: "row" }, [
                _c(
                  "div",
                  { staticClass: "col-sm-6" },
                  [
                    _c(
                      "button",
                      {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: _vm.procesar,
                            expression: "procesar"
                          }
                        ],
                        staticClass: "btn btn-success",
                        attrs: { type: "submit" },
                        on: {
                          click: function($event) {
                            return _vm.procesarPresupuesto()
                          }
                        }
                      },
                      [_vm._v("Procesar Presupuesto")]
                    ),
                    _vm._v(" "),
                    _vm.actualizar
                      ? [
                          _c(
                            "button",
                            {
                              directives: [
                                {
                                  name: "show",
                                  rawName: "v-show",
                                  value: !_vm.update,
                                  expression: "!update"
                                }
                              ],
                              staticClass: "btn btn-primary",
                              attrs: { type: "submit" },
                              on: {
                                click: function($event) {
                                  return _vm.gestionar()
                                }
                              }
                            },
                            [_vm._v("Editar")]
                          ),
                          _vm._v(" "),
                          _c(
                            "button",
                            {
                              directives: [
                                {
                                  name: "show",
                                  rawName: "v-show",
                                  value: _vm.update,
                                  expression: "update"
                                }
                              ],
                              staticClass: "btn btn-primary",
                              attrs: { type: "submit" },
                              on: {
                                click: function($event) {
                                  return _vm.updatePresupuesto()
                                }
                              }
                            },
                            [_vm._v("Editar")]
                          )
                        ]
                      : _vm._e()
                  ],
                  2
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "col-sm-6 d-flex justify-content-end" },
                  [
                    _c(
                      "button",
                      {
                        staticClass: "btn btn-outline-danger",
                        attrs: { type: "button", "data-dismiss": "modal" },
                        on: {
                          click: function($event) {
                            return _vm.AtrasMP()
                          }
                        }
                      },
                      [_vm._v("Salir")]
                    )
                  ]
                )
              ])
            ])
          ]
        )
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "form",
      { staticClass: "form-horizontal" },
      [
        _c(
          "b-modal",
          {
            ref: "Comentario",
            attrs: {
              size: "xl",
              centered: "",
              id: "Comentario",
              tabindex: "-1",
              role: "dialog",
              "aria-hidden": "true",
              "hide-footer": "",
              "no-close-on-backdrop": ""
            },
            scopedSlots: _vm._u([
              {
                key: "modal-header",
                fn: function(ref) {
                  var close = ref.close
                  return [
                    _c(
                      "h5",
                      {
                        staticClass: "modal-title",
                        attrs: { id: "addUsuarioLabel" }
                      },
                      [
                        _vm._v(
                          "Comentarios del caso " + _vm._s(_vm.numero_caso)
                        )
                      ]
                    )
                  ]
                }
              }
            ])
          },
          [
            _vm._v(" "),
            _c("div", { staticClass: "content table-responsive" }, [
              _c(
                "table",
                {
                  staticClass: "table table-striped table-condensed",
                  attrs: { id: "tblGrid" }
                },
                [
                  _c("thead", { attrs: { id: "tblHead" } }, [
                    _c("tr", [
                      _c("th", [_vm._v("Fecha y Hora")]),
                      _vm._v(" "),
                      _c("th", [_vm._v("Usuario")]),
                      _vm._v(" "),
                      _c("th", [_vm._v("Tipo")]),
                      _vm._v(" "),
                      _c("th", [_vm._v("Comentario u Observación")])
                    ])
                  ]),
                  _vm._v(" "),
                  _c(
                    "tbody",
                    _vm._l(_vm.comentarios.data, function(comentario) {
                      return _c("tr", { key: comentario.id }, [
                        _c("td", [_vm._v(_vm._s(comentario.created_at))]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(comentario.usuario))]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(comentario.tipo.tipo))]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(comentario.comentario))])
                      ])
                    }),
                    0
                  )
                ]
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "card-footer" }, [
              _c("div", [
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-sm-3" }),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "col-sm-6 d-flex justify-content-center" },
                    [
                      _c(
                        "pagination",
                        {
                          attrs: { data: _vm.comentarios },
                          on: {
                            "pagination-change-page": _vm.getResultsComentarios
                          }
                        },
                        [
                          _c(
                            "span",
                            { attrs: { slot: "prev-nav" }, slot: "prev-nav" },
                            [_c("i", { staticClass: "fas fa-arrow-left" })]
                          ),
                          _vm._v(" "),
                          _c(
                            "span",
                            { attrs: { slot: "next-nav" }, slot: "next-nav" },
                            [_c("i", { staticClass: "fas fa-arrow-right" })]
                          )
                        ]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-sm-3 container text-right" }, [
                    _c(
                      "button",
                      {
                        staticClass: "btn btn-outline-danger",
                        attrs: { type: "button", "data-dismiss": "modal" },
                        on: { click: _vm.AtrasMC }
                      },
                      [_vm._v("Salir")]
                    ),
                    _vm._v(" "),
                    _c(
                      "button",
                      {
                        directives: [
                          {
                            name: "b-modal",
                            rawName: "v-b-modal.addComentario",
                            modifiers: { addComentario: true }
                          }
                        ],
                        staticClass: "btn btn-success",
                        attrs: { href: "#", type: "button" },
                        on: {
                          click: function($event) {
                            return _vm.NuevoComentarioModal()
                          }
                        }
                      },
                      [_vm._v("Comentar")]
                    )
                  ])
                ])
              ])
            ])
          ]
        ),
        _vm._v(" "),
        _c(
          "b-modal",
          {
            ref: "addComentario",
            staticClass: "modal fade modal-open",
            attrs: {
              centered: "",
              id: "addComentario",
              tabindex: "-1",
              role: "dialog",
              "aria-labelledby": "addUsuarioLabel",
              "aria-hidden": "true",
              "hide-footer": "",
              title: "Diagnóstico para el caso: 'EJEMPLO'",
              "no-close-on-backdrop": ""
            },
            scopedSlots: _vm._u([
              {
                key: "modal-header",
                fn: function(ref) {
                  var scape = ref.scape
                  return [
                    _c(
                      "h5",
                      {
                        staticClass: "modal-title",
                        attrs: { id: "addUsuarioLabel" }
                      },
                      [_vm._v("Agregar Comentario al caso")]
                    )
                  ]
                }
              }
            ])
          },
          [
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "form-group" },
              [
                _c("b-col", [
                  _c(
                    "div",
                    { staticClass: "md-form" },
                    [
                      _c("label", [_vm._v("Tipo")]),
                      _vm._v(" "),
                      _c(
                        "select",
                        {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.form.tipos,
                              expression: "form.tipos"
                            }
                          ],
                          staticClass: "form-control excep custom-select",
                          class: { "is-invalid": _vm.form.errors.has("tipos") },
                          attrs: { type: "text" },
                          on: {
                            change: function($event) {
                              var $$selectedVal = Array.prototype.filter
                                .call($event.target.options, function(o) {
                                  return o.selected
                                })
                                .map(function(o) {
                                  var val = "_value" in o ? o._value : o.value
                                  return val
                                })
                              _vm.$set(
                                _vm.form,
                                "tipos",
                                $event.target.multiple
                                  ? $$selectedVal
                                  : $$selectedVal[0]
                              )
                            }
                          }
                        },
                        _vm._l(_vm.tipos.data, function(tipo) {
                          return _c(
                            "option",
                            { key: tipo.id, domProps: { value: tipo.id } },
                            [_vm._v(_vm._s(tipo.tipo))]
                          )
                        }),
                        0
                      ),
                      _vm._v(" "),
                      _c("has-error", {
                        attrs: { form: _vm.form, field: "tipos" }
                      })
                    ],
                    1
                  )
                ])
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "form-group" },
              [
                _c("b-col", [
                  _c(
                    "div",
                    { staticClass: "md-form" },
                    [
                      _c("i", { staticClass: "fas fa-pencil-alt prefix" }),
                      _c("b", [_vm._v(" Comentario")]),
                      _vm._v(" "),
                      _c("textarea", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.form.comentario,
                            expression: "form.comentario"
                          }
                        ],
                        staticClass: "md-textarea form-control excep",
                        class: {
                          "is-invalid": _vm.form.errors.has("comentario")
                        },
                        attrs: {
                          id: "comentario",
                          rows: "3",
                          name: "Comentario",
                          maxlength: "140"
                        },
                        domProps: { value: _vm.form.comentario },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.form,
                              "comentario",
                              $event.target.value
                            )
                          }
                        }
                      }),
                      _c("has-error", {
                        attrs: { form: _vm.form, field: "comentario" }
                      })
                    ],
                    1
                  )
                ])
              ],
              1
            ),
            _vm._v(" "),
            _c("div", { staticClass: "modal-footer" }, [
              _c(
                "button",
                {
                  staticClass: "btn btn-success",
                  attrs: { type: "submit" },
                  on: {
                    click: function($event) {
                      return _vm.crearComentario()
                    }
                  }
                },
                [_vm._v("Comentar")]
              ),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass: "btn btn-outline-danger",
                  attrs: { type: "button", "data-dismiss": "modal" },
                  on: {
                    click: function($event) {
                      return _vm.salir()
                    }
                  }
                },
                [_vm._v("Salir")]
              )
            ])
          ]
        )
      ],
      1
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "info-box-icon bg-info elevation-1" }, [
      _c("i", { staticClass: "fas fa-flag" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "info-box-icon bg-primary elevation-1" }, [
      _c("i", { staticClass: "fas fa-check" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "info-box-icon bg-warning elevation-1" }, [
      _c("i", { staticClass: "fas fa-exclamation-circle" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "info-box-icon bg-success elevation-1" }, [
      _c("i", { staticClass: "fas fa-check-double" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "span",
      { staticClass: "info-box-icon bg-secondary elevation-1" },
      [_c("i", { staticClass: "fas fa-times" })]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "info-box-icon bg-danger elevation-1" }, [
      _c("i", { staticClass: "fas fa-calendar-times" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-header border-transparent" }, [
      _c("h3", { staticClass: "card-title" }, [
        _vm._v("Bandeja de casos en espera de presupuesto")
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "card-tools" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Numero de caso")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Serial del equipo")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Modelo del equipo")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Diagnostico Técnico")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Estatus")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Fecha de creación")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Opciones")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Comentarios")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("td", [
      _c("span", { staticClass: "badge badge-success" }, [_vm._v("Abierto")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("td", [
      _c("span", { staticClass: "badge badge-warning" }, [_vm._v("En espera")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-header border-transparent" }, [
      _c("h3", { staticClass: "card-title" }, [
        _vm._v("Bandeja de presupuestos")
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "card-tools" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", { attrs: { scope: "col" } }, [
          _vm._v("Numero de presupuesto")
        ]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Observación")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Estatus")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Fecha de creación")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Opciones")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Comentarios")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("td", [
      _c("span", { staticClass: "badge badge-success" }, [_vm._v("Activo")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("td", [
      _c("span", { staticClass: "badge badge-primary" }, [_vm._v("Procesado")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/presupuesto/PresupuestoComponent.vue":
/*!**********************************************************************!*\
  !*** ./resources/js/components/presupuesto/PresupuestoComponent.vue ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PresupuestoComponent_vue_vue_type_template_id_70c030d1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PresupuestoComponent.vue?vue&type=template&id=70c030d1& */ "./resources/js/components/presupuesto/PresupuestoComponent.vue?vue&type=template&id=70c030d1&");
/* harmony import */ var _PresupuestoComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PresupuestoComponent.vue?vue&type=script&lang=js& */ "./resources/js/components/presupuesto/PresupuestoComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _PresupuestoComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PresupuestoComponent_vue_vue_type_template_id_70c030d1___WEBPACK_IMPORTED_MODULE_0__["render"],
  _PresupuestoComponent_vue_vue_type_template_id_70c030d1___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/presupuesto/PresupuestoComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/presupuesto/PresupuestoComponent.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/components/presupuesto/PresupuestoComponent.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PresupuestoComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./PresupuestoComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/presupuesto/PresupuestoComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PresupuestoComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/presupuesto/PresupuestoComponent.vue?vue&type=template&id=70c030d1&":
/*!*****************************************************************************************************!*\
  !*** ./resources/js/components/presupuesto/PresupuestoComponent.vue?vue&type=template&id=70c030d1& ***!
  \*****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PresupuestoComponent_vue_vue_type_template_id_70c030d1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./PresupuestoComponent.vue?vue&type=template&id=70c030d1& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/presupuesto/PresupuestoComponent.vue?vue&type=template&id=70c030d1&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PresupuestoComponent_vue_vue_type_template_id_70c030d1___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PresupuestoComponent_vue_vue_type_template_id_70c030d1___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);