<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Registrollamada extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;
    protected $fillable = [
    	'id','client_id','company_id','user_id','ncontacto','serial_equipo','modelo_equipo','nombre_modelo','solicitudes','comentario','estatus',
    ];
    
    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
}
