<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sgpModelo extends Model
{
    /**
     * The database connection used by the model.
     *
     * @var string
     */

    public $timestamps = true;
    protected $connection = 'dbsgp';
    protected $table = 'modelos_ensamblar';

    // protected $primaryKey = 'sn_equipo';
    // public $incrementing = false;

    protected $fillable = [
        'categorias_codigo', 'modelo',
    ];
}
