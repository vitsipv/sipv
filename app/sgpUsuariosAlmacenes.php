<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sgpUsuariosAlmacenes extends Model
{
    public $timestamps = true;

    protected $connection = 'dbsgp';

    protected $table = 'usuarios_almacenes';

    public function almacenes()
    {
        return $this->belongsTo(sgpAlmacen::class, 'almacenes_id');
    }

    public function usuarios()
    {
        return $this->belongsTo(sgpUsuarios::class, 'usuarios_id');
    }

    public function empleado_usuario_almacen()
    {
        return $this
        ->hasOneThrough(
            sgpEmpleado::class,
            sgpUsuarios::class,
            'id',
            'id',
            'usuarios_id',
            'empleados_id'
        );
    }

    // protected $primaryKey = 'sn_equipo';
    // public $incrementing = false;

    /*protected $fillable = [
        'nombre',
    ];*/
}
