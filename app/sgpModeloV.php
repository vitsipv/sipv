<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sgpModeloV extends Model
{
    public $timestamps = true;
    protected $connection = 'dbsgp';
    protected $table = 'versiones';
    protected $primaryKey = 'modelos_ensamblar_id';
    public $incrementing = false;

    protected $fillable = [
        'modelos_ensamblar_id', 'version',
    ];

    public function modelos(){
        return $this->belongsTo(sgpModelo::class,'modelos_ensamblar_id','id');
    }
}
