<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\sgpCaracteristicaP;

class sgpNumeroParte extends Model
{
    public $timestamps = true;

    protected $connection = 'dbsgp';

    protected $table = 'numeros_partes';
  //  protected $appends = ['descripciones'];  <---- Comentado recientemente

  //   protected $fillable = [
  //       'control_calidad ',
  // 'generar_qn ',
  // 'pn ',
  // 'caracteristicas_piezas_id ',
  // 'caracteristicas_piezas_ids ',
  // 'sn_externo ',
  // 'accesorio ',
  // 'foto character ',
  // 'verificar',
  // 'asociacion_directa',
  // 'habilitar_presupuesto ',
  // 'producto_preensamblado',
  //   ];

    // protected $primaryKey = 'sn_equipo';
    // public $incrementing = false;

    public function caracteristicasP(){
        return $this->hasMany(sgpCaracteristicaP::class,'id','caracteristicas_piezas_ids');
    }

    public function caracteristicaP(){
        return $this->hasOne(sgpCaracteristicaP::class,'id','caracteristicas_piezas_id');
    }

    public function getDescripcionesAttribute(){

        $caracteristicas = $this->caracteristicas_piezas_ids;
        $caracteristicasS = explode('-',$caracteristicas);
        $colecion = collect($caracteristicasS)->map(function($id){
            return $id !== '' ? sgpCaracteristicaP::find((int) $id)->descripcion : ''; 
        });
        return implode(' ',$colecion->toArray());

    }

    public function precios(){
        return $this->hasMany(sgpPrecios::class,'numeros_partes_id','id');
    }

    public function existenciasPp(){
        return $this->hasMany(sgpExistencia_pp::class,'pn_id','id');
    }

    protected $fillable = [
        'carateristicas_piezas_ids',
    ];
}
