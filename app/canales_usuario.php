<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class canales_usuario extends Model
{
    use HasFactory;

    protected $fillable = [
    	'id','canal_soporte_id','user_id'
    ];
}
