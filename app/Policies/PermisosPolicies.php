<?php

namespace App\Policies;

use App\auth;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

class PermisosPolicies
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    private $guard,$request;
    public function __construct(Guard $auth,Request $re)
    {
        $this->guard = $auth;
        $this->request = $re;
    }

    public function index($seg)
    {
        #dd("ddd");
        $auth = $this->guard->user()->nick;
        $path = $this->request->path();
        $search = \App\Permisos::where(function ($query) use ($auth)
        {
            $query->where('usuario',$auth);
            
        })->get();
        
        if($search != null){
            foreach ($search as $key) {
                $modulosSearchAuthUser = \App\Modulos::where('nombre_modulo',"$key->modulo")->first();
                
                if((strcmp($key->codpermission,"ver") === 0) && $modulosSearchAuthUser !== null){
                    switch($modulosSearchAuthUser->nombre_modulo){
                        case "Clientes": 
                            return true;
                        break;
                    }
                }
            }
        }        
        return false;
    }

    public function Apertura_caso($seg)
    {
        $auth = $this->guard->user()->nick;
        $path = $this->request->path();
        $search = \App\Permisos::where(function ($query) use ($auth)
        {
            $query->where('usuario',$auth);
            
        })->get();
       
        if($search != null){
            foreach ($search as $key) {
                $modulosSearchAuthUser = \App\Modulos::where('nombre_modulo',"$key->modulo")->first();
                
                if((strcmp($key->codpermission,"ver") === 0) && $modulosSearchAuthUser !== null){
                    switch($modulosSearchAuthUser->nombre_modulo){
                        case "Aperturar Caso": 
                            return true;
                        break;
                    }
                }
            }
        }        
        return false;
    }

    public function Gestionar_caso($seg)
    {
        $auth = $this->guard->user()->nick;
        $path = $this->request->path();
        $search = \App\Permisos::where(function ($query) use ($auth)
        {
            $query->where('usuario',$auth);
            
        })->get();
        
        if($search != null){
            foreach ($search as $key) {
                $modulosSearchAuthUser = \App\Modulos::where('nombre_modulo',"$key->modulo")->first();
                
                if((strcmp($key->codpermission,"ver") === 0) && $modulosSearchAuthUser !== null){
                    switch($modulosSearchAuthUser->nombre_modulo){
                        case "Gestionar Caso": 
                            return true;
                        break;
                    }
                }
            }
        }        
        return false;
    }

    public function Recepcion($seg)
    {
        $auth = $this->guard->user()->nick;
        $path = $this->request->path();
        $search = \App\Permisos::where(function ($query) use ($auth)
        {
            $query->where('usuario',$auth);
            
        })->get();
        
        if($search != null){
            foreach ($search as $key) {
                $modulosSearchAuthUser = \App\Modulos::where('nombre_modulo',"$key->modulo")->first();
                
                if((strcmp($key->codpermission,"ver") === 0) && $modulosSearchAuthUser !== null){
                    switch($modulosSearchAuthUser->nombre_modulo){
                        case "Recepcion": 
                            return true;
                        break;
                    }
                }
            }
        }        
        return false;
    }

    public function Tecnico($seg)
    {
        $auth = $this->guard->user()->nick;
        $path = $this->request->path();
        $search = \App\Permisos::where(function ($query) use ($auth)
        {
            $query->where('usuario',$auth);
            
        })->get();
        
        if($search != null){
            foreach ($search as $key) {
                $modulosSearchAuthUser = \App\Modulos::where('nombre_modulo',"$key->modulo")->first();
                
                if((strcmp($key->codpermission,"ver") === 0) && $modulosSearchAuthUser !== null){
                    switch($modulosSearchAuthUser->nombre_modulo){
                        case "Tecnico": 
                            return true;
                        break;
                    }
                }
            }
        }        
        return false;
    }
    public function Presupuesto($seg)
    {
        $auth = $this->guard->user()->nick;
        $path = $this->request->path();
        $search = \App\Permisos::where(function ($query) use ($auth)
        {
            $query->where('usuario',$auth);
            
        })->get();
        
        if($search != null){
            foreach ($search as $key) {
                $modulosSearchAuthUser = \App\Modulos::where('nombre_modulo',"$key->modulo")->first();
                
                if((strcmp($key->codpermission,"ver") === 0) && $modulosSearchAuthUser !== null){
                    switch($modulosSearchAuthUser->nombre_modulo){
                        case "Presupuesto": 
                            return true;
                        break;
                    }
                }
            }
        }        
        return false;
    }

    public function LLamada($seg)
    {
       # dd("llego");
        $auth = $this->guard->user()->nick;
        $path = $this->request->path();
        $search = \App\Permisos::where(function ($query) use ($auth)
        {
            $query->where('usuario',$auth);
            
        })->get();
        
        if($search != null){
            foreach ($search as $key) {
                $modulosSearchAuthUser = \App\Modulos::where('nombre_modulo',"$key->modulo")->first();
                
                if((strcmp($key->codpermission,"ver") === 0) && $modulosSearchAuthUser !== null){
                    switch($modulosSearchAuthUser->nombre_modulo){
                        case "LLamadas": 
                            return true;
                        break;
                    }
                }
            }
        }        
        return false;
    }

    // public function perfil($seg)
    // {
    //     dd("lleog");
    //     $auth = $this->guard->user()->nick;
    //     $path = $this->request->path();
    //     $search = \App\Permisos::where(function ($query) use ($auth)
    //     {
    //         $query->where('usuario',$auth);
            
    //     })->get();
        
    //     if($search != null){
    //         foreach ($search as $key) {
    //             $modulosSearchAuthUser = \App\Modulos::where('nombre_modulo',"$key->modulo")->first();
                
    //             if((strcmp($key->codpermission,"ver") === 0) && $modulosSearchAuthUser !== null){
    //                 switch($modulosSearchAuthUser->nombre_modulo){
    //                     case "Perfil": 
    //                         return true;
    //                     break;
    //                 }
    //             }
    //         }
    //     }        
    //     return false;
    // }

    public function PartesPiezas($seg)
    {
        $auth = $this->guard->user()->nick;
        $path = $this->request->path();
        $search = \App\Permisos::where(function ($query) use ($auth)
        {
            $query->where('usuario',$auth);
            
        })->get();
        
        if($search != null){
            foreach ($search as $key) {
                $modulosSearchAuthUser = \App\Modulos::where('nombre_modulo',"$key->modulo")->first();
                
                if((strcmp($key->codpermission,"ver") === 0) && $modulosSearchAuthUser !== null){
                    switch($modulosSearchAuthUser->nombre_modulo){
                        case "Partes y Piezas": 
                            return true;
                        break;
                    }
                }
            }
        }        
        return false;
    }

    public function Equipos($seg)
    {
        $auth = $this->guard->user()->nick;
        $path = $this->request->path();
        $search = \App\Permisos::where(function ($query) use ($auth)
        {
            $query->where('usuario',$auth);
            
        })->get();
        
        if($search != null){
            foreach ($search as $key) {
                $modulosSearchAuthUser = \App\Modulos::where('nombre_modulo',"$key->modulo")->first();
                
                if((strcmp($key->codpermission,"ver") === 0) && $modulosSearchAuthUser !== null){
                    switch($modulosSearchAuthUser->nombre_modulo){
                        case "Equipos": 
                            return true;
                        break;
                    }
                }
            }
        }        
        return false;
    }
    public function Movimientos($seg)
    {
        $auth = $this->guard->user()->nick;
        $path = $this->request->path();
        $search = \App\Permisos::where(function ($query) use ($auth)
        {
            $query->where('usuario',$auth);
            
        })->get();
        
        if($search != null){
            foreach ($search as $key) {
                $modulosSearchAuthUser = \App\Modulos::where('nombre_modulo',"$key->modulo")->first();
                
                if((strcmp($key->codpermission,"ver") === 0) && $modulosSearchAuthUser !== null){
                    switch($modulosSearchAuthUser->nombre_modulo){
                        case "Movimientos": 
                            return true;
                        break;
                    }
                }
            }
        }        
        return false;
    }
}
