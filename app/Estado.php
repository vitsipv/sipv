<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $fillable = ['id', 'estado'];
    public $timestamps = false;
    public function municipios()
    {
        return $this->hasMany(Municipio::class);
    } 
}
