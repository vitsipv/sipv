<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class modulos extends Model
{
    protected $fillable = ['id', 'nombre'];
    public $timestamps = false;
}
