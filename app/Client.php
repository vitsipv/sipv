<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Client extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected  $primaryKey = 'cedula';
    public $incrementing = false;
    protected $fillable = [
    	'nacionalidad', 'cedula', 'name', 'nombres', 'apellidos', 'tlf1', 'tlf2', 'email1', 'email2', 'estado', 'municipio', 'parroquia', 'direccion'
    ];

    public function casos(){
        return $this->hasMany(Caso::class,'client_id','id');
    }

    /*public function canalCompra(){
    	return $this->hasManyThrough('App\PurchaseChannel','App\Caso','canal_compra_id','id');
    }*/
}
