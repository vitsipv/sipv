<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Company extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;
    protected  $primaryKey = 'rif_emp';
    public $incrementing = false;
    protected $fillable = [
        'tipo_emp', 'rif_emp', 'nombre_emp', 'nombre_se', 'estado_emp', 'municipio_emp', 'parroquia_emp', 'direccion_emp', 'tlf1_emp', 'tlf2_emp', 'email_emp',
    ];

    public function casos(){
        return $this->hasMany(Caso::class,'company_id','id');
    }
}
