<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sgpPrecios extends Model
{
    public $timestamps = true;

    protected $connection = 'dbsgp';

    protected $table = 'precios_productos';

    protected $primaryKey = 'id';
    public $incrementing = false;

    public function numeroParte(){
    	return $this->belongsTo(sgpNumeroParte::class,'numeros_partes_id','id');

    }

}
