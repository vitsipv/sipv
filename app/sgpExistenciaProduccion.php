<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sgpExistenciaProduccion extends Model
{
    /**
     * The database connection used by the model.
     *
     * @var string
     */

    public $timestamps = true;

    protected $connection = 'dbsgp';

    protected $table = 'existencia_etiquetada_produccion';

    protected $primaryKey = 'id';

    
}
