<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Piece;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\StorePieceRequest;
use App\Http\Requests\UpdatePieceRequest;


class PieceController extends Controller
{
    public function __construct() // Metodo de seguridad (Para no ingresar sin loguearte en cualquier ruta).
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Piece::paginate(6);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePieceRequest $request)
    {
        return Piece::create([
            'nuid_ppto' => $request['nuid_ppto'],
            'codi_ppto' => $request['codi_ppto'],
            'desc_ppto' => $request['desc_ppto'],
            'prec_ppto' => $request['prec_ppto'],
            'stat_ppto' => $request['stat_ppto'],
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePieceRequest $request, $id)
    {
        $piece = Piece::findOrFail($id);
        $piece->update($request->all());
        return ['message' => 'Pieza actualizada'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $piece = Piece::findOrFail($id);
        $piece->delete();
        return ['message' => 'Pieza eliminada'];
    }

    public function search()
    {
        if ($search = \Request::get('q')) {
           $svit_ppto = Piece::where(function($query) use ($search){
            $query->where('nuid_ppto', 'LIKE', "%$search%")
                ->orWhere('codi_ppto', 'LIKE', "%$search%")
                ->orWhere('desc_ppto', 'LIKE', "%$search%")
                ->orWhere('prec_ppto', 'LIKE', "%$search%")
                ->orWhere('stat_ppto', 'LIKE', "%$search%");
           })->paginate(6);
       }else{
            $svit_ppto = Piece::paginate(6);
       }
       return $svit_ppto;
    }
}
