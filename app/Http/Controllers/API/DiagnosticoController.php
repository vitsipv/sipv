<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use App\sgpAlmacen;
use App\Caso;
use App\Diagnostico;
use App\DetallesDiagnostico;
use App\Iva;
use \stdClass;

class DiagnosticoController extends Controller
{


    public function __construct() // Metodo de seguridad (Para no ingresar sin loguearte en cualquier ruta).
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /*array de almacenes con piezas " coloca dentro de un array el almacen y las pieza*/
    public function obtenerAlmacenesyPiezas($request){

        $array_almacenes= [];
        /* Array de almacenes con piezas */
        foreach ($request->arregloStock as $index => $array_item) {
            //array_push($array_almacenes, $array_item["almacenes"]);
            if (array_key_exists($array_item["almacenes"], $array_almacenes)) {
                array_push($array_almacenes[$array_item["almacenes"]], $array_item["np_piezas"].'-'.$array_item["piezas"]);
            }
            else {
                $array_almacenes[$array_item["almacenes"]] = [$array_item["np_piezas"].'-'.$array_item["piezas"]];
            }
        }
        return $array_almacenes;
        /* /Array de almacenes con piezas */

    }
/* */
    public function obtenerCorreosAlmacenistas($array_almacenes, $request){

        $array_almacenes_correos = [];
        $array_correos_por_almacen= [];
        foreach ($array_almacenes as $nombre_almacen => $items) {
            $almacen = sgpAlmacen::with('usuarios_almacenes.empleado_usuario_almacen:e_mail,nombre,apellido')
            ->where('nombre',$nombre_almacen)
            ->first()
            ->usuarios_almacenes->toArray();

            $key_to_string = strval($nombre_almacen);

            foreach ($almacen as $index => $array_usuario) {

                $objeto_empleado = new \stdClass();

                $nombre_completo_empleado =$array_usuario['empleado_usuario_almacen']['nombre']." ".$array_usuario['empleado_usuario_almacen']['apellido'];
                $email_empleado = $new_str = preg_replace("/\s+/", "", $array_usuario['empleado_usuario_almacen']['e_mail']);

                $objeto_empleado->nombre_completo = $nombre_completo_empleado;
                $objeto_empleado->correo = $email_empleado;

                if (array_key_exists($key_to_string, $array_almacenes_correos)) {
                   
                    if($email_empleado!==""){
                        array_push($array_almacenes_correos[$key_to_string], $objeto_empleado);
                        array_push($array_correos_por_almacen[$key_to_string], $email_empleado);
                    }  

                }
                else {
                    $array_almacenes_correos[$key_to_string] = [$objeto_empleado];
                    $array_correos_por_almacen[$key_to_string] = [$email_empleado];
                }
            }

            $array_correos_por_almacen[$key_to_string] = array_unique($array_correos_por_almacen[$key_to_string], SORT_REGULAR);
            $array_almacenes_correos[$key_to_string] = array_unique($array_almacenes_correos[$key_to_string], SORT_REGULAR);
        }

        return([$array_almacenes_correos,$array_correos_por_almacen]);
        
    }



    public function enviarCorreoAlmacenistas($request){

        $array_almacenes= $this->obtenerAlmacenesyPiezas($request);
        $array_correos= $this->obtenerCorreosAlmacenistas($array_almacenes, $request)[1];
        $array_correos_almacenistas = $this->obtenerCorreosAlmacenistas($array_almacenes, $request)[0];
        //dd([$array_correos_almacenistas,$array_correos]);
        $usuario_tecnico = User::where('id',$request['tecnico_id'])->first();

        foreach ($array_almacenes as $nombre_almacen => $array_piezas){

           $array_correos_enviar = $array_correos[$nombre_almacen];    
           
           $details=[
                'numero_caso' => $request['numero_caso'],
                'almacen' => $nombre_almacen,
                'piezas' => $array_piezas,
                'tecnico' => $usuario_tecnico->name,
                'title'=> 'Solicitud de Piezas Post Venta',
                'body'=> ''
            ];

            //\Mail::to('thistoprovethat@gmail.com')->send(new \App\Mail\TestMail($details));
            \Mail::to($array_correos_enviar)->send(new \App\Mail\TestMail($details));
        }
           
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$this->enviarCorreoAlmacenistas($request);
        
        $this->validate($request,[
            'pieza' => 'required',
            'diagnostico' => 'required',
            'numero_caso' => 'required',
            'tecnico_id' => 'required',
            'arregloStock' => 'required',
        ]);
        $iva = Iva::where('activo','=',true)->first()->porcentaje;
        $iva = $iva/100;

        $diagnostico = new Diagnostico();
        $diagnostico->tecnico_id = $request->tecnico_id;
        $diagnostico->numero_caso = $request->numero_caso;
        $diagnostico->diagnostico_caso = $request->diagnostico;
        $diagnostico->save();
 
        $diagnosticoId = Diagnostico::orderBy('id','DESC')->first()->id;
        $ddiagnosticos = [];
        foreach($request->arregloStock as $item){ //$intersts array contains input data
            $ddiagnostico = new DetallesDiagnostico();
            $ddiagnostico->diagnosticos_id = $diagnosticoId;
            $ddiagnostico->np_piezas = $item['np_piezas'];
            $ddiagnostico->cant_piezas = $item['cant_piezas'];
            $ddiagnostico->piezas = $item['piezas'];
            $ddiagnostico->almacenes = $item['almacenes'];
            $ddiagnostico->precio_piezas = round($item['precio_piezas']*$iva,2);
            $ddiagnostico->created_at = Carbon::now();
            $ddiagnostico->updated_at  = Carbon::now();

            $ddiagnosticos[] = $ddiagnostico->attributesToArray();
        }
 
        
     DetallesDiagnostico::insert($ddiagnosticos);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function casoTecnicos(Request $request){

      
        $tipoTecnico = User::with('usertype')->find(Auth::id())->user_type_id;
        $tecnico = Auth::id();
        if ($tipoTecnico == 1) {
            $casos = Caso::with('caseStatus','diagnosticoCaso','DetallesDiagnostico')->where('estado_caso_id','=', 2)->paginate(4); 
        return $casos; 
        }else{
            $casos = Caso::with('caseStatus','diagnosticoCaso','DetallesDiagnostico')->where('tecnico_id','=',$tecnico)->where('estado_caso_id','=', 2)->paginate(4); 
        return $casos; 
        }
        
       
       /*
        $casos = Caso::join('case_status','casos.estado_caso_id','=','case_status.id')
        ->leftjoin('diagnosticos','diagnosticos.numero_caso','!=','casos.numero_caso')
        ->where('estado_caso_id','=', 2)
        ->get();
        return $casos;
        */
    }

    public function aceptarCasoTecnico($id){
          $caso = Caso::findOrFail($id);
          $caso->preasignacion=true;
          $caso->save();
          return $caso;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editarDiagnostico($id, $piezasEliminar){
        return $piezasEliminar;
    }


    public function update(Request $request, $id)
    {
        
        $diagnosticoId = Diagnostico::where('id','=',$request['diagnosticoId'])->first()->id;
        foreach($request->arregloStock as $item){ //$intersts array contains input data 

            
            if(array_key_exists('id',$item)){
                $detalle = DetallesDiagnostico::where('id','=',$item['id'])->where('np_piezas','=',$item['np_piezas'])->get();
                $id2 = $detalle->max('id');
                $detalle = DetallesDiagnostico::findOrFail($id2);
                $detalle->cant_piezas=$item['cant_piezas'];
                $detalle->save();
            }
            else{
        
                $detalle = new DetallesDiagnostico();
                $detalle->diagnosticos_id = $diagnosticoId;
                $detalle->np_piezas = $item['np_piezas'];
                $detalle->cant_piezas = $item['cant_piezas'];
                $detalle->piezas = $item['piezas'];
                $detalle->almacenes = $item['almacenes'];
                $detalle->precio_piezas = $item['precio_piezas'];
                $detalle->created_at = Carbon::now();
                $detalle->updated_at  = Carbon::now();
                $detalle->save();
            }
        }//foreach

        $diagnostico = Diagnostico::findOrFail($diagnosticoId);
        $diagnostico->diagnostico_caso = $request['diagnostico'];
        $diagnostico->save();


        return response('Actualización exitosa', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function piezasEliminar(Request $request, $piezasEliminar){

        $piezasEliminar2 = explode(',', $piezasEliminar);
        foreach($piezasEliminar2 as $pieza){
            DetallesDiagnostico::find($pieza)->delete(); 
        }
    }
}
