<?php

namespace App\Http\Controllers\API;

use App\Caso;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Http\Requests\CasoStoreRequest;
use App\CaseStatus;
use App\Http\Controllers\SupportChannelController;
use App\sgpModelo;
use App\Falla;
use App\CierreCaso;

class CasoController extends Controller
{

    public function __construct() // Metodo de seguridad (Para no ingresar sin loguearte en cualquier ruta).
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function indexTecnico($tecnico_id)
    {
        return User::where('id',$tecnico_id)->first();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function numeroCaso()
    { 
        $caso = Caso::first();
        if ($caso == null) {
            $lastCaseId = 1;
            $pre = "VIT";                                                      
            $fecha = date('dmY');                                             
            $numero_caso = $pre.$lastCaseId.'-'.$fecha;
        }else{
            $lastCaseId = Caso::orderBy('id','desc')->first()->id+1;                                  
            $pre = "VIT";                                                         
            $fecha = date('dmY');                                                   
            $numero_caso = $pre.$lastCaseId.'-'.$fecha;
        }

        return $numero_caso;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CasoStoreRequest $request)
    {

        
        // Recorremos el array para sacar los np y ponerlos en un string.
        $ps = null;
        for($i=0; $i<count($request['piezas_sugeridas']); $i++){
            $ps = $ps.','.$request['piezas_sugeridas'][$i]['np'];
        }
        $ps = explode(',', $ps, 2);
        
        $numero_caso = $this->numeroCaso();

        $tipoUsuario = User::with('usertype')->find(Auth::id())->user_type_id;

        if ($tipoUsuario == 8) {
            $caseStatus = CaseStatus::where('case_status','=','En espera')->first()->id;
        }
        
        if ($tipoUsuario == 1) {
            $caseStatus = CaseStatus::where('case_status','=','En espera')->first()->id;
        }

        if (($request['serial_equipo'] == null) or ($request['serial_equipo'] == '')) {
            
            $modelo = sgpModelo::where('id','=',$request['modelo_equipo'])->first()->modelo;

        $caso = Caso::create([
            'client_id' => $request['client_id'],
            'company_id' => $request['company_id'],
            'serial_equipo' => null,
            'modelo_equipo' => $modelo,
            'canal_compra_id' => $request['canal_compra_id'],
            'obser_caso' => $request['obser_caso'],
            'estado_caso_id' => $caseStatus,
            'fecha_adqui' => $request['fecha_adqui'],
            'modo_atencion_id' => $request['modo_atencion_id'],
            'falla_equipo' => $request['falla_equipo'],
            'tecnico_id' => $request['tecnico_id'],
            'numero_caso' => $numero_caso,
            'piezas_sugeridas' => $ps[1],
            'canal_soporte_id' => $request['canal_soporte_id'],
            'tipo_equipo' => $request['tipo_equipo'],
            'preasignacion' => FALSE,

        ]);
            
        }
        if (($request['serial_equipo'] != null) or ($request['serial_equipo'] != '')) {

            $serialexiste = Caso::where('serial_equipo', '=' , $request['serial_equipo'])
            ->where('estado_caso_id','=',2)->orWhere('estado_caso_id','=',1)->first();
            // dd($caso);
            if($serialexiste == null){

                // $this->validate($request,['serial_equipo' => 'unique:casos',]);
                $modelo = sgpModelo::where('id','=',$request['modelo_equipo'])->first()->modelo;

                $caso = Caso::create([

                    'client_id' => $request['client_id'],
                    'company_id' => $request['company_id'],
                    'serial_equipo' => $request['serial_equipo'] ,
                    'modelo_equipo' => $modelo,
                    'canal_compra_id' => $request['canal_compra_id'],
                    'obser_caso' => $request['obser_caso'],
                    'estado_caso_id' => $caseStatus,
                    'fecha_adqui' => $request['fecha_adqui'],
                    'modo_atencion_id' => $request['modo_atencion_id'],
                    'falla_equipo' => $request['falla_equipo'],
                    'tecnico_id' => $request['tecnico_id'],
                    'numero_caso' => $numero_caso,
                    'piezas_sugeridas' => $ps[1],
                    'canal_soporte_id' => $request['canal_soporte_id'],
                    'tipo_equipo' => $request['tipo_equipo'],
                    ''
                ]);
            } //if
            if ($serialexiste != null) {
                return ('Ya existe un caso activo con ese serial');
            }
        }
                                                             
        return $caso;
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function fallas(){
        return Falla::get();
    }

    public function confirmarRecepcion(Request $request, $caso_id){
       $caso = Caso::findOrFail($caso_id);
       $caso->fecha_recepcion = date("Y-m-d H:i:s");
       $caso->save();
    }

    public function cerrarCaso(Request $request){
        $id = $request['id'];
        $caso = Caso::findOrFail($id);
        $caso->estado_caso_id=3;
        $caso->save();

        $numero_caso = $request['numero_caso'];
        $cierres_casos = [];
        foreach($request->arregloPF as $item){ //$intersts array contains input data
            $cierre = new CierreCaso();
            $cierre->numero_caso = $numero_caso;
            $cierre->pieza_reemplazada = $item['tipoPieza'];
            $falla = Falla::where('nombre','=',$item['fallaPieza'])->get();
            $fallaId = $falla->max('id');
            $cierre->fallas_id = $fallaId;
            $cierre->resumen = $request['resumen'];
            $cierre->motivo = $request['motivoReemplazo'];
            $cierre->created_at = Carbon::now();
            $cierre->updated_at  = Carbon::now();

            $cierres_casos[] = $cierre->attributesToArray();
        }

        CierreCaso::insert($cierres_casos);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
