<?php

namespace App\Http\Controllers\API;
use App\sgpModelo;
use App\Registrollamada;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegistroLlamadaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Registrollamada::with('user')->latest()->paginate(5);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'ncontacto' => 'required|string',
            'solicitudes' => 'required',
            'comentario' => 'required',
            'estatus' => 'required',
            'user_id' => 'required',
        ]);

        // Recorremos el array para sacar las descripciones de las solicitudes y ponerlos en un string.
        $des = null;
        for($i=0; $i<count($request['solicitudes']); $i++){
            $des = $des.','.$request['solicitudes'][$i]['descripcion'];
        }
        $modelo = sgpModelo::where('id','=',$request['modelo_equipo'])->first()->modelo;
        $des = explode(',', $des, 2);
        Registrollamada::create([
        'ncontacto' => $request['ncontacto'],
        'serial_equipo' => $request['serial_equipo'],
        'nombre_modelo' => $modelo,
        'modelo_equipo' => $request['modelo_equipo'],
        'comentario' => $request['comentario'],
        'estatus' => $request['estatus'],
        'client_id' => $request['client_id'],
        'company_id' => $request['company_id'],
        'user_id' => $request['user_id'],
        'solicitudes' => $des[1],
        ]);

        $Registrollamada = Registrollamada::all();
        $id = $Registrollamada->last()->id;
        $Registrollamada = Registrollamada::findOrFail($id);
        return $Registrollamada;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function getllamada(Request $request)
    {
        $datos = $request->tipo === 'persona'?'client_id':'company_id';
        $data = Registrollamada::with('user')->where($datos,'=',$request->id)->paginate(10);
        return [

            'pagination' => [
                'total' => $data->total(),
                'current_page' => $data->currentPage(),
                'per_page' => $data->perPage(),
                'last_page' => $data->lastPage(),
                'from' => $data->firstItem(),
                'to' => $data->lastPage(),
            ],
            'llamada' => $data
        ];
         
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $this->validate($request,[
            'ncontacto' => 'required|string',
            'serial_equipo' => 'sometimes',
            'solicitudes' => 'required',
            'comentario' => 'required',
            'estatus' => 'required',
        ]);

        // Recorremos el array para sacar las descripciones de las solicitudes y ponerlos en un string.
        $des = null;
        for($i=0; $i<count($request['solicitudes']); $i++){
            $des = $des.','.$request['solicitudes'][$i]['descripcion'];
        }
        $des = explode(',', $des, 2);

        $request->merge(['solicitudes' => $des[1]]);
        $Registrollamada = Registrollamada::findOrFail($request->id);
        $Registrollamada->update($request->all());
        $Registrollamada = Registrollamada::all();
        $id = $Registrollamada->last()->id;
        $Registrollamada = Registrollamada::findOrFail($id);
        return $Registrollamada;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
