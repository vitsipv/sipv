<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\SupportChannel;
use App\Client;
use App\Company;
use App\Caso;
use App\Comentario;
use App\TipoComentario;
use Illuminate\Support\Facades\Auth;


class GeneralReceptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function getCaso($id)
    {
        return Caso::with('cliente','modoAtencion','tecnico')->find($id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    //BUSQUEDA DE TECNICOS A ASIGNAR A CASOS
    public function searchTec(Request $request){
        $search = $request->support_channel;
        $search = intval($search);
        $tecnicos = User::whereHas('usersupportchannel', function($q) use($search)
        {
            $q->where('support_channels.id', $search);

        })->get();

        return $tecnicos;
    }

    public function asigTec(Request $request){
        $asignacion = Caso::findOrFail($request->id);
        $asignacion->tecnico_id = ($request->check);
        $asignacion->preasignacion = FALSE;
        if ($asignacion->estado_caso_id == 1) {
            $asignacion->estado_caso_id = 2;
        } 
        $asignacion->save();
        return ['message' => 'Tecnico Asignado'];
    }

    public function loadAuto_Case(Request $request){
        $Search_case = Caso::leftjoin('clients','casos.client_id','=','clients.id')
        ->leftjoin('companies','casos.company_id','=','companies.id')
        ->leftjoin('purchase_channels','casos.canal_compra_id','=','purchase_channels.id')
        ->leftjoin('attention_modes','casos.modo_atencion_id','=','attention_modes.id')
        ->leftjoin('case_status','casos.estado_caso_id','=','case_status.id')
        ->select('clients.cedula','clients.nombres','clients.apellidos','clients.tlf1','clients.tlf2','clients.email1','clients.email2','clients.direccion','casos.*'
        ,'companies.rif_emp','companies.nombre_emp','companies.nombre_se','companies.direccion_emp','companies.tlf1_emp','companies.tlf2_emp','companies.email_emp',
        'purchase_channels.nombre AS nombre_tienda','attention_modes.nombre AS tipo_atencion','case_status.case_status','case_status.id AS id_estatus')
        ->orderBy('numero_caso','desc')
        ->paginate(4);
        return [
            'pagination' => [
                'total' => $Search_case->total(),
                'current_page' => $Search_case->currentPage(),
                'per_page' => $Search_case->perPage(),
                'last_page' => $Search_case->lastPage(),
                'from' => $Search_case->firstItem(),
                'to' => $Search_case->lastPage(),
            ],
            'Search_case' => $Search_case
        ];
    }

    public function autoload_waiting(Request $request){
        $e = 1;
        $count = Caso::get()->where('estado_caso_id','=',$e);
        $count_waiting = $count->count(); 
        return $count_waiting;
        //dd ($count_esp);
    }

    public function autoload_asig(Request $request){
        $e = 2;
        $count = Caso::get()->where('estado_caso_id','=',$e);
        $count_asig = $count->count();
        return $count_asig;
        //dd ($count_asig);
    }

    public function autoload_reasig(Request $request){
        $e = 5;
        $count = Caso::get()->where('estado_caso_id','=',$e);
        $count_reasig = $count->count();
        return $count_reasig;
    }

    public function autoload_closed(Request $request){
        $e = 3;
        $count = Caso::get()->where('estado_caso_id','=',$e);
        $count_closed = $count->count();
        return $count_closed;
    }

    public function filterAutoCase($search){
        while ($search != "vacio")
        {             
            $Search_case = Caso::leftjoin('clients','casos.client_id','=','clients.id')
                        ->leftjoin('companies','casos.company_id','=','companies.id')
                        ->leftjoin('purchase_channels','casos.canal_compra_id','=','purchase_channels.id')
                        ->leftjoin('attention_modes','casos.modo_atencion_id','=','attention_modes.id')
                        ->leftjoin('case_status','casos.estado_caso_id','=','case_status.id')
                        ->where('casos.numero_caso','ilike',"%".$search."%")
                        ->orWhere('casos.serial_equipo','ilike',"%".$search."%")
                        ->orWhere('clients.cedula','ilike',"%".$search."%")
                        ->orWhere('companies.rif_emp','ilike',"%".$search."%")
                        ->select('clients.cedula','clients.nombres','clients.apellidos','clients.tlf1','clients.tlf2','clients.email1','clients.email2','clients.direccion','casos.*'
                        ,'companies.rif_emp','companies.nombre_emp','companies.nombre_se','companies.direccion_emp','companies.tlf1_emp','companies.tlf2_emp','companies.email_emp',
                        'purchase_channels.nombre AS nombre_tienda','attention_modes.nombre AS tipo_atencion','case_status.case_status','case_status.id AS id_estatus')
                        ->orderBy('numero_caso','desc')
                        ->paginate(4);
                        
                        return [
                            'pagination' => [
                                'total' => $Search_case->total(),
                                'current_page' => $Search_case->currentPage(),
                                'per_page' => $Search_case->perPage(),
                                'last_page' => $Search_case->lastPage(),
                                'from' => $Search_case->firstItem(),
                                'to' => $Search_case->lastPage(),
                            ],
                            'Search_case' => $Search_case
                        ];
        }
        $Search_case = Caso::leftjoin('clients','casos.client_id','=','clients.id')
                    ->leftjoin('companies','casos.company_id','=','companies.id')
                    ->leftjoin('purchase_channels','casos.canal_compra_id','=','purchase_channels.id')
                    ->leftjoin('attention_modes','casos.modo_atencion_id','=','attention_modes.id')
                    ->leftjoin('case_status','casos.estado_caso_id','=','case_status.id')
                    ->select('clients.cedula','clients.nombres','clients.apellidos','clients.tlf1','clients.tlf2','clients.email1','clients.email2','clients.direccion','casos.*'
                    ,'companies.rif_emp','companies.nombre_emp','companies.nombre_se','companies.direccion_emp','companies.tlf1_emp','companies.tlf2_emp','companies.email_emp',
                    'purchase_channels.nombre AS nombre_tienda','attention_modes.nombre AS tipo_atencion','case_status.case_status','case_status.id AS id_estatus')
                    ->orderBy('numero_caso','desc')
                    ->paginate(4);

                        return [
                        'pagination' => [
                            'total' => $Search_case->total(),
                            'current_page' => $Search_case->currentPage(),
                            'per_page' => $Search_case->perPage(),
                            'last_page' => $Search_case->lastPage(),
                            'from' => $Search_case->firstItem(),
                            'to' => $Search_case->lastPage(),
                        ],
                        'Search_case' => $Search_case
                        ];

    }

    public function Recep_Coment(Request $request){
        //dd($request->numero_caso);
        //return Comentario::with('tipo')->where('numero_caso_id',$c)->latest()->paginate(5);
        return Comentario::with('tipo')->where('numero_caso_id',$request->numero_caso)->latest()->paginate(5);
    }

    public function Add_comment(Request $request){
        
        //dd($request);
        $this->validate($request,[
            'tip_comments' => 'required',
            'comentario' => 'required',
        ]);
        //BUSCAR USARIO A HACER COMENTARIO
        $id = Auth::id();
        $user = User::findOrFail($id);
        $usuario = $user->name;
        //VARIABLE FALSE POR DEFECTO
        $estado = false;

        return Comentario::create([
            'comentario' => $request['comentario'],
            'tipo_comentario_id' => $request['tip_comments'],
            'estado' => $estado,
            'numero_caso_id' => $request['num_case']
        ]);
    }

    public function Add_serial(Request $request){
        
       // dd($request);
        $Serial = Caso::where('numero_caso','=',$request->numero_caso)->first();
        $Serial->serial_equipo = $request->serial_equipo;
        $Serial->save();
        return ['message' => 'Serial Agregado'];
    }

    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) // Borrar el usuario por su ID.
    {
        $user = User::findOrFail($id );
        
        $user->delete();
        return ['message' => 'comentario eliminado.'];
        
    }

    public function crearRecibo($id){

        $caso = $this->getCaso($id);
        $coleccion_info_recibo = collect();

        $serial_equipo = $caso->serial_equipo ? $caso->serial_equipo : "No especificado";

        $coleccion_info_recibo->push([
            'modelo' => $caso->modelo_equipo,
            'serial' => $serial_equipo,
            'caso' => $caso->id,
            'modo_atencion' =>$caso->modoAtencion->nombre,
            'tecnico' =>$caso->tecnico->name,
            'nombre_cliente' =>$caso->cliente->nombres.' '.$caso->cliente->apellidos,
            'cedula_cliente' =>$caso->cliente->cedula,
            'telefono_cliente' =>$caso->cliente->tlf1.'/'.($caso->cliente->tlf2?$caso->cliente->tlf2 : ''),
            'observacion' =>$caso->obser_caso,
            'fecha' => now()->format('d-m-Y')
        ]);

        return $coleccion_info_recibo;
    }

}