<?php

namespace App\Http\Controllers\API;
use Illuminate\Database\Eloquent\Builder;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\sgpMovimientoInventario;
use App\sgpPaletasMovimiento;
use App\sgpPosicion;
use App\sgpPaleta;
use App\sgpNumeroParte;
use App\sgpExistencia_pp;
use App\sgpExistenciaProduccion;
use App\DespachoMovimientoInventario;
use App\sgpAlmacen;
use Carbon\Carbon;


class MovimientoInventarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movimientos_inventario = sgpMovimientoInventario::with('galpon_destino', 'info_administrador_almacen_origen', 'administrador_almacen_destino', 'seguridad_origen', 'seguridad_destino')->orderBy('id', 'DESC')->paginate(10);
        return $movimientos_inventario;
    }

    public function indexSeriales($numero_parte_id)
    {
        $seriales = [];
        $numero_parte = sgpNumeroParte::with('existenciasPp.exisEtiquetadas')->find($numero_parte_id)->existenciasPp;
        foreach ($numero_parte as $existencia_pp) {
            foreach ($existencia_pp->exisEtiquetadas as $etiqueta_produccion) {
                array_push($seriales, $etiqueta_produccion);
            }
        }
        return $seriales;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = sgpMovimientoInventario::orderBy('id','DESC')->get()->first()->id+1;
        $correlativo = sgpMovimientoInventario::orderBy('id','DESC')->get()->first()->correlativo+1;

        $this->validate($request,[
            'administrador_almacen_origen' => 'required',
            'seguridad_origen' => 'required'
        ]);

        return sgpMovimientoInventario::create([
            'id' => $id,
            'correlativo' => $correlativo,
            'administrador_almacen_origen' => $request['administrador_almacen_origen']['id'],
            'galpon_destino' => $request['almacen_destino']['id'],
            'conductores_id' => $request->conductores_id['id'],
            'seguridad_origen' => $request['seguridad_origen']['id'],
            'vehiculos_id' => $request->vehiculos_id['id'],
        ]);
        
    }

    private function crearExistenciaPP($paleta_id, $almacen_destino_id, $pieza_id, $numero_parte_id)
    {
        $id = sgpExistencia_pp::orderBy('id','DESC')->get()->first()->id+1;

        $existencia_pp = new sgpExistencia_pp;
        $existencia_pp->id = $id;
        $existencia_pp->paletas_id = $paleta_id;
        $existencia_pp->piezas_id = $pieza_id;
        $existencia_pp->cantidad = 0;
        $existencia_pp->pn_id = $numero_parte_id;
        $existencia_pp->inspecciones_cce_id = null;
        $existencia_pp->np_proveedores_id = null;
        $existencia_pp->detalles_facturas_invoice_id = null;
        $existencia_pp->save();
        /*$existencia_pp = sgpExistencia_pp::create([
            'paletas_id' => $paleta_id,
            'piezas_id' => $pieza_id,
            'cantidad' => 0,
            'pn_id' => $numero_parte_id,
            'inspecciones_cce_id' => null,
            'np_proveedores_id' => null,
            'detalles_facturas_invoice_id' => null,
        ]);*/
        return $existencia_pp;
    }

    private function crearPaletaInexistente($almacen_destino_id)
    {
        //Crea una paleta para la existencia actual en el caso de que, al realizar el movimiento, no exista esa paleta

        /*Encontrar el id de la posicion donde estára esa paleta a crear*/
        $posiciones_id = sgpPosicion::where('almacenes_id',$almacen_destino_id)->where('posicion','--')->first()->id;

        $id = sgpPaleta::orderBy('id','DESC')->get()->first()->id+1;

        $nombre_paleta = sgpPaleta::where('paleta', 'like', '%'.'VIT'.date('dmy').'%')->first();

        $paleta = new sgpPaleta;
        $paleta->id = $id;
        $paleta->fechas_contenedores_id = null;
        $paleta->paleta = ($nombre_paleta ? $nombre_paleta : 'VIT'.date('dmy').'-000PP');
        $paleta->lote = null;
        $paleta->encargado_id = null;
        $paleta->eliminado = 0;
        $paleta->observacion = '';
        $paleta->posiciones_id = $posiciones_id;
        $paleta->liberado_calidad = null;
        $paleta->despiece = null;
        $paleta->save();

        return $paleta;
    }

    private function encontrarPaletaConExistenciaPP($almacen_destino_id, $pieza, $numero_parte)
    {
        //Busca en el almacen de destino una paleta vacía que contenga el mismo tipo de pieza
        return sgpExistencia_pp::with('pieza', 'posiciones.almacen','paleta', 'numeroParte', 'exisEtiquetadas')
            ->whereHas('posiciones.almacen',function($q) use ($almacen_destino_id){
                      $q->where('id',$almacen_destino_id);
                    })
            ->whereHas('pieza',function($q) use ($pieza){
                      $q->where('id',$pieza);
                    })
            ->whereHas('numeroParte',function($q) use ($numero_parte){
                      $q->where('pn',$numero_parte);
                    })
            ->where('cantidad', '!=', '-1')
            ->first();
    }

    private function movimientoPorEtiquetas($existencia_pp_actual, $existencia_pp_objetivo, $array_etiquetadas_id)
    {
        $existencia_pp_origen = sgpExistencia_pp::find($existencia_pp_actual);
        $existencia_pp_destino = sgpExistencia_pp::find($existencia_pp_objetivo);
        foreach ($array_etiquetadas_id as $index => $etiqueta_id) {
            //Por cada existencia etiquetada a mover, cambiar el existencia_pp_id de la misma a la existencia_pp_id del objetivo
            $existencia_etiquetada = sgpExistenciaProduccion::find($etiqueta_id);
            $existencia_etiquetada->existencias_pp_id = $existencia_pp_destino['id'];
            $existencia_etiquetada->save();
        }
    }

    private function movimientoPorCantidad($existencia_pp_actual, $existencia_pp_objetivo, $cantidad_mover)
    {
        //Resta la cantidad en la existencia_pp de la paleta de origen
        $existencia_pp_origen = sgpExistencia_pp::find($existencia_pp_actual);
        $existencia_pp_origen->timestamps = false; //Al momento de realizar la actualizacion laravel busca la columna updated_at que no existe en esta tabla
        $existencia_pp_origen->cantidad = $existencia_pp_origen->cantidad - $cantidad_mover;
        $existencia_pp_origen->save();

        // Y suma la misma cantidad en la existencia_pp de la paleta objetivo
        $existencia_pp_objetivo = sgpExistencia_pp::find($existencia_pp_objetivo);
        $existencia_pp_objetivo->timestamps = false; //Al momento de realizar la actualizacion laravel busca la columna updated_at que no existe en esta tabla
        $existencia_pp_objetivo->cantidad = $existencia_pp_objetivo->cantidad + $cantidad_mover;
        $existencia_pp_objetivo->save();
    }

    public function despacharMovimiento(Request $request)
    {   
        $despacho = DespachoMovimientoInventario::where('movimiento_inventario_id',$request['movimiento_inventario_id'])->first();
         //NO ETIQUETADA
        if ($despacho->cantidad != 0 ) {
            $this->movimientoPorCantidad($despacho->existencias_pp_origen_id, $despacho->existencias_pp_destino_id, $despacho->cantidad);
        }

        //ETIQUETADA
        if ($despacho->array_etiquetadas_id) {
            $this->movimientoPorEtiquetas($despacho->existencias_pp_origen_id, $despacho->existencias_pp_destino_id, $despacho->array_etiquetadas_id);
        }
        /*Modifica el valor de despachado en la tabla de despachos*/
        $despacho->despachado = True;
        $despacho->save();
        /* Modifica la fila en la tabla movimiento inventario */
        $movimiento = sgpMovimientoInventario::find($request['movimiento_inventario_id']);
        $movimiento->administrador_almacen_destino = $request->administrador_almacen_destino['id'];
        $movimiento->seguridad_destino = $request->seguridad_destino['id'];
        $movimiento->fecha_recepcion = now()->toDateTimeString();
        $movimiento->save();
    }

    private function obtenerIdsArray($array)
    {   //De un array de objetos devuelve un array json de los ids de cada objeto

        $array_ids = [];
        foreach ($array as $index => $array_items) {
            array_push($array_ids, $array_items['id']);
        }
        return json_encode($array_ids);
    }

    public function procesarMovimiento(Request $request)
    {   

        foreach ($request->piezas_partes_asociadas_movimiento as $index => $objeto_movimiento) {
            /*  
            * Cada $objeto_movimiento contiene como propiedades: 1) cantidad, 2) un array de las piezas etiquetadas, 3) información de la paleta donde está esa pieza
            *
            */
            $cantidad = $objeto_movimiento['cantidad'];
            $array_etiquetadas = $objeto_movimiento['etiquetadas'];
            $pieza_id = $objeto_movimiento['info_existencia_pp']['piezas_id'];
            $numero_parte = $objeto_movimiento['info_existencia_pp']['numero_parte'];
            $almacen_destino_id = $request->almacen_destino['id'];

            $existencia_pp_actual =  $objeto_movimiento['info_existencia_pp'];
            $existencia_pp_objetivo = $this->encontrarPaletaConExistenciaPP($almacen_destino_id, $pieza_id, $numero_parte['pn']);

            //dd($existencia_pp_actual, $existencia_pp_objetivo, $cantidad);

            if ($existencia_pp_objetivo) {
                //En el caso de que exista una paleta con esa existencia en el almacen determinado
                DespachoMovimientoInventario::create([
                    'movimiento_inventario_id' => $request->id,
                    'existencias_pp_origen_id' => $existencia_pp_actual['id'],
                    'existencias_pp_destino_id' => $existencia_pp_objetivo['id'],
                    'cantidad' => $cantidad,
                    'array_etiquetadas_id' => $array_etiquetadas==[] ? null : $this->obtenerIdsArray($array_etiquetadas),
                    'motivo' => $request->motivo,
                ]);

            }
            else{
                $paleta_id = $this->crearPaletaInexistente($almacen_destino_id)->id;
                $existencia_pp_objetivo = $this->crearExistenciaPP($paleta_id, $almacen_destino_id, $pieza_id, $numero_parte['id']);
                //dd("No existe el existencia_pp_objetivo asi que debes crear una nueva paleta");
                DespachoMovimientoInventario::create([
                    'movimiento_inventario_id' => $request->id,
                    'existencias_pp_origen_id' => $existencia_pp_actual['id'],
                    'existencias_pp_destino_id' => $existencia_pp_objetivo['id'],
                    'cantidad' => $cantidad,
                    'array_etiquetadas_id' => $array_etiquetadas==[] ? null : $this->obtenerIdsArray($array_etiquetadas),
                    'motivo' => $request->motivo,
                ]);
            }

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $movimiento = sgpMovimientoInventario::findOrFail($id);

        $this->validate($request,[
            'administrador_almacen_origen' => 'required',
            'seguridad_origen' => 'required'
        ]);

        $movimiento->administrador_almacen_origen = $request['administrador_almacen_origen']['id'];
        $movimiento->galpon_destino = $request['almacen_destino']['id'];
        $movimiento->conductores_id = $request->conductores_id['id'];
        $movimiento->seguridad_origen = $request['seguridad_origen']['id'];
        $movimiento->vehiculos_id = $request->vehiculos_id['id'];

        $movimiento->save();
       return response('Actualización exitosa', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $movimiento = sgpMovimientoInventario::findOrFail($id);
        $movimiento->delete();
    }

    public function filtroReporte($info_request){

        $array_ids_filtrados_almacen_destino = [];
        $array_ids_filtrados_paleta = [];
        $array_ids_filtrados_np = [];

        $movimientos = DespachoMovimientoInventario::with('existencia_pp_origen.posiciones.almacen', 'existencia_pp_destino.posiciones.almacen', 'movimiento_inventario.info_administrador_almacen_origen', 'existencia_pp_origen.numeroParte');

        /* ----------------------------------------*
        *         Filtros de Partes y piezas       *
        * -----------------------------------------*
        */

        //dd(sgpExistencia_pp::with('numeroParte')->where('id',$movimientos->get()->first()->existencias_pp_origen_id)->first());


        // *** Condicional de almacen_destino ***

        if ($info_request->almacen_destino != null) {
            foreach ($movimientos->get() as $mov) {
                $query = sgpExistencia_pp::with('posiciones.almacen')->where('id', $mov->existencias_pp_destino_id)->first()->posiciones->almacen->id;
                if ($info_request->almacen_destino['id'] == $query) {
                    array_push($array_ids_filtrados_almacen_destino, $mov->existencias_pp_destino_id);
                }
            }
            $movimientos = $movimientos->whereIn('existencias_pp_destino_id',$array_ids_filtrados_almacen_destino);
        }

        // *** Condicional de paleta ***

       if ($info_request->paleta != null) {
            foreach ($movimientos->get() as $mov) {
                $query = sgpExistencia_pp::where('paletas_id', $info_request->paleta)->orWhere('paletas_id', $info_request->paleta)->first();
                if ($query) {
                    array_push($array_ids_filtrados_paleta, $query->id);
                }
            }
            $movimientos = $movimientos->whereIn('existencias_pp_destino_id',$array_ids_filtrados_paleta)->orWhereIn('existencias_pp_destino_id',$array_ids_filtrados_paleta);
        }

        // *** Condicional de np ***
        
        if ($info_request->np != null) {
            foreach ($movimientos->get() as $mov) {
                $query = sgpExistencia_pp::where('pn_id', $info_request->np['id'])->first();
                if ($query) {
                    array_push($array_ids_filtrados_np, $query->id);
                }
            }
            $movimientos = $movimientos->whereIn('existencias_pp_destino_id',$array_ids_filtrados_np)->orWhereIn('existencias_pp_destino_id',$array_ids_filtrados_np);
        }

        // *** Condicional de serial ***

        if ($info_request->np != null and $info_request->serial != null) {
            foreach ($movimientos->get() as $mov) {
                $query = sgpExistencia_pp::where('id', $info_request->serial['existencias_pp_id'])->first();
                if ($query) {
                    array_push($array_ids_filtrados_np, $query->id);
                }
            }
            $movimientos = $movimientos->whereIn('existencias_pp_destino_id',$array_ids_filtrados_np)->orWhereIn('existencias_pp_destino_id',$array_ids_filtrados_np);
        }


        // *** Condicional de motivo ***
        $movimientos->when($info_request->motivo,
            function($query) use($info_request){
                $query->where('motivo',$info_request->motivo);
            });

        // *** Condicional de fecha_inicial ***
        $movimientos->when($info_request->fecha_inicial,
            function($query) use($info_request){
                $this->validate($info_request,[
                    'fecha_final' => 'required',
                ]);
                $query->whereBetween('created_at',[$info_request->fecha_inicial, $info_request->fecha_final]);
            });

        // *** Condicional de fecha_final ***
        $movimientos->when($info_request->fecha_final,
            function($query) use($info_request){
                $this->validate($info_request,[
                    'fecha_inicial' => 'required',
                ]);
                $query->whereBetween('created_at',[$info_request->fecha_inicial, $info_request->fecha_final]);
            });

        return $movimientos->get();
    }

    public function crearInfoReporte($movimientos_filtrados){
        // Codigo, Fecha, Administrador almacen de origen, Motivo, NP, Almacen origen, Almacen destino
        $coleccion_info_reporte = collect();
        foreach ($movimientos_filtrados as $movimiento) {
            $coleccion_info_reporte->push([
                'codigo' => 'MIV'.$movimiento->created_at->format('dmy').'-'.$movimiento->movimiento_inventario['correlativo'],
                'fecha' => $movimiento->created_at->format('m/d/Y'),
                'admin_almacen_origen' => $movimiento->movimiento_inventario->info_administrador_almacen_origen->nombre.' '.$movimiento->movimiento_inventario->info_administrador_almacen_origen->apellido,
                'motivo' => $movimiento->motivo,
                'np' => $movimiento->existencia_pp_origen->numeroParte->pn,
                'almacen_origen' => $movimiento->existencia_pp_origen->posiciones->almacen->iniciales,
                'almacen_destino' => $movimiento->existencia_pp_destino->posiciones->almacen->iniciales,
            ]);
        }
        return $coleccion_info_reporte;
    }

    public function crearReporte(Request $request){
        $movimientos_filtrados = $this->filtroReporte($request);

        $info_reporte = $this->crearInfoReporte($movimientos_filtrados);

        return $info_reporte;;
    }

}
