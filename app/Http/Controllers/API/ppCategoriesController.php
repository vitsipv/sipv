<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ppCategory;
use App\Origin;

class ppCategoriesController extends Controller
{
    public function ppOrigins(){
    	return Origin::orderBy('origin', 'ASC')->get(['id', 'origin']);
    }
    
    public function ppCategories(){
    	return ppCategory::orderBy('name', 'ASC')->get(['id', 'name']);
    }

    public function ppDescription(ppCategory $ppcategory){
    	return $ppcategory->ppdescription()->orderBy('pp_description', 'ASC')->get(['id', 'pp_description']);
    }
}
