<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\ppInventory;
use App\ppDescription;
use App\ppCategory;
use App\Origin;
use App\sgpExistencia_pp;
use App\sgpNumeroParte;
use App\sgpAlmacen;

use Illuminate\Support\Collection as Collection;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\StorePieceRequest;
use App\Http\Requests\UpdatePieceRequest;

class ppController extends Controller
{
    public function __construct() // Metodo de seguridad (Para no ingresar sin loguearte en cualquier ruta).
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $ppInventory = ppInventory::find(1);
        // $ppInventory = ppInventory::with('ppdescription')->paginate(12);
        $ppInventory = sgpExistencia_pp::with('paleta','exisEtiquetadas','numeroParte','posiciones.almacen')->whereHas('posiciones.almacen',function($q){
            $q->where('clasificacion','PV');
        })->orderBy('id','ASC')->take(10)->paginate(30);

        foreach ($ppInventory as $pp) {
            if($pp->numeroParte){
                 $c = sgpNumeroParte::where('id',$pp->numeroParte->id)->first();
            }
        }

        // $ppInventory = \DB::table('pp_descriptions')
        //     ->select('pp_inventories.*', 'pp_descriptions.pp_np', 'pp_descriptions.pp_tipo', 'pp_descriptions.pp_description', 'pp_descriptions.pp_category_id', 'origins.origin')
        //     ->join('pp_inventories', 'pp_inventories.pp_description_id', '=', 'pp_descriptions.id')
        //     ->join('origins', 'pp_inventories.origin_id', '=', 'origins.id')
        //     ->latest()
        //     ->paginate(12);
        return $ppInventory;
        
        // $ppInventory = \DB::table('pp_inventories')
        //         ->select(\DB::raw('SUM(pp_cantidad) as total_piezas'))
        //         // ->groupBy('department')
        //         // ->havingRaw('SUM(pp_cantidad) > ?', [2500])
        //         ->get();
        //         dd($ppInventory);
        // return $ppInventory;

    }

    public function index_etiquetadas($pp_id) {
        return sgpExistencia_pp::with('exisEtiquetadas')
                ->whereHas('exisEtiquetadas',function($q)
                {
                    $q->where('existencias_pp_id',$pp_id);
                })
                ->orderBy('id','ASC')
                ->take(10)
                ->paginate(30);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePieceRequest $request)
    {
        $search = $request['pp_description_id'];
        $cantidad = $request['pp_cantidad'];
        $cond = $request['pp_cond'];
        $origin = $request['origin_id'];
        $respuesta = $request['respuesta'];
        $serial = $request['pp_serial'];

        $piece = ppInventory::where(function($query) use ($search, $cond, $origin){
            $query->where([
                ['pp_description_id', '=', $search],
                ['pp_cond', '=', $cond],
                ['origin_id', '=', $origin],
                ['pp_status', '=', 'TRUE'],
            ])->whereNull('pp_serial');
           })->first();

           if($piece != null && $respuesta === null && $serial === null){
               
            return ['existe'];

           }elseif($piece != null && $respuesta === 'si' && $serial === null){

            $piece->pp_cantidad = $piece->pp_cantidad + $cantidad;
            $piece->save();
            return ['message' => 'Cantidad actualizada con exito.'];

           }elseif($piece != null && $respuesta === 'no' && $serial === null){

            return ['message' => 'Cantidad no actualizada.'];

           }else{
               
            return ppInventory::create([
                'pp_description_id' => $request['pp_description_id'],
                'pp_serial' => $request['pp_serial'],
                'pp_cantidad' => $request['pp_cantidad'],
                'pp_cond' => $request['pp_cond'],
                'origin_id' => $request['origin_id'],
            ]);  

           }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePieceRequest $request, $id)
    {
        $piece = ppInventory::findOrFail($id);
        $this->validate($request,[
            'pp_serial' => 'max:20|unique:pp_inventories,pp_serial,'.$piece->id,
        ],
        [   'pp_serial.unique' => 'El serial ya se encuentra registrado.']);
        $piece->update($request->all());
        return ['message' => 'Pieza actualizada'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $piece = ppInventory::findOrFail($id);
        $piece->pp_status = false;
        $piece->pp_cantidad = 0;
        $piece->save();
        return ['message' => 'Desactivo su pieza correctamente.'];
    }

    public function search()
    {

        $search = \Request::get('q');
        if ($search != '') {

           $r = sgpExistencia_pp::with('paleta','exisEtiquetadas','numeroParte','posiciones.almacen')->whereHas('posiciones.almacen',function($q){
            $q->where('clasificacion','PV');
        })->WhereHas('numeroParte',function($q) use ($search){
               $q->where('pn','ilike','%'.$search.'%');
           })->paginate(12);
       }else{

           $r = sgpExistencia_pp::with('paleta','exisEtiquetadas','numeroParte','posiciones.almacen')->whereHas('posiciones.almacen',function($q){
            $q->where('clasificacion','PV');
        })->paginate(12);
       }
       return $r;

       /* if($search = \Request::get('q')){
            $resultado = \DB::table('pp_descriptions')
            ->select('pp_inventories.*','pp_descriptions.pp_np', 'pp_descriptions.pp_tipo', 'pp_descriptions.pp_description', 'pp_descriptions.pp_category_id', 'origins.origin')
            ->join('pp_inventories', 'pp_inventories.pp_description_id', '=', 'pp_descriptions.id')
            ->join('origins', 'pp_inventories.origin_id', '=', 'origins.id')
            ->where(function($query) use ($search){
                $query->where('pp_np', 'LIKE', "%$search%")
                        ->orWhere('pp_tipo', 'LIKE', "%$search%")
                        ->orWhere('pp_description', 'LIKE', "%$search%")
                        ->orWhere('pp_cantidad', 'LIKE', "%$search%")
                        ->orWhere('origin', 'LIKE', "%$search%")
                        ->orWhere('pp_cond', 'LIKE', "%$search%")
                        ->orWhere('pp_serial', 'LIKE', "%$search%");

            })->paginate(10);
        }else{
            $resultado = \DB::table('pp_descriptions')
            ->select('pp_inventories.*','pp_descriptions.pp_np', 'pp_descriptions.pp_tipo', 'pp_descriptions.pp_description', 'origins.origin')
            ->join('pp_inventories', 'pp_inventories.pp_description_id', '=', 'pp_descriptions.id')
            ->join('origins', 'pp_inventories.origin_id', '=', 'origins.id')
            ->paginate(10);
        }
        return $resultado;*/
    }

    public function busquedaA(Request $request)
    {
        $np = $request['busquedaA'];
        for($i=0; $i<5; $i++){
            $busqueda[$i] = \DB::table('pp_inventories')
            ->select('pp_inventories.*', 'pp_descriptions.id', 'pp_descriptions.pp_np', 'origins.origin')
            ->join('pp_descriptions', 'pp_descriptions.id', '=', 'pp_inventories.pp_description_id')
            ->join('origins', 'origins.id', '=', 'pp_inventories.origin_id')
            ->where([['pp_np', '=', [$np]],['pp_status', '=', TRUE]])
            ->where(function ($query) use ($i) {
                switch($i){
                    case 0: $query->where('pp_serial', '!=', NULL); break; // Total de piezas con serial
                    case 1: $query->where('pp_serial', '=', NULL); break; // Total de piezas sin serial
                    case 2: $query->where('pp_cond', '=', 'nuevo'); break; // Total de piezas nuevas
                    case 3: $query->where('pp_cond', '=', 'usado'); break; // Total de piezas usadas 
                    case 4: $query->where('pp_status', '=', TRUE); break; // Total en global
                }
            })
            ->select(\DB::raw('sum(pp_cantidad) as totales'))
            ->first();
        }
        for($i=0; $i<2; $i++){
            $result2[$i] = \DB::table('pp_inventories')
            ->select('pp_inventories.*', 'pp_descriptions.id', 'pp_descriptions.pp_np', 'origins.origin')
            ->join('pp_descriptions', 'pp_descriptions.id', '=', 'pp_inventories.pp_description_id')
            ->join('origins', 'origins.id', '=', 'pp_inventories.origin_id')
            ->where([['pp_np', '=', [$np]],['pp_serial', '!=', NULL],['pp_status', '=', TRUE]])
            ->where(function ($query) use ($i) {
                switch($i){
                    case 0: $query->where('pp_cond', '=', 'nuevo'); break; // Total de piezas nuevas
                    case 1: $query->where('pp_cond', '=', 'usado'); break; // Total de piezas usadas 
                    // case 2: $query->where('pp_status', '=', TRUE); break; // Total en global
                }
            })
            ->get();
        }
        $result = new \stdClass();
        $result->total_cs = $busqueda[0]->totales;
        $result->total_ss = $busqueda[1]->totales;
        $result->total_nuevo = $busqueda[2]->totales;
        $result->total_usado = $busqueda[3]->totales;
        $result->total_all = $busqueda[4]->totales;
        if($result->total_all != true){
            return abort(500, 'El código ingresado no existe.');
        }else{
            return [$result,$result2];
        }
    }

    public function ppGalpones(){
        return sgpAlmacen::where('clasificacion','PV')->orderBy('nombre', 'ASC')->get();
    }

    public function searchg(){
         $search = \Request::get('q');
        if ($search != '') {

           $r = sgpExistencia_pp::with('paleta','exisEtiquetadas','numeroParte','posiciones.almacen')->whereHas('posiciones.almacen',function($q){
            $q->where('clasificacion','PV');
        })->WhereHas('posiciones.almacen',function($q) use ($search){
               $q->where('id',$search);
           })->paginate(12);
       }else{

           $r = SsgpExistencia_pp::with('paleta','exisEtiquetadas','numeroParte','posiciones.almacen')->whereHas('posiciones.almacen',function($q){
            $q->where('clasificacion','PV');
        })->paginate(12);
       }
       return $r;
    }
}
