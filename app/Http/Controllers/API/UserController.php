<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\SupportChannel;
use App\canales_usuario;
use App\Clases\CedulaVE;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdatePerfilUserRequest;
use App\Http\Requests\CneUserRequest;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct() // Metodo de seguridad (Para no ingresar sin loguearte en cualquier ruta).
    {
        $this->middleware('auth:api');
    }

    public function index() // Retornar la vista de "Xs" usuarios existentes.
    {
        /*if (\Gate::allows('esAdmin') || \Gate::allows('esGerente')) {
        }*/
        /*dd(auth('api')->user()->id);*/
        $this->authorize('esAdmin');
        return User::with('usertype','usersupportchannel')->latest()->paginate(10);
    }

    public function store(Request $request) // Crear un nuevo usuario con los datos del Request.
    {
        User::create([
            'name' => $request['name'],
            'photo' => $request['photo'],
            'email' => $request['email'],
            'user_type_id' => $request['user_type_id'],
            'nacionalidad' => $request['nacionalidad'],
            'cedula' => $request['cedula'],
            'estado' => $request['estado'],
            'municipio' => $request['municipio'],
            'parroquia' => $request['parroquia'],
            'direccion' => $request['direccion'],
            'password' => md5($request->password),
            'nick' => $request->nick,
            'estado_user' => $request->estado_user
       ]);

        $user_id = User::latest()->first()['id'];
        foreach ($request['canales_soporte_id'] as $key => $canal_soporte_id) {
            canales_usuario::create([
                'canal_soporte_id' => $canal_soporte_id,
                'user_id' => $user_id
            ]);
        }
        return ['message' => 'Usuario Creado'];
    }

    public function show($id)
    {
        //
    }

    public function update(Request $request) // Actualizar los datos (Validacion con relación al id y encrypt del Password).
    {
        # dd($request->all());
        $this->validate($request,[
            'email' => 'required|string|max:150|unique:users,email,'.$request->id,
            'cedula' => 'required|string|max:20|unique:users,cedula,'.$request->id,
            'nick' => 'required|unique:users,nick,'.$request->id,
            'password' => 'required|min:6|unique:users,nick,'.$request->id
        ]);

        User::updateOrCreate([
            'id' => $request->id
        ],[
            'cedula' => $request->cedula,
            'name' => $request->name,
            'email' => $request->email,
            'user_type_id' => $request->user_type_id,
            'canal_soporte_id' => $request->canal_soporte_id,
            'estado' => $request->estado,
            'municipio' => $request->municipio,
            'parroquia' => $request->parroquia,
            'direccion' => $request->direccion,
            'nacionalidad' => $request->nacionalidad,
            'password' => md5($request->password),
            'nick' => $request->nick,
            'estado_user' => $request->estado_user
        ]);

        $user_id = $request->id;
        foreach ($request['canales_soporte_id'] as $key => $canal_soporte_id) {
            canales_usuario::create([
                'canal_soporte_id' => $canal_soporte_id,
                'user_id' => $user_id
            ]);
        }

        return ['message' => 'usuario actualizado'];
    }

    public function destroy($id) // Borrar el usuario por su ID.
    {
        $user = User::findOrFail($id);
        $fotoDefault = $user->photo;
            $fotoActual = public_path('images/perfil/').$fotoDefault;
            if (file_exists($fotoActual) && ($fotoDefault != 'profile.png')){
                @unlink($fotoActual);
            }
        $user->delete();
        return ['message' => 'Usuario eliminado.'];
        
    }

    public function updatePerfil(UpdatePerfilUserRequest $request)
    {
        $user = auth('api')->user();
        $fotoDefault = $user->photo;
        if($request->photo != $fotoDefault){
            $name = time().'.' . explode('/', explode(':', substr($request->photo, 0, strpos($request->photo, ';')))[1])[1];
            \Image::make($request->photo)->save(public_path('images/perfil/').$name);
            $request->merge(['photo' => $name]);
            $user->photo = $name;
            $fotoActual = public_path('images/perfil/').$fotoDefault;
            if (file_exists($fotoActual) && ($fotoDefault != 'profile.png')){
                @unlink($fotoActual);
            }
        }
        if (!empty($request->password)){
            $request->merge(['password' => md5($request['password'])]);
        }
        $user->update($request->all());
        return ['message' => 'Perfil editado.'];
    }

    public function perfil()
    {
        //return auth('api')->user()->with('usertype')->get();
        return User::with('usertype')->find(Auth::id());
    }

    public function operador()
    {
        return User::with('registrollamada')->find(Auth::id());
    }

    public function search()
    {
       if ($search = \Request::get('q')) {
           $users = user::where(function($query) use ($search){
            $query->where('name', 'ilike', "%$search%")
                ->orWhere('email', 'ilike', "%$search%")
                ->orWhere('user_type_id', 'ilike', "%$search%")
                ->orWhere('canal_soporte_id', 'ilike', "%$search%")
                ->orWhere('cedula', 'ilike', "%$search%")
                ->orWhere('nacionalidad', 'ilike', "%$search%")
                ->orWhere('nick', 'ilike', "%$search%");
           })->with('usertype','usersupportchannel')->paginate(12);
       }else{
            $users = User::latest()->paginate(12);
       }
       return $users;
    }

    public function cne(CneUserRequest $request)
    {

        $cneResultado = CedulaVE::get($request['nacionalidad'], $request['cedula']);
        return $cneResultado;
    }
}
