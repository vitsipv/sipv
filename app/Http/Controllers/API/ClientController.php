<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Client;
use App\Company;
use App\Clases\CedulaVE;
use App\Http\Requests\StoreClientRequest;
use App\Http\Requests\UpdateClientRequest;
use App\Http\Requests\StoreCompanyRequest;
use App\Http\Requests\UpdateCompanyRequest;
use App\Http\Requests\CneClientRequest;
use App\Http\Requests\SearchClientERequest;
use App\Http\Requests\SearchClientPRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Client::latest();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreClientRequest $request)
    {
        // $request['cedula'] = $request->nacionalidad.$request->cedula;
        return Client::create([
            'nacionalidad' => $request['nacionalidad'],
            'cedula' => $request['cedula'],
            'nombres' => $request['nombres'],
            'apellidos' => $request['apellidos'],
            'tlf1' => $request['tlf1'],
            'tlf2' => $request['tlf2'],
            'email1' => $request['email1'],
            'email2' => $request['email2'],
            'estado' => $request['estado'],
            'municipio' => $request['municipio'],
            'parroquia' => $request['parroquia'],
            'direccion' => $request['direccion'],
        ]);
    }

    public function storeCompany(StoreCompanyRequest $request)
    {
        $request['rif_emp'] = $request->rif_emp;
        return Company::create([
            'tipo_emp' => $request['tipo_emp'],
            'rif_emp' => $request['rif_emp'],
            'nombre_emp' => $request['nombre_emp'],
            'nombre_se' => $request['nombre_se'],
            'estado_emp' => $request['estado_emp'],
            'municipio_emp' => $request['municipio_emp'],
            'parroquia_emp' => $request['parroquia_emp'],
            'direccion_emp' => $request['direccion_emp'],
            'tlf1_emp' => $request['tlf1_emp'],
            'tlf2_emp' => $request['tlf2_emp'],
            'email_emp' => $request['email_emp'],
        ]);
    }

    public function update(UpdateClientRequest $request, $id)
    {
        $cliente = Client::findOrFail($id);
        $request->merge(['cedula' => $id]);
        $this->validate($request,[
            'cedula' => 'required|unique:clients,cedula,'.$cliente->id,
            'email1' => 'required|email|unique:clients,email1,'.$cliente->id,
            'email2' => 'required|email|unique:clients,email2,'.$cliente->id,
        ],
        [   'email1.required' => 'Ingrese un correo principal.',
            'email1.email' => 'El correo personal no es un correo válido',
            'email1.unique' => 'El correo personal ingresado ya esta en uso.',
            'email2.required' => 'Ingrese un correo secundario.',
            'email2.email' => 'El correo secundario no es un correo válido',
            'email2.unique' => 'El correo secundario ingresado ya esta en uso.',
        ]);
        $cliente->update($request->all());
    }

    public function updateCompany(UpdateCompanyRequest $request, $id)
    {
        $empresa = Company::findOrFail($id);
        $request->merge(['rif_emp' => $id]);
        $this->validate($request,[
            'rif_emp' => 'required|unique:companies,rif_emp,'.$empresa->id,
            'email_emp' => 'required|email|unique:companies,email_emp,'.$empresa->id,
        ],
        [   'rif_emp.required' =>'El número de RIF es obligatorio.',
            'rif_emp.unique' => 'El número de RIF ya se encuentra registrado.',
            'email_emp.required' => 'El correo es obligatorio.',
            'email_emp.email' => 'Ingrese una dirección de correo valida.',
            'email_emp.unique' => 'El correo ingresado ya esta en uso.',
        ]);
        $empresa->update($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //   
    }

    public function searchClientP(SearchClientPRequest $request)
    {
        $cedula = $request->nacionalidad.$request->cedula;
        try{
            $cliente = Client::findOrFail($cedula);
        }catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'El cliente con la Cedula: '.$cedula.' No esta registrada'], 404);
        }
        return $cliente;    
    }

    public function searchClientE(SearchClientERequest $request)
    {
        $rif_emp = $request->tipo_emp.$request->rif_emp;
        try{
            $empresa = Company::findOrFail($rif_emp);
        }catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'La empresa con el RIF: '.$rif_emp.' No esta registrada'], 404);
        }

        return $empresa;
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cne(CneClientRequest $request)
    {
        $cneResultado = CedulaVE::get($request['nacionalidad'], $request['cedula']);
        return $cneResultado;
    }
}
