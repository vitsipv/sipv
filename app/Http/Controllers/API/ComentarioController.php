<?php

namespace App\Http\Controllers\API;

use App\Comentario;
use App\TipoComentario;
use App\User;
use App\Caso;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ComentarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct() // Metodo de seguridad (Para no ingresar sin loguearte en cualquier ruta).
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        //return Comentario::with('tipo')->latest()->paginate(12);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        
        $this->validate($request,[
            'comentario' => 'required',
            'tipos' => 'required',
        ]);

        $id = Auth::id();
        $user = User::findOrFail($id);
        $usuario = $user->name;

        $estado = false;
        return Comentario::create([
            'comentario' => $request['comentario'],
            'tipo_comentario_id' => $request['tipos'],
            'usuario' => $usuario,
            'estado' => $estado,
            'numero_caso_id' => $request['numero_caso']
        ]);
    }

//estoy aqui
    public function destroy($id) // Borrar el usuario por su ID.
    {

        $Comentario = Comentario::findOrFail($id);
        $Comentario->delete();
        return ['message' => 'Usuario eliminado.'];
    }

    public function casos(Request $request){

        //dd($request);
        $ClienteId = $request->client_id;//$request->numero_caso;
        //$comentarios = Caso::with('comentariosCaso','tipo','cliente')->where('numero_caso_id',$numeroCaso)->get(); 
        $casos = Caso::with('empresa','cliente','modoAtencion', 'canalCompra','caseStatus','comentariosCaso')->where('client_id',$ClienteId)->latest()->paginate(5);
        return $casos;

    }

    public function ComentariosCasos(Request $request){
        $numeroCaso = $request->numero_caso;
        $comentario = Comentario::with('tipo')->where('numero_caso_id',$numeroCaso)->latest()->paginate(5);
        return $comentario;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
}
