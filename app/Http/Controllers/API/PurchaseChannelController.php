<?php

namespace App\Http\Controllers\API;

use App\PurchaseChannel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PurchaseChannelStoreRequest;

class PurchaseChannelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PurchaseChannel::latest()->paginate(5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PurchaseChannelStoreRequest $request)
    {
        PurchaseChannel::create([
            'nombre' => $request['nombre'],
        ]);
        return response('Creación exitosa', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PurchaseChannel  $purchaseChannel
     * @return \Illuminate\Http\Response
     */
    public function show(PurchaseChannel $purchaseChannel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PurchaseChannel  $purchaseChannel
     * @return \Illuminate\Http\Response
     */
    public function edit(PurchaseChannel $purchaseChannel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PurchaseChannel  $purchaseChannel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $canal = PurchaseChannel::findOrFail($id);
        $this->validate($request,[
            'nombre' => 'required|string|max:15|unique:purchase_channels,nombre,'.$canal->id,
        ]);
        $canal->update($request->all());
        return response('Actualización exitosa', 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PurchaseChannel  $purchaseChannel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PurchaseChannel::find($id)->delete();
        return response('Canal borrado exitosamente', 200);
    }
}
