<?php

namespace App\Http\Controllers\API;

use App\AttentionMode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AttentionModeStoreRequest;

class AttentionModeController extends Controller
{

    public function index()
    {
        return AttentionMode::latest()->paginate(5);
    }

    public function store(Request $request)
    {
        AttentionMode::create([
            'nombre' => $request['nombre'],
        ]);
        return response('Creación exitosa', 200);
    }

    public function update(Request $request, $id)
    {
        $modoAtencion = AttentionMode::findOrFail($request->$id);
        $this->validate($request,[
            'nombre' => 'required|string|max:15|unique:attention_modes,nombre,'.$modoAtencion->id,
        ]);
        $modoAtencion->update($request->all());
        return response('Actualización exitosa', 200);

    }

    public function destroy($id)
    {
        AttentionMode::find($id)->delete();
        return response('Modo de atención borrado exitosamente', 200);
    }
}