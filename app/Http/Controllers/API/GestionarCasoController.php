<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Client;
use App\Company;
use App\Caso;

class GestionarCasoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function vCaso(Request $request){


        $cedula = $request->nacionalidad.$request->cedula;
        $clientid= Client::select('id')->where('cedula','=',$cedula)->get(); 
        $clientid=$clientid->max('id'); //obtiene id del cliente segun la cedula.
        if ($clientid != null) {
            $casos= Caso::with('cliente','modoAtencion','canalCompra','caseStatus','diagnosticoCaso')->where('client_id',$clientid)->paginate(3);
            return [

            'pagination' => [
                'total' => $casos->total(),
                'current_page' => $casos->currentPage(),
                'per_page' => $casos->perPage(),
                'last_page' => $casos->lastPage(),
                'from' => $casos->firstItem(),
                'to' => $casos->lastPage(),
            ],
            'casos' => $casos
        ];
        }else{
            return response('Esto es un error de cliente', 404);
        }
        
    }

    public function jCaso(Request $request){
        $rif = $request->tipo_emp.$request->rif_emp;
        $companyid= Company::select('id')->where('rif_emp','=',$rif)->get(); 
        $companyid=$companyid->max('id'); //obtiene id del la empresa segun el rif.
        if ($companyid != null) {
            $casos= Caso::with('empresa','modoAtencion','canalCompra','caseStatus')->where('company_id',$companyid)->paginate(3);
            return [

                'pagination' => [
                    'total' => $casos->total(),
                    'current_page' => $casos->currentPage(),
                    'per_page' => $casos->perPage(),
                    'last_page' => $casos->lastPage(),
                    'from' => $casos->firstItem(),
                    'to' => $casos->lastPage(),
                ],
                'casos' => $casos
            ];
        }else{
            return response('Esto es un error de empresa', 404);
        }
    }

    public function nCaso(Request $request){

        $caso = $request->numero_caso;
        if ($caso != null) {
            $casos = Caso::with('empresa','cliente','modoAtencion', 'canalCompra','caseStatus')->where('numero_caso',$caso)->get();
            return $casos;
        }else{
            return response('Esto es un error de caso', 404);
        }
        
    }

    public function sCaso(Request $request){

        $serial = $request->serial_equipo;
        $caso = Caso::with('empresa','cliente','modoAtencion', 'canalCompra','caseStatus')->where('serial_equipo',$serial)->get();
        return $caso;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

       $this->validate($request,[
        'modo_atencion_id' => 'required',
        'canal_compra_id' => 'required',
        'obser_caso' => 'required',
        'falla_equipo' => 'required',
        'tipo_equipo' => 'required',
    ]);

       $caso = Caso::findOrFail($id);
       $caso->update($request->all());
       return response('Actualización exitosa', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
