<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use App\Caso;
use App\sgpExistencia_pp;
use App\PresupuestoStatus;
use App\Iva;
use App\Presupuesto;
use App\tecnico;
use App\DetallesPresupuesto;



class PresupuestoController extends Controller
{

   /* public function __construct() // Metodo de seguridad (Para no ingresar sin loguearte en cualquier ruta).
    {
        $this->middleware('auth:api');
    }*/
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'presupuesto' => 'required',
        ]);

        $dash = '-';
        
        $presupuesto = Presupuesto::first();
        if ($presupuesto == null) {
            $lastPresupuestoId = 1;
            $pre = "PPTO";                                                      
            $fecha = date('dmY');                                             
            $numero_presupuesto = $pre.$dash.$lastPresupuestoId.$dash.$fecha;
        }else{
            $lastPresupuestoId = Presupuesto::orderBy('id','desc')->first()->id+1;                                  
            $pre = "PPTO";                                                         
            $fecha = date('dmY');                                                   
            $numero_presupuesto = $pre.$dash.$lastPresupuestoId.$dash.$fecha;
        }

        $presupuestoStatus = PresupuestoStatus::all();
        $presupuestoStatusId = $presupuestoStatus->first()->id;

        $ivaId = Iva::where('activo','=',true)->first()->id;
        $presupuesto = new Presupuesto;
        $presupuesto->numero_presupuesto = $numero_presupuesto;
        $presupuesto->presupuesto = $request->presupuesto;
        $presupuesto->numero_caso = $request->numero_caso;
        $presupuesto->status_id = $presupuestoStatusId;
        $presupuesto->condiciones = null;
        $presupuesto->iva_id = $ivaId;
        $presupuesto->save();

        $presupuestos = [];
        foreach($request->arregloStock as $item){ //$intersts array contains input data
            $presupuesto = new DetallesPresupuesto();
            $presupuesto->numero_presupuesto = $numero_presupuesto;
            $presupuesto->np_piezas = $item['np_piezas'];
            $presupuesto->cant_piezas = $item['cant_piezas'];
            $presupuesto->piezas = $item['piezas'];
            $presupuesto->almacenes = $item['almacenes'];
            $presupuesto->precio_piezas = $item['precio_piezas'];
            $presupuesto->created_at = Carbon::now();
            $presupuesto->updated_at  = Carbon::now();
            $presupuestos[] = $presupuesto->attributesToArray();
        }
        DetallesPresupuesto::insert($presupuestos);

        return response('Creación exitosa', 200);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request,[
            'presupuesto' => 'required',
        ]);

        foreach($request->arregloStock as $item){ //$intersts array contains input data 
            
            $detalle = DetallesPresupuesto::where('numero_presupuesto','=',$item['numero_presupuesto'])->where('np_piezas','=',$item['np_piezas'])->get();
            
            if($detalle->isEmpty()){
                $detalle = new DetallesPresupuesto();
                $detalle->numero_presupuesto = $item['numero_presupuesto'];
                $detalle->np_piezas = $item['np_piezas'];
                $detalle->cant_piezas = $item['cant_piezas'];
                $detalle->piezas = $item['piezas'];
                $detalle->almacenes = $item['almacenes'];
                $detalle->precio_piezas = $item['precio_piezas'];
                $detalle->created_at = Carbon::now();
                $detalle->updated_at  = Carbon::now();
                $detalle->save();
            }
            else{
                $id2 = $detalle->max('id');
                $detalle = DetallesPresupuesto::findOrFail($id2);
                $detalle->update(['cant_piezas'=>$item['cant_piezas']]);
            }
            
        }

        $presupuesto = Presupuesto::findOrFail($id);
        $presupuesto->update($request->all());


        return response('Actualización exitosa', 200);
    }

    public function descontarPiezasAlmacen($arregloStock)
    {
        //dd($arregloStock);
        foreach($arregloStock as $item){
            /**
             * Identificamos la existencia a la cual se va a descontar 
             * tomando en cuenta el NP y el almacen donde se encuentra
             */

            $existencia_pp = sgpExistencia_pp::where('cantidad','>',0)
            ->with('numeroParte', 'posiciones.almacen', 'pieza')
            ->whereHas('numeroParte',function($query) use($item){
                $query->where('pn',$item['np_piezas']);
            })->whereHas('posiciones.almacen',function($query) use($item){
                $query->where('nombre',$item['almacenes']);
            })->first();

            

            if($item['piezas']=="Servicio"){
                //Si la pieza es "Servicio" No se hace nada, no se descuenta.
            }
            else
            { 
            /**
             * Descontamos la cantidad
             */

            $existencia_pp->cantidad = $existencia_pp->cantidad - $item['cant_piezas'];
            $existencia_pp->save();

            }
           
        }
    }

    public function procesarPresupuesto(Request $request)
    {   
        /**
         *  Descontar piezas
         */

        $this->descontarPiezasAlmacen($request->arregloStock);

        /**
         * Procesar presupuesto
         */
        $id=$request->id; 
        $status = Presupuesto::findOrFail($id);
        $status->status_id = 3;
        /* bla */
        if ($request->condiciones === false || $request->condiciones === 0) {
            $request->condiciones = 'PC';
        }else{
            $request->condiciones = 'SERVIDOR';
        }

        $status->update(['status_id'=> $status->status_id, 'condiciones' => $request->condiciones]);
        return response('Actualización exitosa', 200);




    }

    public function diagnosticoTecnico(Request $request)
    {
        
        $analistaPresupuestoid = Auth::id();
  
        return $casos =Caso::with('caseStatus','cliente','diagnosticoCaso','detallesDiagnostico','empresa')->doesntHave('presupuestoCaso')->where('estado_caso_id','=', 2)->paginate();
          
    }


    /*public function htmlpdf(){
        return view('invoice');

    }

    public function generate()
    {
        $data = ['titulo' => 'Laravel 5.8 HTML to PDF'];
        $pdf = PDF::loadView('invoice', $data);
        return $pdf->download('demonutslaravel.pdf');

    }*/

    public function exportpdf(Request $request){
        /* info_pdf es un array que contiene la información del $request menos el arregloStock y en un array
        */
        $cabecera_pdf = $request->except('arregloStock');
        $iva2 = Iva::where('id','=',$request['iva_id'])->first()->porcentaje;
        $iva = $iva2 / 100;
        $sub=0;
        $presupuestos = [];
        foreach($request->arregloStock as $item){ //$intersts array contains input data
            $presupuesto = new DetallesPresupuesto();
            $presupuesto->cant_piezas = $item['cant_piezas'];
            $presupuesto->piezas = $item['piezas'];
            $presupuesto->precio_piezas = $item['precio_piezas'];//round($item['precio_piezas']+($item['precio_piezas']*$iva),2);
            $presupuesto->total = round($presupuesto->precio_piezas*$item['cant_piezas'],2);
            $presupuestos[] = $presupuesto->ToArray();
        }

        foreach($presupuestos as $item){
            $sub=$sub +  $item['total']; 
        }

        $ivaTotal = round($sub*$iva,2);
        $montoFinal = round($sub+$ivaTotal);//$sub+$ivaTotal;

        return ["cabecera_pdf"=>$cabecera_pdf,"presupuesto"=>$presupuestos,"subtotal"=>$sub, "ivaTotal"=>$ivaTotal, "montoFinal"=>$montoFinal,"iva"=>$iva2,];
    }


    /*
    *
    *   EXPORT PDF VIEJO
    *
    */
    
/*
    public function exportpdf(Request $request){ 
        $iva2 = Iva::where('id','=',$request['iva_id'])->first()->porcentaje;
        $iva = $iva2 / 100;
        $sub=0;
        $presupuestos = [];
        foreach($request->arregloStock as $item){ //$intersts array contains input data
            $presupuesto = new DetallesPresupuesto();
            $presupuesto->cant_piezas = $item['cant_piezas'];
            $presupuesto->piezas = $item['piezas'];
            $presupuesto->precio_piezas = $item['precio_piezas'];//round($item['precio_piezas']+($item['precio_piezas']*$iva),2);
            $presupuesto->total = round($presupuesto->precio_piezas*$item['cant_piezas'],2);
            $presupuestos[] = $presupuesto->attributesToArray();
        }

        foreach($presupuestos as $item){
            $sub=$sub +  $item['total']; 
        }

        $ivaTotal = round($sub*$iva,2);
        $montoFinal = $sub+$ivaTotal;

        return view("invoice", ["request"=>$request,"presupuesto"=>$presupuestos,"subtotal"=>$sub, "ivaTotal"=>$ivaTotal, "montoFinal"=>$montoFinal,"iva"=>$iva2]);
    }

    */

    public function presupuestos(Request $request)
    {
        
        $analistaPresupuestoid = Auth::id();
        $presupuestos =Presupuesto::with('iva','condicionesPresupuesto','detallesPresupuesto','caso','cliente','empresa')->paginate(10);
        return $presupuestos;
          
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function destroy($piezasEliminar)
    {
        $piezasEliminar2 = explode(',', $piezasEliminar);
        foreach($piezasEliminar2 as $pieza){
            DetallesPresupuesto::find($pieza)->delete(); 
        }
    }*/

    public function eliminarPiezas(Request $request, $piezasEliminar)
    {
        //dd($piezasEliminar);
        $piezasEliminar2 = explode(',', $piezasEliminar);
        foreach($piezasEliminar2 as $pieza){
            DetallesPresupuesto::find($pieza)->delete(); 
        }
    }
}
