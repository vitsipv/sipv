<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;

class PermisosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = \App\auth::paginate(10);
        return $user;

    }

    public function filters(Request $request)
    {
        
        $search = \App\auth::where(function($query) use ($request){
            $query->where('nick','ilike',"%$request->q%")
                  ->orWhere('name','ilike',"%$request->q%");
        })->paginate(10);
        return $search;

    }

    public function listaModulos()
    {
        $user = \App\Modulos::all();
        $collect = collect();
        
        foreach ($user as $key => $value) {
            $collect->push($value->nombre_modulo);
        }
        
        
        return $collect;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function registro_P(Request $request)
    {
    #dd($request->all());
        DB::beginTransaction();
        try{
         $user = \App\User::where('nick',$request->nick)->first()->user_type_id;       
         if($user === 1){
            $cod = ['crear','ver','editar','borrar'];
            
            foreach($request->modulos as $key){
                for ($i=0; $i < count($cod); $i++) { 
                    \App\Permisos::updateOrCreate([
                        'usuario' => $request->nick,
                        'modulo' => $key,
                        'codpermission' => $cod[$i]
                    ]);
                }
                
            }
        }else{
            $i = 0;
            foreach ($request->modulos as $key) {
                if($i < count($request->contenido)){
                    if($request->contenido[$i] === true){
                        \App\Permisos::updateOrCreate([
                            'codpermission' => 'ver',
                            'usuario' => $request->nick,
                            'modulo' => $key
                        ]);
                    }
                }
                if($i < count($request->edit)){
                    if($request->edit[$i] === true){
                        \App\Permisos::updateOrCreate([
                            'codpermission' => 'editar',
                            'usuario' => $request->nick,
                            'modulo' => $key
                        ]);
                    }
                }
                if($i < count($request->delete)){
                    if($request->delete[$i] === true){
                        \App\Permisos::updateOrCreate([
                            'codpermission' => 'borrar',
                            'usuario' => $request->nick,
                            'modulo' => $key
                        ]);
                    }
                }
                if($i < count($request->crear)){
                    if($request->crear[$i] === true){
                        \App\Permisos::updateOrCreate([
                            'codpermission' => 'crear',
                            'usuario' => $request->nick,
                            'modulo' => $key
                        ]);
                    }
                }
                $i++;
            }
        }  
            
        DB::commit();
        }catch(Exception $e){
            DB::rollback();
            return $e;
        }
        
        
    }

    public function destroy(Request $request)
    {
       # dd($request->all());
        DB::beginTransaction();
        try {
            if(count($request->modulosDelete) > 0){
                for ($i=0; $i < count($request->modulosDelete); $i++) { 
                    $search = \App\Permisos::where(function ($q) use ($request,$i)
                    {
                        $q->where('modulo',$request->modulosDelete[$i]['item'])
                          ->where('usuario',$request->modulosDelete[$i]['user'])
                          ->where('codpermission',$request->modulosDelete[$i]['cod']);
                    })->delete();
                   
                }

            }else{
                throw new Exception("Error no has añadido los permisos");
            
            }
            
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return json_encode($e->getMessage());
        }

        return json_encode("Se ha Eliminado con exito");
            
    }

    public function show(Request $request)
    {
        $search = \App\Permisos::where(function ($query) use ($request)
        {
            $query->where('usuario',$request->q)
            ->whereNull('deleted_at');
        })->get();

        return $search;
    }
    public function modulosSearch(Request $request){
        

        $array = explode('/',$request->q);
        
        $search = \App\Permisos::where(function ($query) use ($array)
        {
            $query->where('modulo','ilike',"%$array[0]%")
                  ->where('usuario',$array[1]);

        })->get();
        return $search;
    }

    public function middlewares(Request $request)
    {
        
        $array = explode('/',$request->user);
        
        try {
            $modulosSearchAuthUser = \App\Modulos::where('ruta_modulo',"/$array[1]")->first()->nombre_modulo;
            
            $permisos = \App\Permisos::where(function($query) use ($modulosSearchAuthUser,$array)
            {
                $query->where('modulo',$modulosSearchAuthUser)
                ->where('usuario',$array[0])
                ->where('codpermission','ver');
    
            })->first();
            
            if($permisos === null) throw new \Exception("Error No se Encuentra el permisos", 1);
            
                 
        } catch (\Exception $th) {
            return $th->getMessage();
        }

        return true;
        

    }

    public function aprobarPermisos(Request $request)
    {
        #dd($request->all());
        $array = explode('/',$request->ruta);
        $modulosSearchAuthUser = \App\Modulos::where('ruta_modulo',"/$array[1]")->first()->nombre_modulo;
        $permisos = \App\Permisos::where(function($q) use ($array,$modulosSearchAuthUser)
        {
            $q->where('modulo',$modulosSearchAuthUser)
            ->where('usuario',$array[2]);

        })->get();
        #dd($permisos);
        $aprobar = collect();
        foreach ($permisos as $key) {
            switch($key->codpermission){
                case 'crear':
                 $aprobar->push('crear');
                break;
                
                case 'borrar':
                 $aprobar->push('borrar');
                break;
                case 'editar':
                    $aprobar->push('editar');
                break;
                
            }

        }
        if(count($aprobar) > 0) return json_encode($aprobar);
        else return json_encode($aprobar);
    }
public function agregarPermisosAdmin(Request $request)
{
    #dd($request->all());
    $user = \App\User::where('nick',$request->nick)->first()->user_type_id;

    return json_encode($user);
}

}

