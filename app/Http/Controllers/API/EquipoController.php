<?php

namespace App\Http\Controllers\API;

use App\Equipo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreEquipoRequest;
use App\sgpExistencia_pt;
use App\sgpAlmacen;
use App\sgpModelo;

class EquipoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       /* return Equipo::latest()->paginate(12);*/
        return SgpExistencia_pt::with('paleta','posiciones.almacenpt','version.modelos')->whereHas('posiciones.almacen',function($q){
            $q->where('clasificacion','!=','GP');
        })->where('notas_entregas_id',null)->orderBy('id','ASC')->paginate(12);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEquipoRequest $request)
    {

        if ($request['imagen']) {
            $name = time().'.' . explode('/', explode(':', substr($request['imagen'], 0, strpos($request['imagen'], ';')))[1])[1];
            \Image::make($request['imagen'])->save(public_path('images/equipos/').$name);
            $request->merge(['imagen' => $name]);
        }else if($request['imagen'] === null){
            $request->merge(['imagen' => 'vitlogo.png']);
        }

        $descripcion = '';
        foreach($request['descripciones'] as $x){
            $descripcion = $descripcion.'|'.$x.'|';
        }


       $h = Equipo::create([
            'imagen' => $request['imagen'],
            'modelo' => $request['modelo'],
            'version' => $request['version'],
            'descripcion' => $descripcion,
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Equipo  $equipo
     * @return \Illuminate\Http\Response
     */
    public function show(Equipo $equipo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Equipo  $equipo
     * @return \Illuminate\Http\Response
     */
    public function edit(Equipo $equipo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Equipo  $equipo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $equipo = Equipo::findOrFail($id);
        $fotoActualEquipo = $equipo->imagen;
            if ($request['imagen'] != $fotoActualEquipo) {
                $name = time().'.' . explode('/', explode(':', substr($request['imagen'], 0, strpos($request['imagen'], ';')))[1])[1];
                \Image::make($request['imagen'])->save(public_path('images/equipos/').$name);
                $data = $request->imagen;
                $data['imagen'] = $name;
                $request->merge(['imagen' => $data]);
                $fotoActual = public_path('images/equipos/').$fotoActualEquipo;
                if (file_exists($fotoActual)){
                    @unlink($fotoActual);
                }
            }
            $descripcion = '';
                foreach($request['descripciones'] as $x){
                    $descripcion = $descripcion.'|'.$x.'|';
                }
            $request->merge(['descripcion' => $descripcion]);
            $request->replace($request->except('descripciones'));

            $equipo->update([
                'imagen' => $request['imagen'],
                'modelo' => $request['modelo'],
                'version' => $request['version'],
                'descripcion' => $descripcion,
            ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Equipo  $equipo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $equipo = Equipo::findOrFail($id);
        $equipo->status = false;
        $equipo->update();
    }

    public function search(){
           $search = \Request::get('q');
        if ($search != '') {

           $r = SgpExistencia_pt::with('paleta','posiciones.almacenpt','version.modelos')
           ->has('posiciones')
           ->has('posiciones.almacenpt')
           ->where('notas_entregas_id',null)
           ->WhereHas('version.modelos',function($q) use ($search){
                    $q->where('modelo','ilike','%'.$search.'%');
            })->orWhere([['sn_equipo','ilike','%'.$search.'%'],['notas_entregas_id',null]])
           ->paginate(12);

            if(!$r->isEmpty())return $r;
            else $r = $this->index();
       }else{
        $r = $this->index();
       }
       return $r;
    }

    public function searchg(){
        $search = \Request::get('q');
        if ($search != '') {

           $r = SgpExistencia_pt::with('paleta','posiciones.almacenpt','version.modelos')
            ->has('posiciones')
            ->where('notas_entregas_id',null)
            ->WhereHas('posiciones.almacenpt',function($q) use ($search){
                $q->where('id',$search);
            })
            ->where('notas_entregas_id',null)
            ->orderBy('id','ASC')
            ->paginate(12);

           if(!$r->isEmpty())return $r;
           #else $r = $this->index();
       }else{
        $r = $this->index();
       }
       return $r;
    }

    public function ptGalpones(){
        return sgpAlmacen::where('clasificacion','!=','GP')->orderBy('nombre', 'ASC')->get();
    }

    public function ptCategorias(){
        return sgpModelo::groupBy('categorias_codigo')->select('categorias_codigo')->get();
    }

    public function searchc(){
        $search = \Request::get('q');
        if ($search != '') {

           $r = SgpExistencia_pt::with('paleta','posiciones.almacenpt','version.modelos')->has('posiciones')->has('posiciones.almacenpt')->where('notas_entregas_id',null)->WhereHas('version.modelos',function($q) use ($search){
               $q->where('categorias_codigo','=',$search);
           })->where('notas_entregas_id',null)->orderBy('id','ASC')->paginate(12);
           if(!$r->isEmpty())return $r;
           #else $r = $this->index();
       }else{
           $r = $this->index();
       }
       return $r;
    }
}
