<?php

namespace App\Http\Controllers\API;

use App\sgpSerialPT;
use App\sgpOrdenP;
use App\sgpModelo;
use App\sgpModeloV;
use App\sgpPiezaModelo;
use App\sgpEmpleados;
use App\sgpNumeroParte;
use App\sgpCaracteristicaP;
use App\sgpAlmacen;
use App\sgpExistencia_pp;
use App\sgpExistencia_pt;
use App\sgpPrecios;
use App\sgpPaleta;
use App\sgpExistenciaProduccion;
use App\sgpPieza;
use App\sgpVehiculos;
use App\sgpConductores;
use App\sgpUsuariosAlmacenes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection as Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;


class sgpController extends Controller
{

    public function indexUsuariosAlmacenistas()
    {
        $empleados_almacenistas = sgpUsuariosAlmacenes::with('empleado_usuario_almacen')->get()->pluck('empleado_usuario_almacen')->unique()->values()->all();
        return $empleados_almacenistas;
    }

    public function indexEmpleadosPostVenta()
    {
        //
    }

    public function indexSerialesPT()
    {
        return sgpSerialPT::paginate();
    }

    public function indexOrdenP()
    {
        return sgpOrdenP::paginate();
    }

    public function indexPiezas(){
        return sgpPieza::orderBy('nombre','asc')->get();
    }

    public function indexPiezasEtiquetadas($pieza_id)
    {
        return sgpExistenciaProduccion::where('existencias_pp_id', $pieza_id)->whereNull('existencias_pt_id')->paginate(3);
    }

    public function indexExistenciaProduccion()
    {
        return sgpExistenciaProduccion::orderBy('id', 'DESC')->get();
    }

    public function piezasEtiquetadas(){
        return sgpPieza::with('existenciaProduccion')->paginate(10);
    }

    public function indexModelos()
    {
        return sgpModelo::get();
    }
    
    public function indexModelosV()
    {
        return sgpModeloV::get();
    }

    public function indexPiezasM()
    {
        return sgpPiezaModelo::get();
    }

    public function indexNumerosP()
    {
        return sgpNumeroParte::orderBy('pn','DESC')->get();
    }
    
    public function indexCP()
    {
        return sgpCaracteristicaP::get();
    }

    public function indexExistenciaPP()
    {
        return sgpExistencia_pp::where('cantidad','>',0)->get();
    }

    public function indexLote($fecha)
    {
        return DB::connection('dbsgp')->table('fechas_contenedores')->where('fecha',$fecha)->value('lote_max');
    }

    private function filtrosEstandar($index, $filtro, $posicion)
    {
        /* ----------------------------------------*
        *         Filtros de Estandar              *
        * -----------------------------------------*
        */

        // *** Condicional de posicion ***
        $index->when($posicion=="true",
                    //Si $posicion = true 
                    function ($query, $posicion){
                        return $query->whereHas('posiciones',function($query){
                                    $query->where('posicion','--');
                                });
                    //Si $posicion = false
                },  function ($query, $posicion) {
                         return $query->whereHas('posiciones',function($query){
                                    $query->where('posicion','!=','--');
                                });
                });

        // *** Condicional de almacen ***

        $index->when($filtro->almacen,
            function($query) use($filtro){
                return $query->whereHas('posiciones.almacen', function($query) use($filtro){
                    $query->where('id',$filtro->almacen->id);
                });
            });

        // *** Condicional de fecha_contenedor ***

        $index->when($filtro->fecha_entrada,
            function($query) use($filtro){
                return $query->whereHas('paleta', function($query) use($filtro){
                    $query->whereDate('updated_at',$filtro->fecha_entrada);
                }); 
            });

        return $index;
    }

    public function indexExistenciaPPInventario($posicion, $filtro_encoded)
    {
        $filtro = json_decode($filtro_encoded);

        // Traer todas las partes y piezas del almacen de postventa
        $index = sgpExistencia_pp::where('cantidad','>',0)
                ->with('posiciones.almacen', 'numeroParte.caracteristicaP', 'paleta.fecha_contenedor', 'pieza', 'exisEtiquetadas')
                ->whereHas('posiciones.almacen',function($query){
                    $query->where('clasificacion','PV');
                });
        $index = $this->filtrosEstandar($index, $filtro, $posicion);

        /* ----------------------------------------*
        *         Filtros de Partes y piezas       *
        * -----------------------------------------*
        */

        // *** Condicional de fecha_contenedor ***

        $index->when($filtro->fecha_contenedor,
            function($query) use($filtro){
                return $query->whereHas('paleta.fecha_contenedor', function($query) use($filtro){
                    $query->where('fecha',$filtro->fecha_contenedor);
                }); 
            });

        // *** Condicional de piezas ***

        $index->when($filtro->piezas,
            function($query) use($filtro){
                return $query->whereHas('pieza', function($query) use($filtro){
                    $query->where('id',$filtro->piezas->id);
                });
            });

        // *** Condicional de numero_parte ***

        $index->when($filtro->numero_parte,
            function($query) use($filtro){
                return $query->whereHas('numeroParte', function($query) use($filtro){
                    $query->where('id',$filtro->numero_parte->id);
                }); 
            });

        // *** Condicional de Numero de calidad (NC) ***

        $index->when($filtro->numero_calidad != 'Todos',
            function($query) use($filtro){
                if ($filtro->numero_calidad == 'NC') {
                    return $query->has('exisEtiquetadas','>',0);
                }
                else{
                    return $query->has('exisEtiquetadas','=',0);
                }
            });     
            
            


        return $index->get();

    }

    public function indexAlmacenes()
    {
        return sgpAlmacen::where('clasificacion','=','PV')->where('eliminado','=',FALSE)->get();
    }

    public function indexEmpleados()
    {
        return sgpEmpleados::get();
    }

    public function indexVehiculos()
    {
        return sgpVehiculos::get();
    }
    public function indexConductores()
    {
        return sgpConductores::get();
    }
    public function indexPaleta()
    {
        return sgpPaleta::orderBy('id', 'DESC')->get();
    }

    public function indexPosicion()
    {
        return sgpPosicion::get();
    }

    public function indexMovimientoInventario()
    {
        return sgpMovimientoInventario::paginate();
    }

    public function existenciaEtiquetadas(){
        return sgpExistenciaProduccion::paginate();
    }

    public function Precios()
    {
        $precios = sgpPrecios::with('numeroParte')->where('versiones_id','=',null)->latest()->get();
        $precios = $precios->unique('numeros_partes_id');
        return $precios;
    }

    public function comprobarSE(Request $request)
    {
        try{
           $resultado = sgpSerialPT::findOrFail($request['serial_equipo']);
       }catch (ModelNotFoundException $e) {
        if ($request['serial_equipo'] == '') {
            return response()->json($request['serial_equipo'], 404);
           
        }else{
            return response()->json(['message' => 'No existe el serial '.$request['serial_equipo']], 404);
        }
        
       }


        $resultado2 = sgpOrdenP::where('codigo_produccion', $resultado->cod_produccion)->first();
        $resultado3 = sgpModeloV::where('id', $resultado2->versiones_id)->first();

        $resultado3 = sgpModelo::where('id', $resultado3->modelos_ensamblar_id)->first();
        return $resultado3;
    }

    public function piezaAlmacen($almacen){

        if($almacen==='PUNTO FIJO - CUARTO DE P'){
            $almacen='PUNTO FIJO - CUARTO DE P/P';
        }

        if($almacen==='ALMACEN DE STOCK DE PRUEBAS SERVIDORES'){

        $almacen = sgpAlmacen::where('iniciales','=','ASPS')->first();
        $id= $almacen->id;

            return $pieza = sgpExistencia_pp::with('pieza','numeroParte.caracteristicaP')
            ->whereHas('posiciones',function($q){
              $q->where('cantidad','>',0);
            })->whereHas('posiciones',function($q) use ($id){
                $q->where('almacenes_id','=',$id);
            })->get();
        }

        else{
            //funcion que obtiene el id del almacen recibido de la vista y evalua la existencia de Piezas en ese almacen
        $almacen = sgpAlmacen::where('nombre','=',$almacen)->first();
        $id= $almacen->id;
        // query relacional, obtiene las piezas segun la ubicación de paletas en el almacen especifico
        
        return $pieza = sgpExistencia_pp::with('pieza','numeroParte.caracteristicaP')
        ->whereHas('posiciones',function($q){
          $q->where('cantidad','>',0);
        })->whereHas('posiciones',function($q) use ($id){
            $q->where('almacenes_id','=',$id);
        })->orderBy('id')->get();
        }
        
    }

    public function inventario($slash,$almacen){
        if($almacen==='PUNTO FIJO - CUARTO DE P'){
            $almacen='PUNTO FIJO - CUARTO DE P/P';
        }
        try{
            $almacen = sgpAlmacen::where('nombre','=',$almacen)->first();
            if ($almacen == null) {
            }
            $id= $almacen->id;
            $pieza = explode(' - ',$slash);  
        }

        catch(Exception $e) {
          echo 'Message: ' .$e->getMessage();
        }

        $pieza2 = $pieza[1];
        $inventario = sgpExistencia_pp::with('pieza','numeroParte')
        ->whereHas('posiciones',function($q){
          $q->where('cantidad','>',0);
        })->whereHas('posiciones',function($q) use ($id){
            $q->where('almacenes_id','=',$id);
        })->whereHas('numeroParte',function($q) use ($pieza2){
            $q->where('pn','=',$pieza2);
        })->limit(1)->get();
        

        $npid = sgpNumeroParte::where('pn','=',$pieza2)->first()->id;
        $precio = sgpPrecios::where('numeros_partes_id','=',$npid)->orderBy('id','DESC')->first();
        
        $inventario[0]->precio = $precio;
        return $inventario;
      
    }

    public function cantidadPieza($cantidadPieza, $slash, $almacen){

        if($almacen==='PUNTO FIJO - CUARTO DE P'){
            $almacen='PUNTO FIJO - CUARTO DE P/P';
        }
        $pieza = explode(' - ',$slash);
        $almacen = sgpAlmacen::where('nombre','=',$almacen)->first();
        $id= $almacen->id;

        $pieza2 = $pieza[1];

        $existencia = sgpExistencia_pp::with('pieza','numeroParte')
        ->whereHas('posiciones',function($q){
          $q->where('cantidad','>',0);
        })->whereHas('posiciones',function($q) use ($id){
            $q->where('almacenes_id','=',$id);
        })->whereHas('numeroParte',function($q) use ($pieza2){
            $q->where('pn','=',$pieza2);
        })->limit(1)->first();
 
        $cant = $existencia->cantidad;

        if($cant>0 && $cant>$cantidadPieza){
            $cantidadPieza=$cantidadPieza+1;
            return $cantidadPieza;
        }
        else{
            return $cantidadPieza;
        }

    }

    public function menosCantidadPieza($cantidadPieza ){
        
        if ($cantidadPieza>1) {
            return $cantidadPieza-1;
        }else{
            return $cantidadPieza;
        }
    
        
    }

    public function buscarPiezasS(Request $request)
    {

        $explodeR = null; // Se usa para guardar todas las descripciones de piezas.
        $modeloV = sgpModeloV::orderBy('id','desc')
        ->where('modelos_ensamblar_id', $request['modelo_equipo'])->get();

        foreach($modeloV as $indexKey => $m){
            $piezasM[$indexKey] = sgpPiezaModelo::orderBy('id', 'desc')
            ->where('versiones_id', $m->id)->get();     
        }

        for ($i=0; $i < count($piezasM); $i++) { 
            foreach($piezasM[$i] as $indexKey => $pm){
                $numerosP[$i][$indexKey] = sgpNumeroParte::orderBy('id', 'desc')
                ->where('id', $pm->numeros_partes_id)->first();
                if ($numerosP[$i][$indexKey] != null) {
                    $explodeR[$i][$indexKey] = explode('-',$numerosP[$i][$indexKey]->caracteristicas_piezas_ids);
                }else{
                    $explodeR[$i][$indexKey] = '';
                }
            }
        }

        if ($explodeR != null) {
                for ($i=0; $i <= count($explodeR); $i++) { 

                    if (isset($explodeR[$i]) != false) {
                        foreach($explodeR[$i] as $indexKey => $ex){
                            for ($ii=0; $ii < 6 ; $ii++) {
                                  $descrip[$i][$indexKey][$ii] = sgpCaracteristicaP::orderBy('id', 'desc')
                                    ->where('id', $ex[$ii])->first();  
                            }
                        }
                    }
            }
        }else{
            return response('No hay piezas asociadas.', 404);
        }

        // Parte encargada de pegar las descripciones.
        for ($i=0; $i < count($descrip); $i++) {
            foreach($descrip[$i] as $indexKey => $des){
                $descripPegada[$i][$indexKey] = null;
                for ($ii=0; $ii < 6 ; $ii++) {

                    if ($descrip[$i][$indexKey][$ii] != null) {
                        $pegarDescrip[$i][$indexKey][$ii] = $descrip[$i][$indexKey][$ii]->descripcion;
                        $descripPegada[$i][$indexKey] = $descripPegada[$i][$indexKey].' '.$pegarDescrip[$i][$indexKey][$ii];
                    }else{
                        $descrip[$i][$indexKey][$ii] = null;
                    }
                }   
            }
        }

        // Pegar np a las discripciones.
        for($i=0;$i < count($piezasM); $i++){
            for($ii=0;$ii < count($descripPegada[$i]); $ii++){
                if ($ii === 0) {
                    $resultado[$i] = array(['np' => $numerosP[$i][$ii]->pn,'desc' => $descripPegada[$i][$ii]]);
                }else{
                    $resultado[$i][$ii] = ['np' => $numerosP[$i][$ii]->pn,'desc' => $descripPegada[$i][$ii]];
                }
                
            }
        }      
        /*$resultado[$i][$ii] = [$numerosP[$i][$ii]->pn.' |'.$descripPegada[$i][$ii]];*/
        return response($resultado, 200);
    }

}
