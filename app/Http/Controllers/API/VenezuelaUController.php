<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Estado;
use App\Municipio;

class VenezuelaUController extends Controller
{
    public function estados(){
    	return Estado::orderBy('estado', 'ASC')->get(['id', 'estado']);
    }
    public function municipios(Estado $estado){
    	return $estado->municipios()->orderBy('municipio', 'ASC')->get(['id', 'municipio']);
    }
    public function parroquias(Municipio $municipio){
    	return $municipio->parroquias()->orderBy('parroquia', 'ASC')->get(['id', 'parroquia']);
    }
}
