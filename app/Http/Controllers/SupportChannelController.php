<?php

namespace App\Http\Controllers;

use App\SupportChannel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SupportChannelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return SupportChannel::get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SupportChannel  $supportChannel
     * @return \Illuminate\Http\Response
     */
    public function show(SupportChannel $supportChannel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SupportChannel  $supportChannel
     * @return \Illuminate\Http\Response
     */
    public function edit(SupportChannel $supportChannel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SupportChannel  $supportChannel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SupportChannel $supportChannel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SupportChannel  $supportChannel
     * @return \Illuminate\Http\Response
     */
    public function destroy(SupportChannel $supportChannel)
    {
        //
    }
}
