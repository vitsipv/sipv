<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {      
        
          $user = \App\auth::where('nick',$request->loguse)
                  ->where('password',md5($request->pass))->where('estado_user',1)->first();
   
          if($user !== null){
            
            Auth::login($user);
             
             return redirect('/home');
          }else{
            return "Sus Credenciales no estan registrada en nuestra Bases de datos";
          }


          $user2 = \App\auth::where('email',$request->loguse)
                  ->where('password',md5($request->pass))->first();
                  if($user2 !== null){
            
                    Auth::login($user2);
                     
                     return redirect('/home');
                  }
    }
}
