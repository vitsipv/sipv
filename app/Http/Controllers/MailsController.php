<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\TestMail;

class MailsController extends Controller
{
    public function Mensaje(){

        $details= [
            'title'=> 'Mail from Surfside Media',
            'body'=> 'This is aform testing email using smtp'
        ];
        
        \Mail::to('thenewstart0111@gmail.com')->send(new TestMail($details));
    
        return "email has been sent";

    }

}
