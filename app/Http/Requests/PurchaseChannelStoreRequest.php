<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PurchaseChannelStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|string|max:30|unique:purchase_channels',
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El nombre del canal de compra es obligatorio.',
            'nombre.unique' => 'El nombre ya ha sido registrado.',
            'nombre.max' => 'El nombre no debe ser mayor que 15 caracteres.',
        ];
    }
}
