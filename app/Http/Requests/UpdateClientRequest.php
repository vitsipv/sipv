<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nacionalidad' => 'required',
            'nombres' => 'required',
            'apellidos' => 'required',
            'tlf1' => 'required|min:18|max:18',
            'tlf2' => 'required|min:18|max:18',
            'estado' => 'required',
            'municipio' => 'required',
            'parroquia' => 'required',
            'direccion' => 'required',
        ];
    }

    public function messages()
    {
        return[
            'cedula.required' => 'El campo cédula es obligatorio.',
            'nacionalidad.required' => 'Seleccione un tipo de cédula.',
            'nombres.required' => 'Ingrese sus nombres.',
            'apellidos.required' => 'Ingrese sus apellidos.',
            'tlf1.required' => 'Ingrese un numero de teléfono móvil.',
            'tlf1.min' => 'El teléfono personal debe contener al menos 10 numeros.',
            'tlf2.required' => 'Ingrese un numero de teléfono de habitación.',
            'tlf2.min' => 'El teléfono de habitación debe contener al menos 10 numeros.',
            'estado.required' => 'Ingrese su estado.',
            'municipio.required' => 'Ingrese su municipio.',
            'parroquia.required' => 'Ingrese su parroquia.',
            'direccion.required' => 'Ingrese su dirección.',
        ];
    }
}
