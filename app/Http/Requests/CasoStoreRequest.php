<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CasoStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'serial_equipo' => 'unique:casos',
            'modelo_equipo' => 'required',
            'canal_compra_id' => 'required',
            'obser_caso' => 'required',
            'fecha_adqui' => 'required',
            'modo_atencion_id' => 'required',
            'falla_equipo' => 'required',
            'piezas_sugeridas' => 'required',
        ];
    }
}
