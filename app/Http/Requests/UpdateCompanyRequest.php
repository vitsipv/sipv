<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tipo_emp' => 'required',
            'nombre_emp' => 'required',
            'nombre_se' => 'required',
            'estado_emp' => 'required',
            'municipio_emp' => 'required',
            'parroquia_emp' => 'required',
            'direccion_emp' => 'required',
            'tlf1_emp' => 'required|min:18|max:18',
            'tlf2_emp' => 'required|min:18|max:18',
        ];
    }

    public function messages()
    {
        return [
            'tipo_emp.required' => 'El tipo de RIF es obligatorio.',
            'rif_emp.required' =>'El número de RIF es obligatorio.',
            'nombre_emp.required' => 'El nombre de la empresa es obligatorio.',
            'nombre_se.required' => 'El nombre de la sede o entidad es obligatorio.',
            'estado_emp.required' => 'El estado es obligatorio.',
            'municipio_emp.required' => 'El municipio es obligatorio.',
            'parroquia_emp.required' => 'La parroquia es obligatoria.',
            'direccion_emp.required' => 'La dirección es obligatoria.',
            'tlf1_emp.required' => 'El teléfono móvil es obligatorio.',
            'tlf1_emp.min' => 'El teléfono personal debe contener al menos 10 numeros.',
            'tlf2_emp.required' => 'El télefono de habitación es obligatorio.',
            'tlf2_emp.min' => 'El teléfono personal debe contener al menos 10 numeros.',
        ];
    }
}
