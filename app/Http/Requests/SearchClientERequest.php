<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SearchClientERequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tipo_emp' => 'required',
            'rif_emp' => 'required|min:8',
        ];
    }
    public function messages()
    {
        return [
            'tipo_emp.required' => 'El tipo de RIF es obligatorio.',
            'rif_emp.required' => 'El numero de RIF es obligatorio.',
            'rif_emp.min' => 'El numero de RIF debe ser mayor a 8 digitos.',
        ];
    }
}
