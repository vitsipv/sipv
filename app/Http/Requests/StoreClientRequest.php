<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nacionalidad' => 'required',
            'cedula' => 'required|unique:clients',
            'nombres' => 'required',
            'apellidos' => 'required',
            'tlf1' => 'required|min:18|max:18',
            'tlf2' => 'required|min:18|max:18',
            'email1' => 'required|email|unique:clients',
            'email2' => 'required|email|unique:clients',
            'estado' => 'required',
            'municipio' => 'required',
            'parroquia' => 'required',
            'direccion' => 'required',
        ];
    }

    public function messages()
    {
        return[
            'cedula.required' => 'El campo cédula es obligatorio.',
            'cedula.unique' => 'La cédula ya se encuentra registrada.',
            'nacionalidad.required' => 'Seleccione un tipo de cédula.',
            'nombres.required' => 'Ingrese sus nombres.',
            'apellidos.required' => 'Ingrese sus apellidos.',
            'tlf1.required' => 'Ingrese un numero de teléfono móvil.',
            'tlf1.min' => 'El teléfono personal debe contener al menos 10 numeros.',
            'tlf2.required' => 'Ingrese un numero de teléfono de habitación.',
            'tlf2.min' => 'El teléfono de habitación debe contener al menos 10 numeros.',
            'email1.required' => 'Ingrese un correo principal.',
            'email1.email' => 'El correo personal no es un correo válido',
            'email1.unique' => 'El correo personal ingresado ya esta en uso.',
            'email2.required' => 'Ingrese un correo secundario.',
            'email2.email' => 'El correo secundario no es un correo válido',
            'email2.unique' => 'El correo secundario ingresado ya esta en uso.',
            'estado.required' => 'Ingrese su estado.',
            'municipio.required' => 'Ingrese su municipio.',
            'parroquia.required' => 'Ingrese su parroquia.',
            'direccion.required' => 'Ingrese su dirección.',
        ];
    }
}
