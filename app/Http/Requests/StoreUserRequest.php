<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:150',
            'email' => 'required|string|max:150|unique:users',
            'user_type_id' => 'required|numeric',
            'nacionalidad' => 'required|string|max:1',
            'cedula' => 'required|string|max:20',
            'canal_soporte_id' => 'required|numeric',
            'estado' => 'required|numeric',
            'municipio' => 'required|numeric',
            'parroquia' => 'required|numeric',
            'direccion' => 'required|string|max:250',
            'password' => 'required'
        ];
    }
}
