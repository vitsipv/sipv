<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePieceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pp_serial' => 'nullable|max:20|unique:pp_inventories',
            'pp_cantidad' => 'integer|min:1',
            'pp_cond' => 'required|string',
            'origin_id' => 'required|integer',
            'ppcategory' => 'required',
            'pp_description_id' => 'required',
            'tipo_registro' => 'required',              
        ];
        
    }
    public function messages()
    {
        return[
            'ppcategory.required' => 'La categoría es obligatoria.',
            'pp_description_id.required' => 'La sub categoría es obligatoria.',
            'tipo_registro.required' => 'El tipo de registro es obligatorio.',
            'pp_serial.unique' => 'El serial ya se encuentra registrado.',
            'pp_cond.required' => 'La condición es obligatoria.',
            'origin_id.required' => 'La procedencia es obligatoria.',
            'pp_cantidad.integer' => 'La cantidad se encuentra vacia.',
            'pp_cantidad.min' => 'El cantidad debe ser de al menos 1.',
        ];
    }
}
