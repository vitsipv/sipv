<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sgpVehiculos extends Model
{
    public $timestamps = true;

    protected $connection = 'dbsgp';

    protected $table = 'vehiculos';

}
