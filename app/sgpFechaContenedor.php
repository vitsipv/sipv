<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sgpFechaContenedor extends Model
{
    public $timestamps = true;

    protected $connection = 'dbsgp';

    protected $table = 'fechas_contenedores';
}
