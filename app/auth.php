<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use OwenIt\Auditing\Contracts\Auditable;


class auth extends Authenticatable implements Auditable
{
    
    use HasApiTokens,Notifiable;
    use \OwenIt\Auditing\Auditable;



    protected $table="users";
    protected $connection="pgsql";
    protected $fillable = [
        'name', 'email', 'password', 'user_type_id', 'canal_soporte_id', 'nacionalidad', 'cedula', 'estado', 'municipio', 'parroquia',
        'direccion', 'correoPersonal', 'tlfPersonal', 'tlfHabitacion', 'nick'
    ];
    
}
