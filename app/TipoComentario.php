<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoComentario extends Model
{
    protected $fillable = [
    	'id','tipo'
    ];

  protected $table = 'tipo_comentarios';
}
