<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sgpConductores extends Model
{
    public $timestamps = true;

    protected $connection = 'dbsgp';

    protected $table = 'conductores';

}
