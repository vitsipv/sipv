<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class DetallesPresupuesto extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;
    protected $fillable = [
    	'id','piezas','numero_presupuesto','almacenes','np_piezas','cant_piezas','precio_piezas'
    ];
}
