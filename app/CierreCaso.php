<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CierreCaso extends Model
{
    protected $fillable = [
    	'id','numero_caso','pieza_reemplazada','fallas_id','serial_reemplazado','serial_nuevo','resumen','motivo'
    ];

  protected $table = 'cierre_casos';
}
