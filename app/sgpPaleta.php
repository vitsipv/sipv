<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sgpPaleta extends Model
{
    public $timestamps = true;

    const CREATED_AT = null; //Por alguna extraña razón, la tabla de paletas de sgp tiene una columna de fecha de actualizacion pero no de creacion

    protected $connection = 'dbsgp';

    protected $table = 'paletas';

    public function posicion(){
        return $this->belongsTo(sgpPosicion::class,'posiciones_id','id');
    }

    public function existencias_pp(){
        return $this->hasMany(sgpExistencia_pp::class,'paletas_id','id');
    }

    public function fecha_contenedor(){
        return $this->belongsTo(sgpFechaContenedor::class,'fechas_contenedores_id','id');
    }

    // protected $primaryKey = 'sn_equipo';
    // public $incrementing = false;

    protected $fillable = [
        'id','paleta', 'fechas_contenedores_id', 'lote', 'encargado_id', 'eliminado', 'observacion', 'posiciones_id', 'liberado_calidad', 'despiece'
    ];
}