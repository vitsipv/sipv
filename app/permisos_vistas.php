<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use SoftDeletes;
 
class permisos_vistas extends Model
{
    
    
    protected $dates = ['deleted_at'];
    protected $fillable = ['id','id_usuario', 'cedula_usuario',
    'clientes_ver', 'clientes_crear','clientes_eliminar','clientes_editar',
    'casos_ver',
    'apertura_casos_ver',
    'apertura_casos_crear','apertura_casos_eliminar','apertura_casos_editar',
    'gestionar_casos_ver','gestionar_casos_crear','gestionar_casos_eliminar','gestionar_casos_editar',
    'recepcion_ver','recepcion_crear','recepcion_eliminar','recepcion_editar',
    'tecnico_ver','tecnico_crear','tecnico_elimanar','tecnico_editar',
    'presupesto_ver','presupesto_crear','presupesto_eliminar','presupesto_editar',
    'llamadas_ver', 'llamadas_crear','llamadas_eliminar','llamadas_editar','perfil_ver',
    'perfil_crear','perfil_eliminar','perfil_editar',
    'inventario_ver',
    'partesy_piezas_ver','partesy_piezas_crear','partesy_piezas_eliminar','partesy_piezas_editar',
    'equipos_ver','equipos_crear','equipos_eliminar','equipos_editar',
    'movimientos_inventario_ver','movimientos_inventario_crear','movimientos_inventario_eliminar','movimientos_inventario_editar',
    'desarrollo_ver','desarrollo_crear','desarrollo_eliminar','desarrollo_editar'];
}
