<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\sgpAlmacen;

class sgpExistencia_pt extends Model
{
    public $timestamps = true;

    protected $connection = 'dbsgp';

    protected $table = 'existencias_pt';
    protected $fillable = [
        'paletas_id',
    ];

    public function posiciones(){
        return $this->hasOneThrough(sgpPosicion::class,sgpPaleta::class,'id','id','paletas_id','posiciones_id');
    }

    public function paleta(){
        return $this->belongsTo(sgpPaleta::class,'paletas_id','id');
    }

    public function version(){
        return $this->hasOneThrough(sgpModeloV::class,sgpOrden_produccion::class,'codigo_produccion','id','cod_produccion','versiones_id');
    }

    public function piezas(){
    	return $this->hasMany(sgpExistenciaProduccion::class,'existencias_pt_id');
    }
}
