<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Caso extends Model implements Auditable
{
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;

    protected $fillable = [
        'serial_equipo', 'modelo_equipo', 'canal_compra_id',
        'obser_caso', 'fecha_adqui', 'modo_atencion_id',
        'falla_equipo', 'piezas_sugeridas', 'client_id',
        'company_id','tecnico_id','estado_caso_id',
        'numero_caso','canal_soporte_id','tipo_equipo',
    ];

    public function cliente(){
        return $this->belongsTo(Client::class,'client_id','id');
    }

    public function empresa(){
        return $this->belongsTo(Company::class,'company_id','id');
    }

    public function canalCompra(){
        return $this->belongsTo(PurchaseChannel::class,'canal_compra_id','id');
    }

    public function modoAtencion(){
        return $this->belongsTo(AttentionMode::class,'modo_atencion_id','id');
    }

    public function caseStatus(){
        return $this->belongsTo(CaseStatus::class,'estado_caso_id','id');
    }

    public function diagnosticoCaso(){
        return $this->hasMany(Diagnostico::class,'numero_caso','numero_caso');
    }

    public function detallesDiagnostico(){
        return $this->hasManyThrough('App\DetallesDiagnostico','App\Diagnostico','numero_caso','diagnosticos_id','numero_caso','id');
    }

    public function comentariosCaso(){
        return $this->hasMany(Comentario::class,'numero_caso_id','numero_caso');
    }

    public function presupuestoCaso(){
        return $this->hasMany(Presupuesto::class,'numero_caso','numero_caso');
    }

    public function tecnico(){
        return $this->belongsTo(User::class, 'tecnico_id', 'id');
    }

}
