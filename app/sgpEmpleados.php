<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\sgpAlmacen;

class sgpEmpleados extends Model
{
    public $timestamps = true;

    protected $connection = 'dbsgp';

    protected $table = 'empleados';

}
