<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Presupuesto extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $fillable = [
        'numero_presupuesto', 'pfd_presupuesto', 'presupuesto','numero_caso', 'status_id', 'iva_id', 'condiciones'
    ];

    public function condicionesPresupuesto(){
        return $this->belongsTo(PresupuestoStatus::class,'status_id','id');
    }

    public function iva(){
        return $this->belongsTo(Iva::class,'iva_id','id');
    }

    public function detallesPresupuesto(){
        return $this->hasMany(DetallesPresupuesto::class,'numero_presupuesto','numero_presupuesto');
    }

    public function caso(){
        return $this->belongsTo(Caso::class,'numero_caso','numero_caso');
    }

    public function cliente()
    {
        return $this->hasOneThrough('App\Client','App\Caso','numero_caso','id','numero_caso','client_id'); // (modelo final a consultar, modelo intermedio, llave local en modelo intermedio, llave local modelo final, llave foranea en modelo presupuesto, llave foranea en modelo Caso)
    }

    public function empresa()
    {
        return $this->hasOneThrough('App\Company','App\Caso','numero_caso','id','numero_caso','company_id'); // (modelo final a consultar, modelo intermedio, llave local en modelo intermedio, llave local modelo final, llave foranea en modelo presupuesto, llave foranea en modelo Caso)
    }
    

}
