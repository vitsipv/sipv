<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sgpOrdenP extends Model
{
    public $timestamps = true;

    protected $connection = 'dbsgp';

    protected $table = 'orden_produccion';

    // protected $primaryKey = 'sn_equipo';
    // public $incrementing = false;

    protected $fillable = [
        'codigo_produccion',
    ];
}
