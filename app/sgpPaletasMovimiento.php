<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sgpPaletasMovimiento extends Model
{
    public $timestamps = true;

    protected $connection = 'dbsgp';

    protected $table = 'paletas_movimiento';

    protected $fillable = [
        'paletas_id',
        'galpon_origen',
        'movimiento_inventario_id',
        'cantidad',
        'paleta_origen',
    ];

    public function paleta(){
        return $this->belongsTo(sgpPaleta::class,'paletas_id','id');
    }

    public function movimiento_inventario(){
        return $this->belongsTo(sgpMovimientoInventario::class,'movimiento_inventario_id','id');
    }

}
