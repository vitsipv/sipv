<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaseStatus extends Model
{
    protected $fillable = ['id', 'case_status'];
    protected $table = 'case_status';
}
