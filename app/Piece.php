<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Piece extends Model
{
    /**
     * The database connection used by the model.
     *
     * @var string
     */

    public $timestamps = false; // Para omitir el campo UPDATED_AT.

    protected $connection = 'svit';

    protected $table = 'svit_ppto';

    protected $primaryKey = 'nuid_ppto';

    protected $fillable = [
        'nuid_ppto', 'codi_ppto', 'desc_ppto', 'prec_ppto', 'stat_ppto'
    ];

}
