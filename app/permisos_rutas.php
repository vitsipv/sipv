<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
 
    use SoftDeletes;
class permisos_rutas extends Model
{
    protected $fillable = ['id', 'permisos', 'cedula','rutas'];
   
 
    protected $dates = ['deleted_at'];
}
