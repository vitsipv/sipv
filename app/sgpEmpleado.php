<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sgpEmpleado extends Model
{
    public $timestamps = true;

    protected $connection = 'dbsgp';

    protected $table = 'empleados';

    // protected $primaryKey = 'sn_equipo';
    // public $incrementing = false;

    /*protected $fillable = [
        'nombre',
    ];*/
}
