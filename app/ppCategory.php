<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ppCategory extends Model
{
		protected $fillable = ['id', 'name'];
		
    public function ppdescription(){
    	return $this->hasMany(ppDescription::class);
    }
}
