<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Comentario extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $fillable = [
    	'id','usuario','tipo_comentario_id','comentario','estado','numero_caso_id'
    ];

  protected $table = 'comentarios';

  public function tipo(){
        return $this->belongsTo(TipoComentario::class,'tipo_comentario_id','id');
    }

     public function caso(){
        return $this->belongsTo(Caso::class,'numero_caso_id','numero_caso');
    }
}
