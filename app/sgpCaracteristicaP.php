<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sgpCaracteristicaP extends Model
{
    public $timestamps = true;
    protected $connection = 'dbsgp';
    protected $table = 'caracteristicas_piezas';

    // protected $primaryKey = 'sn_equipo';
    // public $incrementing = false;
    public function numeroParte(){
       return $this->belongsTo(sgpNumeroParte::class,'caracteristicas_piezas_id','id');
    }

    protected $fillable = [
        'descripcion',
    ];
}
