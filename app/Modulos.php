<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Modulos extends Model
{
    use HasFactory;

    protected $connection = 'pgsql';
    protected $fillable = ['nombre_modulo','ruta_modulo'];
}
