<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ppInventory extends Model
{
    protected $fillable = ['id', 'pp_description_id', 'pp_serial', 'pp_cantidad', 'pp_cond', 'origin_id', 'pp_status']; // pp_cond = condicion y pp_proc = procedencia
    
    public function ppdescription(){
        return $this->belongsTo(ppDescription::class,'pp_description_id','id');
    }
}