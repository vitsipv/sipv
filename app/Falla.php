<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Falla extends Model
{
    protected $fillable = [
    	'id','nombre',
    ];

  protected $table = 'fallas';
}
