<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PresupuestoStatus extends Model
{
    protected $fillabel = ['id', 'presupuesto_status'];
    protected $table = 'presupuesto_status';
}
