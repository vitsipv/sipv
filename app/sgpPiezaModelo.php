<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sgpPiezaModelo extends Model
{
    public $timestamps = true;

    protected $connection = 'dbsgp';

    protected $table = 'lista_bom';

    // protected $primaryKey = 'sn_equipo';
    // public $incrementing = false;

    protected $fillable = [
        'versiones_id', 'numeros_partes_id',
    ];
}
