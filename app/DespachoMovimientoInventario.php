<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\sgpExistencia_pp;
use OwenIt\Auditing\Contracts\Auditable;

class DespachoMovimientoInventario extends Model
implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
	protected $fillable = [
    	'movimiento_inventario_id','existencias_pp_origen_id','existencias_pp_destino_id','cantidad','array_etiquetadas_id', 'motivo'
    ];

    protected $casts = [
        'array_etiquetadas_id' => 'array'
    ];

    public function existencia_pp_origen(){
        return $this->belongsTo(sgpExistencia_pp::class,'existencias_pp_origen_id','id');
    }
    public function existencia_pp_destino(){
    	return $this->belongsTo(sgpExistencia_pp::class,'existencias_pp_destino_id','id');
    }
    public function movimiento_inventario(){
    	return $this->belongsTo(sgpMovimientoInventario::class,'movimiento_inventario_id','id');
    }
}
