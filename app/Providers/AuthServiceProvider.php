<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Policies\PermisosPolicies;
class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        //'App\Model' => 'App\Policies\ModelPolicy',
         'App\Permisos' => PermisosPolicies::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot() // PUERTAS DE ACCESO
    {
        $this->registerPolicies();

        Gate::define('esAdmin', function($user){
        
            return intval($user->user_type_id) === 1;
        });

        Gate::define('esGerente', function($user){
            return $user->user_type_id === 2;
        });

        Gate::define('esCoordinador', function($user){
            return $user->user_type_id === 3;
        });

        Passport::routes();
    }
}
