<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\sgpAlmacen;
use App\sgpEmpleados;

class sgpMovimientoInventario extends Model
{
    public $timestamps = true;

    protected $connection = 'dbsgp';

    protected $table = 'movimiento_inventario';

    protected $appends = ['codigo'];

    protected $fillable = [
        'id',
        'correlativo',
        'administrador_almacen_origen',
        'galpon_destino',
        'conductores_id',
        'fecha_movimiento',
        'seguridad_origen',
        'vehiculos_id',
    ];


    public function galpon_destino(){
        return $this->belongsTo(sgpAlmacen::class,'galpon_destino','id');
    }

    public function info_administrador_almacen_origen(){
        return $this->belongsTo(sgpEmpleados::class,'administrador_almacen_origen','id');
    }
    public function administrador_almacen_destino(){
        return $this->belongsTo(sgpEmpleados::class,'administrador_almacen_destino','id');
    }
    public function seguridad_origen(){
        return $this->belongsTo(sgpEmpleados::class,'seguridad_origen','id');
    }
    public function seguridad_destino(){
        return $this->belongsTo(sgpEmpleados::class,'seguridad_destino','id');
    }

    /*public static function codigo_movimiento(){
        return self::get();
    }*/

    public function getCodigoAttribute()
    {
        //return $this->correlativo ? "MIV".date("dmy-").$this->correlativo : 'N/E';
        return $this->correlativo ? "MIV".$this->created_at->format("dmy-").$this->correlativo : 'N/E';
    }

}
