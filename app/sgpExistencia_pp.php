<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\sgpAlmacen;

class sgpExistencia_pp extends Model
{

    public $timestamps = false;

    protected $connection = 'dbsgp';

    protected $table = 'existencias_pp';

    public function paleta(){
        return $this->belongsTo(sgpPaleta::class,'paletas_id','id');
    }

    public function pieza(){
        return $this->belongsTo(sgpPieza::class,'piezas_id','id');
    }

    public function numeroParte(){
        return $this->belongsTo(sgpNumeroParte::class,'pn_id','id')->groupBy('id');
    }

    public function exisEtiquetadas(){
        return $this->hasMany(sgpExistenciaProduccion::class,'existencias_pp_id','id')->where('existencias_pt_id',null)->orderBy('qn', 'ASC');
    }

    public function exisEtiquetadasCount(){
        $count = $this->exisEtiquetadas;
        return count($count);
    }

    public function precios()
    {
        return $this->hasOneThrough('App\sgpPrecios','App\sgpNumeroParte','id','id','pn_id'); // (modelo final a consultar, modelo intermedio, llave local en modelo intermedio, llave local modelo final, llave foranea en modelo presupuesto, llave foranea en modelo Caso)
    } 

   /* public function precios(){
        return $this->hasManyThrough(sgpPrecios::class,sgpNumeroParte::class,'id','numeros_partes_id');
    } */

   /* public function precios(){
        return $this->hasManyThrough('App\sgpPrecios','App\sgpNumeroParte','id','numeros_partes_id');
    } */

    public function posiciones(){
        return $this->hasOneThrough(sgpPosicion::class,sgpPaleta::class,'id','id','paletas_id','posiciones_id');
    }

    // protected $primaryKey = 'sn_equipo';
    // public $incrementing = false;

    protected $fillable = [
        'id','paletas_id','piezas_id', 'cantidad', 'pn_id', 'inspecciones_cce_id', 'np_proveedores_id', 'detalles_facturas_invoice_id'
    ];
}
