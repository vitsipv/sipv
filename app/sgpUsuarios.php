<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sgpUsuarios extends Model
{
    public $timestamps = true;

    protected $connection = 'dbsgp';

    protected $table = 'usuarios';

    public function usuario_almacen()
    {
        return $this->belongsTo(sgpUsuariosAlmacenes::class, 'usuarios_id');
    }

    public function empleado()
    {
        return $this->belongsTo(sgpEmpleado::class, 'empleados_id');
    }

    // protected $primaryKey = 'sn_equipo';
    // public $incrementing = false;

    /*protected $fillable = [
        'nombre',
    ];*/
}
