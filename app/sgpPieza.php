<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sgpPieza extends Model
{
    public $timestamps = true;

    protected $connection = 'dbsgp';

    protected $table = 'piezas';

    public function existenciasPP(){
        return $this->hasMany(sgpExistencia_pp::class,'piezas_id','id');
    }

    public function existenciaProduccion(){
        return $this->hasOneThrough(sgpExistenciaProduccion::class,sgpExistencia_pp::class,'id','existencias_pp_id');
    }
    // protected $primaryKey = 'sn_equipo';
    // public $incrementing = false;

    protected $fillable = [
        'nombre',
    ];
}
