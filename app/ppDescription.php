<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ppDescription extends Model
{
		protected $fillable = ['id', 'pp_category_id', 'pp_tipo', 'pp_description', 'pp_np'];
		
    public function ppcategory(){
    	return $this->belongsTo(ppCategory::class);
    }
    public function ppinventory(){
    	return $this->hasMany(ppInventory::class);
    }
}
