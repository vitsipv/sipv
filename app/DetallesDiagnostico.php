<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class DetallesDiagnostico extends Model
implements Auditable
{
	use \OwenIt\Auditing\Auditable;

	protected $fillable = [
    	'id','diagnosticos_id','piezas','almacenes','np_piezas','cant_piezas','precio_piezas'
    ];

  protected $table = 'detalles_diagnosticos';
  

	public function numerospartes(){
        return $this->hasMany(sgpNumeroParte::class,'numeros_partes_id','id');
    }
    
}
