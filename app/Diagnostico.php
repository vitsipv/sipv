<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Diagnostico extends Model implements Auditable
{
  use \OwenIt\Auditing\Auditable;
	protected $fillable = [
    	'id','diagnostico_caso','numero_caso','tecnico_id',
    ];

  protected $table = 'diagnosticos';


  public function detallesDiagnostico(){
       return $this->hasMany(detallesDiagnostico::class);
  }

	public function numerospartes(){
        return $this->hasMany(sgpNumeroParte::class,'numeros_partes_id','id');
    }
    
}
