<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasApiTokens, Notifiable,softDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table="users";
    protected $connection="pgsql";
    protected $fillable = [

        'name', 'email', 'password', 'user_type_id', 'canal_soporte_id', 'nacionalidad', 'cedula', 'estado', 'municipio', 'parroquia',
        'direccion', 'correoPersonal', 'tlfPersonal', 'tlfHabitacion', 'nick','estado_user'

    ];
    // protected $fillable = [
    //     'loguse', 'nomuse','apluse','pasuse','cedemp','diremp','telemp','id','feccad','numemp','stablo','created_at','updated_at','deleted_at','emaemp','codigoId','codsuc_reg','codroles'
    // ];
    // protected $fillable = [
    //     'logi_usua','clav_usua','codi_usua','remember_token'
    // ];

    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function usertype(){
        return $this->belongsTo(UserType::class, 'user_type_id', 'id');
    }

    public function usersupportchannel(){
        return $this->belongsToMany(SupportChannel::class, 'canales_usuarios', 'user_id', 'canal_soporte_id');
    }

    public function registrollamada(){
        return $this->hasMany(Registrollamada::class, 'user_id', 'id');
    }
}
