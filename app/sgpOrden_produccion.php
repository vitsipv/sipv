<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sgpOrden_produccion extends Model
{
    public $timestamps = true;

    protected $connection = 'dbsgp';

    protected $table = 'orden_produccion';
    protected $fillable = [
        'versiones_id','codigo_produccion',
    ];

    public function version(){
        return $this->belongsTo(sgpModeloV::class,'versiones_id','id');
    }
}
