<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sgpSerialPT extends Model
{
    /**
     * The database connection used by the model.
     *
     * @var string
     */

    public $timestamps = true;

    protected $connection = 'dbsgp';

    protected $table = 'existencias_pt';

    protected $primaryKey = 'sn_equipo';
    public $incrementing = false;

    protected $fillable = [
        'sn_equipo',
    ];
}
