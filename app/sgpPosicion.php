<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sgpPosicion extends Model
{
    public $timestamps = true;

    protected $connection = 'dbsgp';

    protected $table = 'posiciones';

    // protected $primaryKey = 'sn_equipo';
    // public $incrementing = false;

    protected $fillable = [
        'almacen_id',
    ];

    public function almacen()
    {
    	return $this->belongsTo(sgpAlmacen::class,'almacenes_id');
    }

    public function almacenpt()
    {
        return $this->belongsTo(sgpAlmacen::class,'almacenes_id','id')->where('clasificacion','!=','GP');
    }
}
