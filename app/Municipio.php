<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    protected $fillable = ['id', 'id_estado', 'municipio'];
    public $timestamps = false;
    public function estado()
    {
        return $this->belongsTo(Estado::class);
    }
	public function parroquias()
    {
        return $this->hasMany(Parroquia::class);
    }
}
