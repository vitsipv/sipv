<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class TransferirUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:transf';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Este comando se usa para transferir los usuarios que estan en la tabla que estan mysql a postgres';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
         $cp = \App\auth::all();
         foreach ($cp as $value) {
            //dd($value->nomb_usua);
            
            \App\User::updateOrCreate([
                'cedula' => $value->cedu_usua
            ],
            [
                'name' => $value->nomb_usua,
                'nick' => $value->logi_usua,
                'password' => $value->clav_usua,
                'correoPersonal' => $value->mail_usua,
                #'user_type_id' => intval($value->codi_nive),
                
            ]);
        }

        return 0;
    }
}
