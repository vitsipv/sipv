import cv2
import os
import imutils

nombre_usuario = 'Fernand'
ruta_datos = '/home/fernand/Documentos/python_entornos/biometrico_facial/rostros' #Cambia a la ruta donde hayas almacenado Data
ruta_usuario = ruta_datos + '/' + nombre_usuario

#Crea la carpeta con los rostros en caso de no existir
if not os.path.exists(ruta_usuario):
    print('Carpeta creada: ',ruta_usuario)
    os.makedirs(ruta_usuario)

cap = cv2.VideoCapture(0,cv2.CAP_DSHOW) # <---- Hace la captura de rostros en un streaming
#cap = cv2.VideoCapture('Video.mp4') <--- Hace la captura de rostros desde un video

faceClassif = cv2.CascadeClassifier(cv2.data.haarcascades+'haarcascade_frontalface_default.xml')
count = 0

while True:
    
    ret, frame = cap.read()
    if ret == False: break
    frame =  imutils.resize(frame, width=640)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    auxFrame = frame.copy()

    faces = faceClassif.detectMultiScale(gray,1.3,5)

    for (x,y,w,h) in faces:
        cv2.rectangle(frame, (x,y),(x+w,y+h),(0,255,0),2)
        rostro = auxFrame[y:y+h,x:x+w]
        rostro = cv2.resize(rostro,(150,150),interpolation=cv2.INTER_CUBIC)
        cv2.imwrite(ruta_usuario + '/rotro_{}.jpg'.format(count),rostro)
        count = count + 1
    cv2.imshow('frame',frame)

    k =  cv2.waitKey(1)
    if k == 27 or count >= 300:
        break

cap.release()
cv2.destroyAllWindows()