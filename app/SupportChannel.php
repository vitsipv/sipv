<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupportChannel extends Model
{
    protected $fillable = [
        'id', 'nombre',
    ];

    public function users(){
        return $this->belongsToMany(User::class, 'canales_usuarios', 'canal_soporte_id', 'user_id');
    }
}
