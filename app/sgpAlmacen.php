<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sgpAlmacen extends Model
{
    public $timestamps = true;

    protected $connection = 'dbsgp';

    protected $table = 'almacenes';

    public function usuarios_almacenes()
    {
        return $this->hasMany(sgpUsuariosAlmacenes::class, 'almacenes_id', 'id');
    }

    // protected $primaryKey = 'sn_equipo';
    // public $incrementing = false;

    protected $fillable = [
        'nombre',
    ];
}
