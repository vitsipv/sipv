<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permisos extends Model
{
    use HasFactory,softDeletes;

    protected $connection="pgsql";
    protected $table = "permisos";
    protected $fillable = ['codpermission','usuario','modulo'];
   
    use SoftDeletes;
 
    protected $dates = ['deleted_at'];
}
