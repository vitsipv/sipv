<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('password');
            
            $table->string('nacionalidad')->nullable();
            $table->string('cedula')->unique();
            $table->string('estado')->nullable();
            $table->string('municipio')->nullable();
            $table->string('parroquia')->nullable();
            $table->string('direccion')->nullable();

            $table->integer('user_type_id')->unsigned()->nullable();

// <<<<<<< HEAD
//             /*$table->string('canal');*/
//             $table->integer('canal_soporte_id')->unsigned()->nullable();
//             $table->foreign('canal_soporte_id')->references('id')->on('support_channels');

// =======
            $table->string('correoPersonal')->nullable();
            $table->string('tlfPersonal')->nullable();
            $table->string('tlfHabitacion')->nullable();
            $table->string('photo')->default('profile.png');
            $table->string('nick')->default('Usuario');
            $table->rememberToken();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
