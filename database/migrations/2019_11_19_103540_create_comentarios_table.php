<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComentariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comentarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('usuario');

            $table->integer('tipo_comentario_id')->unsigned();
            $table->foreign('tipo_comentario_id')->references('id')->on('tipo_comentarios');

            $table->string('numero_caso_id')->unsigned()->after('numero_caso');;
            $table->foreign('numero_caso_id')->references('numero_caso')->on('casos');

            $table->string('comentario');
            $table->boolean('estado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comentarios');
    }
}
