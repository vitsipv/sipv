<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipo_emp');
            $table->string('rif_emp')->unique();
            $table->string('nombre_emp');
            $table->string('nombre_se');
            $table->string('estado_emp');
            $table->string('municipio_emp');
            $table->string('parroquia_emp');
            $table->string('direccion_emp');
            $table->string('tlf1_emp');
            $table->string('tlf2_emp');
            $table->string('email_emp')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
