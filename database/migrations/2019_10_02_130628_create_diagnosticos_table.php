<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiagnosticosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diagnosticos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('diagnostico_caso');

            $table->string('numero_caso')->unsigned()->after('numero_caso');
            $table->foreign('numero_caso')->references('numero_caso')->on('casos');

            $table->integer('tecnico_id')->unsigned();
            $table->foreign('tecnico_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diagnosticos');
    }
}
