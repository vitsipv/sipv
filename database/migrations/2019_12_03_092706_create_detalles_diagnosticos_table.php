<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetallesDiagnosticosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalles_diagnosticos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('diagnosticos_id')->unsigned();
            $table->foreign('diagnosticos_id')->references('id')->on('diagnosticos');
            $table->string('piezas');
            $table->string('almacenes');
            $table->string('np_piezas');
            $table->integer('cant_piezas');
            $table->string('precio_piezas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalles_diagnosticos');
    }
}
