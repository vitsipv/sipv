<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDespachoMovimientoInventariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('despacho_movimiento_inventarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            //Movimiento Relacionado
            $table->bigInteger('movimiento_inventario_id')->unsigned();
            /*$table->foreign('movimiento_inventario_id')->references('id')->on('movimiento_inventario');
            $table->index('movimiento_inventario_id');*/
            //Existencia de Origen
            $table->bigInteger('existencias_pp_origen_id')->unsigned();
            /*$table->foreign('existencias_pp_origen_id')->references('id')->on('existencias_pp');
            $table->index('existencias_pp_origen_id');*/
            //Existencia de Destino
            $table->bigInteger('existencias_pp_destino_id')->unsigned();
            /*$table->foreign('existencias_pp_destino_id')->references('id')->on('existencias_pp');
            $table->index('existencias_pp_destino_id');*/
            //Cantidad
            $table->bigInteger('cantidad')->unsigned()->nullable();
            //Etiquetadas
            $table->string('motivo')->nullable();
            $table->json('array_etiquetadas_id')->nullable();
            $table->boolean('despachado')->default(0); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('despacho_movimiento_inventarios');
    }
}
