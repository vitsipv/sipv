<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePresupuestosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presupuestos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('numero_presupuesto')->unique();
            $table->string('pdf_presupuesto')->nullable();
            $table->string('presupuesto');
            $table->string('condiciones')->nullable();

            $table->string('numero_caso')->unsigned()->after('numero_caso');
            $table->foreign('numero_caso')->references('numero_caso')->on('casos');

            $table->integer('status_id')->unsigned();
            $table->foreign('status_id')->references('id')->on('presupuesto_status');

            $table->integer('iva_id')->unsigned();
            $table->foreign('iva_id')->references('id')->on('iva'); 

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presupuestos');
    }
}
