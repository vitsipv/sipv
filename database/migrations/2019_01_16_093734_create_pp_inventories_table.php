<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePpInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pp_inventories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pp_serial')->nullable()->unique();
            $table->integer('pp_cantidad')->default(1);
            $table->string('pp_cond');
            $table->boolean('pp_status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pp_inventories');
    }
}
