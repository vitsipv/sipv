<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePpDescriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pp_descriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pp_category_id')->unsigned();
            $table->foreign('pp_category_id')->references('id')->on('pp_categories');
            $table->string('pp_tipo')->references('name')->on('pp_categories');
            $table->string('pp_description');
            $table->string('pp_np')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pp_Descriptions');
    }
}
