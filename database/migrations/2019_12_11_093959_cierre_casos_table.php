<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CierreCasosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cierre_casos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('numero_caso')->unsigned();
            $table->foreign('numero_caso')->references('numero_caso')->on('casos');
            $table->string('pieza_reemplazada');
            $table->integer('fallas_id')->unsigned();
            $table->foreign('fallas_id')->references('id')->on('fallas');
            $table->string('serial_reemplazado')->nullable();
            $table->string('serial_nuevo')->nullable();
            $table->string('resumen')->nullable();
            $table->string('motivo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cierre_casos');
    }
}
