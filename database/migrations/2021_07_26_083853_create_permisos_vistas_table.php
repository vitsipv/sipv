<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermisosVistasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permisos_vistas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_usuario');
            $table->integer('cedula_usuario');
            $table->boolean('clientes_ver')->default(0);
            $table->boolean('clientes_crear')->default(0);
            $table->boolean('clientes_eliminar')->default(0);
            $table->boolean('clientes_editar')->default(0);
            $table->boolean('casos_ver')->default(0);
            $table->boolean('apertura_casos_ver')->default(0);
            $table->boolean('apertura_casos_crear')->default(0);
            $table->boolean('apertura_casos_eliminar')->default(0);
            $table->boolean('apertura_casos_editar')->default(0);
            $table->boolean('gestionar_casos_ver')->default(0);
            $table->boolean('gestionar_casos_crear')->default(0);
            $table->boolean('gestionar_casos_eliminar')->default(0);
            $table->boolean('gestionar_casos_editar')->default(0);
            $table->boolean('recepcion_ver')->default(0);
            $table->boolean('recepcion_crear')->default(0);
            $table->boolean('recepcion_eliminar')->default(0);
            $table->boolean('recepcion_editar')->default(0);
            $table->boolean('tecnico_ver')->default(0);
            $table->boolean('tecnico_crear')->default(0);
            $table->boolean('tecnico_elimanar')->default(0);
            $table->boolean('tecnico_editar')->default(0);
            $table->boolean('presupesto_ver')->default(0);
            $table->boolean('presupesto_crear')->default(0);
            $table->boolean('presupesto_eliminar')->default(0);
            $table->boolean('presupesto_editar')->default(0);
            $table->boolean('llamadas_ver')->default(0);
            $table->boolean('llamadas_crear')->default(0);
            $table->boolean('llamadas_eliminar')->default(0);
            $table->boolean('llamadas_editar')->default(0);
            $table->boolean('perfil_ver')->default(0);
            $table->boolean('perfil_crear')->default(0);
            $table->boolean('perfil_eliminar')->default(0);
            $table->boolean('perfil_editar')->default(0);
            $table->boolean('inventario_ver')->default(0);
            $table->boolean('partesy_piezas_ver')->default(0);
            $table->boolean('partesy_piezas_crear')->default(0);
            $table->boolean('partesy_piezas_eliminar')->default(0);
            $table->boolean('partesy_piezas_editar')->default(0);
            $table->boolean('equipos_ver')->default(0);
            $table->boolean('equipos_crear')->default(0);
            $table->boolean('equipos_eliminar')->default(0);
            $table->boolean('equipos_editar')->default(0);
            $table->boolean('movimientos_inventario_ver')->default(0);
            $table->boolean('movimientos_inventario_crear')->default(0);
            $table->boolean('movimientos_inventario_eliminar')->default(0);
            $table->boolean('movimientos_inventario_editar')->default(0);
            $table->boolean('desarrollo_ver')->default(0);
            $table->boolean('desarrollo_crear')->default(0);
            $table->boolean('desarrollo_eliminar')->default(0);
            $table->boolean('desarrollo_editar')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permisos_vistas');
    }
}
