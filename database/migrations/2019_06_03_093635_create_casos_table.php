<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('casos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('serial_equipo')->nullable();
            $table->string('modelo_equipo');
            $table->string('tipo_equipo');

            $table->integer('canal_compra_id')->unsigned();
            $table->foreign('canal_compra_id')->references('id')->on('purchase_channels');

            $table->string('obser_caso');
            $table->string('fecha_adqui');

            $table->integer('modo_atencion_id')->unsigned();
            $table->foreign('modo_atencion_id')->references('id')->on('attention_modes');

            $table->string('falla_equipo');
            $table->string('piezas_sugeridas');
            $table->integer('estado_caso_id')->unsigned();
            $table->foreign('estado_caso_id')->references('id')->on('case_status');

            $table->integer('canal_soporte_id')->unsigned();
            $table->foreign('canal_soporte_id')->references('id')->on('support_channels');

            $table->integer('client_id')->unsigned()->nullable();
            $table->foreign('client_id')->references('id')->on('clients');

            $table->integer('tecnico_id')->unsigned()->nullable();
            $table->foreign('tecnico_id')->references('id')->on('users');

            $table->integer('company_id')->unsigned()->nullable();
            $table->foreign('company_id')->references('id')->on('companies');

            $table->date('fecha_recepcion')->nullable();


            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('casos');
    }
}
