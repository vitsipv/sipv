<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetallesPresupuestosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalles_presupuestos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('piezas');
            $table->string('almacenes');
            $table->string('np_piezas');
            $table->integer('cant_piezas');
            $table->double('precio_piezas');

            $table->string('numero_presupuesto')->unsigned()->after('numero_presupuesto');
            $table->foreign('numero_presupuesto')->references('numero_presupuesto')->on('presupuestos');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalles_presupuestos');
    }
}
