<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pp_inventories', function (Blueprint $table) {

            $table->integer('pp_description_id')->unsigned();
            $table->foreign('pp_description_id')->references('id')->on('pp_descriptions')->onDelete('set null');
            $table->integer('origin_id')->unsigned();
            $table->foreign('origin_id')->references('id')->on('origins')->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pp_inventories',function(Blueprint $table){
            // Place foreign key name
            $table->dropForeign('pp_description_id');
            $table->dropForeign('origin_id');
            $table->dropForeign('equipos_pp_id');
        });
    }
}
