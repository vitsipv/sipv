<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
class ModulosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Modulos::insert([
            'nombre_modulo' => 'Clientes',
            'ruta_modulo' => '/clientes'
        ]);

        // \App\Modulos::create([
        //     'nombre_modulo' => 'Aperturar Caso',
        //     'ruta_modulo' => '/casos'
        // ]);

        // \App\Modulos::create([
        //     'nombre_modulo' => 'Gestionar Caso',
        //     'ruta_modulo' => '/gestionar Caso'
        // ]);

        // \App\Modulos::create([
        //     'nombre_modulo' => 'Recepcion',
        //     'ruta_modulo' => '/recepcion'
        // ]);

        // \App\Modulos::create([
        //     'nombre_modulo' => 'Tecnico',
        //     'ruta_modulo' => '/tecnico'
        // ]);

        // \App\Modulos::create([
        //     'nombre_modulo' => 'Presupuesto',
        //     'ruta_modulo' => '/presupuesto'
        // ]);

        // \App\Modulos::create([
        //     'nombre_modulo' => 'LLamadas',
        //     'ruta_modulo' => '/llamadas'
        // ]);

        // \App\Modulos::create([
        //     'nombre_modulo' => 'Perfil',
        //     'ruta_modulo' => '/perfil'
        // ]);

        // \App\Modulos::create([
        //     'nombre_modulo' => 'Partes y Piezas',
        //     'ruta_modulo' => '/inventario_partes'
        // ]);

        // \App\Modulos::create([
        //     'nombre_modulo' => 'Equipos',
        //     'ruta_modulo' => '/inventario_equipos'
        // ]);

        // \App\Modulos::create([
        //     'nombre_modulo' => 'Movimientos',
        //     'ruta_modulo' => '/inventario_movimientos'
        // ]);
    }
}
