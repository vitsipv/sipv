<?php

use Illuminate\Database\Seeder;
use App\PurchaseChannel;

class PurchaseChannelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PurchaseChannel::create(array( 'id' => 1, 'nombre' => 'CANTV ABA Equipado' ));
        PurchaseChannel::create(array( 'id' => 2, 'nombre' => 'CANTV a Empresas' ));
        PurchaseChannel::create(array( 'id' => 3, 'nombre' => 'Compra Directa VIT' ));
        PurchaseChannel::create(array( 'id' => 4, 'nombre' => 'Abasto Bicentenario' ));
        PurchaseChannel::create(array( 'id' => 5, 'nombre' => 'Telecom' ));
        PurchaseChannel::create(array( 'id' => 6, 'nombre' => 'Donación' ));
        PurchaseChannel::create(array( 'id' => 7, 'nombre' => 'Devolución' ));
        PurchaseChannel::create(array( 'id' => 8, 'nombre' => 'Otro(a)' ));
        PurchaseChannel::create(array( 'id' => 9, 'nombre' => 'Vproductiva' ));
    }
}
