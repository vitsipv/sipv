<?php

use Illuminate\Database\Seeder;
use App\TipoComentario;

class TipoComentarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	TipoComentario::create(array( 'id' => 1, 'tipo' => 'Asignar Tecnico' ));
    	TipoComentario::create(array( 'id' => 2, 'tipo' => 'Confirmación de Dirección' ));
    	TipoComentario::create(array( 'id' => 3, 'tipo' => 'Consulta de Estado' ));
    	TipoComentario::create(array( 'id' => 4, 'tipo' => 'Contacto por No enviado' ));
    	TipoComentario::create(array( 'id' => 5, 'tipo' => 'Diagnostico de Equipo' ));
    	TipoComentario::create(array( 'id' => 6, 'tipo' => 'Entrega personal VIT' ));
    	TipoComentario::create(array( 'id' => 7, 'tipo' => 'Equipo Pendiente por Pago de Presupuesto' ));
    	TipoComentario::create(array( 'id' => 8, 'tipo' => 'Error en Diagnostico' ));
    	TipoComentario::create(array( 'id' => 9, 'tipo' => 'Estatus a Presupuesto' ));
    	TipoComentario::create(array( 'id' => 10, 'tipo' => 'Estatus a Sin Garantia' ));
    	TipoComentario::create(array( 'id' => 11, 'tipo' => 'Generar Presupuesto' ));
    	TipoComentario::create(array( 'id' => 12, 'tipo' => 'Llegada de Tecnico a Sitio' ));
    	TipoComentario::create(array( 'id' => 13, 'tipo' => 'Orden de Reparación' ));
    	TipoComentario::create(array( 'id' => 14, 'tipo' => 'Otro' ));
    	TipoComentario::create(array( 'id' => 15, 'tipo' => 'Reclamo' ));
    	TipoComentario::create(array( 'id' => 16, 'tipo' => 'Reclamo a Canal' ));
    	TipoComentario::create(array( 'id' => 17, 'tipo' => 'Reenviar Presupuesto' ));
    	TipoComentario::create(array( 'id' => 18, 'tipo' => 'Salida de Tecnico de Sitio' ));
    	TipoComentario::create(array( 'id' => 19, 'tipo' => 'Se Contactó Cliente' ));
    	TipoComentario::create(array( 'id' => 20, 'tipo' => 'Solicitud de Guia Prepagada' ));
    	TipoComentario::create(array( 'id' => 21, 'tipo' => 'Suspendido por Cliente' ));
    	TipoComentario::create(array( 'id' => 22, 'tipo' => 'Suspendido por Piezas' ));
    	TipoComentario::create(array( 'id' => 23, 'tipo' => 'Suspendido por Presupuesto' ));
        TipoComentario::create(array( 'id' => 24, 'tipo' => 'Venta Directa' ));

    }
}
