<?php

use Illuminate\Database\Seeder;
use App\SupportChannel;

class SupportChannelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SupportChannel::create(array( 'id' => 1, 'nombre' => 'VIT - FALCON' ));
        SupportChannel::create(array( 'id' => 2, 'nombre' => 'VIT - VALENCIA' ));
        SupportChannel::create(array( 'id' => 3, 'nombre' => 'VIT - GRAN CARACAS' ));
    }
}
