<?php

use Illuminate\Database\Seeder;
use App\PresupuestoStatus;
class PresupuestoStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statusArr = [
            'Activo', 
            'Enviado', 
            'Procesado',
            'Aprobado',  
            'En Facturación',
            'Cancelado',
        ];
        
        for($i=0; $i<6; $i++){
           PresupuestoStatus::create(['presupuesto_status' => $statusArr[$i]]);
        }
    }
}
