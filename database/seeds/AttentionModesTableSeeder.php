<?php

use Illuminate\Database\Seeder;
use App\AttentionMode;

class AttentionModesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AttentionMode::create(array( 'id' => 1, 'nombre' => 'Domicilio' ));
        AttentionMode::create(array( 'id' => 2, 'nombre' => 'Sin Garantía' ));
        AttentionMode::create(array( 'id' => 3, 'nombre' => 'Devolucion CANTV' ));
        AttentionMode::create(array( 'id' => 4, 'nombre' => 'RMA' ));
        AttentionMode::create(array( 'id' => 5, 'nombre' => 'Devolucion CCD' ));
    }
}
