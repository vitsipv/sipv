<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\modulos;
class ModulosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        modulos::create(array( 'id' => 1, 'nombre' => 'clientes' ));
        modulos::create(array( 'id' => 2, 'nombre' => 'casos' ));
        modulos::create(array( 'id' => 3, 'nombre' => 'apertura casos' ));
        modulos::create(array( 'id' => 4, 'nombre' => 'gestionar casos' ));
        modulos::create(array( 'id' => 5, 'nombre' => 'recepcion' ));
        modulos::create(array( 'id' => 6, 'nombre' => 'tecnico' ));
        modulos::create(array( 'id' => 7, 'nombre' => 'presupuesto' ));
        modulos::create(array( 'id' => 8, 'nombre' => 'llamadas' ));
        modulos::create(array( 'id' => 9, 'nombre' => 'perfil' ));
        modulos::create(array( 'id' => 10, 'nombre' => 'inventario' ));
        modulos::create(array( 'id' => 11, 'nombre' => 'Partes y piezas' ));
        modulos::create(array( 'id' => 12, 'nombre' => 'Equipos' ));
        modulos::create(array( 'id' => 13, 'nombre' => 'Movimientos' ));
        modulos::create(array( 'id' => 14, 'nombre' => 'Desarrollo' ));
    }
}
