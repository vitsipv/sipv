<?php

use Illuminate\Database\Seeder;
use App\Origin;

class OriginsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $originsArr = [
            'CDDVP - IPSFA', 
            'CDDVP - Los Teques', 
            'CDDVP - Plaza Venezuela', 
            'CDDVP - Terrazas del Ávila', 
            'CDDVP - Urdaneta',
            'Sede - Chuao',
            'Sede - Dos Caminos',
            'Sede - Falcón',
            'Sede - San Antonio',
            'Sede - Valencia'
        ];
        
        for($i=0; $i<10; $i++){
           Origin::create(['origin' => $originsArr[$i]]);
        }
    }
}
