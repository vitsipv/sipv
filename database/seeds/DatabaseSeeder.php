<?php

use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(UserTypesTableSeeder::class);
        $this->call(SupportChannelsTableSeeder::class);
        $this->call(UserAdminSeeder::class);
        $this->call(ppCategoriesTableSeeder::class);
        $this->call(OriginsTableSeeder::class);
        $this->call(VenezuelaTableSeeder::class);
        $this->call(PurchaseChannelsTableSeeder::class);
        $this->call(AttentionModesTableSeeder::class);
        $this->call(CaseStatusTableSeeder::class);
        $this->call(PresupuestoStatusTableSeeder::class);
        $this->call(IvaTableSeeder::class);
        $this->call(TipoComentarioTableSeeder::class);
        $this->call(FallasTableSeeder::class);
        $this->call(CanalesUsuariosSeeder::class);
        
    }
}
