<?php

use Illuminate\Database\Seeder;
use App\UserType;

class UserTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserType::create(array( 'id' => 1, 'nombre' => 'admin' ));
        UserType::create(array( 'id' => 2, 'nombre' => 'Gerente' ));
        UserType::create(array( 'id' => 3, 'nombre' => "Coordinador de atención al usuario"));
        UserType::create(array( 'id' => 4, 'nombre' => "Analista de soporte técnico"));
        UserType::create(array( 'id' => 5, 'nombre' => "Analista de certificación"));
        UserType::create(array( 'id' => 6, 'nombre' => "Operador de soporte técnico"));
        UserType::create(array( 'id' => 7, 'nombre' => "Supervisor de soporte técnico"));
        UserType::create(array( 'id' => 8, 'nombre' => "Supervisor división de soporte telefónico pc"));
        UserType::create(array( 'id' => 9, 'nombre' => "Asistente administrativo"));
        UserType::create(array( 'id' => 10, 'nombre' => "Recepcionista"));
        UserType::create(array( 'id' => 11, 'nombre' => "Tecnico almacenista"));
        UserType::create(array( 'id' => 12, 'nombre' => "Jefe division"));
        UserType::create(array( 'id' => 13, 'nombre' => "Jefe del centro de atención telefónico"));
        UserType::create(array( 'id' => 14, 'nombre' => "Tecnico pc"));
        UserType::create(array( 'id' => 15, 'nombre' => "Cajera"));
        UserType::create(array( 'id' => 16, 'nombre' => "Agente vip"));
        UserType::create(array( 'id' => 17, 'nombre' => "Supervisor area presupuesto y canales de soporte"));
        UserType::create(array( 'id' => 18, 'nombre' => "Jefe de division de soporte tecnico de pc"));
        UserType::create(array( 'id' => 19, 'nombre' => "Supervisor de call center"));
        UserType::create(array( 'id' => 20, 'nombre' => "Técnico de servidores"));
        UserType::create(array( 'id' => 21, 'nombre' => "Supervisor de almacén"));
        UserType::create(array( 'id' => 22, 'nombre' => "Analista integral administrativo"));
        UserType::create(array( 'id' => 23, 'nombre' => "Jefe de division de servidores"));

    }
}

