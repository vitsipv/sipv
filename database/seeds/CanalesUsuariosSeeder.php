<?php

use Illuminate\Database\Seeder;
use App\canales_usuario;

class CanalesUsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        canales_usuario::create(array( 'canal_soporte_id' => 1, 'user_id' => 1 ));
        canales_usuario::create(array( 'canal_soporte_id' => 2, 'user_id' => 1 ));
        canales_usuario::create(array( 'canal_soporte_id' => 3, 'user_id' => 1 ));
    }
}
