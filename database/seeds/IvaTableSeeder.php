<?php

use Illuminate\Database\Seeder;
use App\Iva;
class IvaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Iva::create(array( 'id' => 1, 'porcentaje' => 16 ));
    }
}
