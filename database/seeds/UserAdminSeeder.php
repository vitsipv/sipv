<?php

use Illuminate\Database\Seeder;
use App\User;

class UserAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Javier',
            'email' => 'javier@me.com',
            'password' => bcrypt('123456'),
            'nacionalidad' => 'Sin asignar',
            'cedula' => 'Sin asignar',
            'estado' => 'Sin asignar',
            'municipio' => 'Sin asignar',
            'parroquia' => 'Sin asignar',
            'direccion' => 'Sin asignar',
            'user_type_id' => '1',
        ]);
    }
}
