<?php

use Illuminate\Database\Seeder;
use App\CaseStatus;
class CaseStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statusArr = [
            'En espera', 
            'Abierto', 
            'Cerrado', 
            'Anulado', 
            'Re-aperturado'
        ];
        
        for($i=0; $i<5; $i++){
           CaseStatus::create(['case_status' => $statusArr[$i]]);
        }
    }
}
