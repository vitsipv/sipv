<?php

use Illuminate\Database\Seeder;
use App\ppCategory;

class ppCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $ppCategoriesArr = [
        //     'Telefonos',
        //     'Cables',
        //     'Carros',
        //     'Zapatos'
        // ];

        // $ppDescriptionsArr = [
        //     ''

        // ];
        foreach (range(1, 5) as $c){
        	$ppcategory = ppCategory::create(['name' => "Categoría {$c}"]);

        	$ppsubcategories = [];

        	foreach (range(1, rand(1, 5)) as $s){
                $ppsubcategories[] = [
                    'pp_category_id' => "$s",
                    'pp_tipo' => "Categoría {$c}",
                    'pp_description' => "Sub Categoría {$c} - {$s}",
                    'pp_np' => "N{$c}P{$s}AAA",
                
                ];
        	}

        	$ppcategory->ppdescription()->createMany($ppsubcategories);
        }
    }
}
